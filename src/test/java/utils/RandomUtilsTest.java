package utils;

import org.junit.Test;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Comparator;

import static org.junit.Assert.*;

public class RandomUtilsTest {

    @Test
    public void variance() {
        ArrayList<Float> list = new ArrayList<>(100000);
        for (int i = 0; i < 100000; i++) {
            float random = RandomUtils.getInstance().variance(0.2f);
            list.add(random);
            assertEquals(1, random, 0.2f);
        }
        list.sort(new Comparator<Float>() {
            @Override
            public int compare(Float o1, Float o2) {
                if (o1 > o2) return 1;
                if (o2 > o1) return -1;
                return 0;
            }
        });
        System.out.println("Min");
        System.out.println(list.get(0));
        System.out.println("Max");
        System.out.println(list.get(list.size()-1));
    }
}