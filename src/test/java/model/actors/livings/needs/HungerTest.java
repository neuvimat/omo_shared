package model.actors.livings.needs;

import controller.action.Action;
import model.actors.Living;
import model.configuration.House;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Duration;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

public class HungerTest {
    Hunger hunger = new Hunger(null, 100, 50);

    @Before
    public void setUp() throws Exception {
        hunger = new Hunger(null);
    }

    @Test
    public void refreshFully() {
        hunger.setCurrentPercentage(0.5f);
        assertEquals(0.5f, hunger.getPercentageHunger(), 0.000001f);
        hunger.refreshFully();
        assertEquals(1, hunger.getPercentageHunger(), 0);
    }

    @Test
    public void refreshPercentage() {
        hunger.setCurrentPercentage(0.5f);
        assertEquals(0.5f, hunger.getPercentageHunger(), 0.000001f);
        hunger.refreshPercentage(.2f);
        assertEquals(0.7f, hunger.getPercentageHunger(), 0);
    }

    @Test
    public void refreshAbsolute() {
        hunger.setCurrentPercentage(0.5f);
        assertEquals(0.5f, hunger.getPercentageHunger(), 0.000001f);
        hunger.refreshAbsolute(20f);
        assertEquals(70, hunger.getCurrent(), 0);
    }

    @Test
    public void drain() {
        hunger.drain(Duration.ofHours(1));
        assertEquals(hunger.getMax() - hunger.getDrain(), hunger.getCurrent(), 0.001f);
    }

    @Test
    public void hungerIsProperlyCapped() {
        hunger.setCurrentPercentage(10000);
        assertEquals(100, hunger.getCurrent(), 0);
        hunger.setCurrentPercentage(-1000);
        assertEquals(0, hunger.getCurrent(), 0);
        hunger.refreshAbsolute(999999999999f);
        assertEquals(100, hunger.getCurrent(), 0);
    }

    @Test
    public void getFoodAmountForFullStomach() {
        hunger.setCurrentPercentage(.5f);
        assertEquals(50, hunger.getFoodAmountForFullStomach(), 0);
    }

    @Test
    public void getTotalFoodEaten() {
        hunger.setCurrentPercentage(0.5f);
        hunger.refreshAbsolute(20);
        assertEquals(20, hunger.getTotalFoodEaten(), 0);
        hunger.refreshFully();
        assertEquals(50, hunger.getTotalFoodEaten(), 0);
    }

    @Test
    public void reducePercentage() {
        hunger.setCurrentPercentage(0.5f);
        assertEquals(50, hunger.getCurrent(), 0);
        hunger.reducePercentage(0.25f);
        assertEquals(25, hunger.getCurrent(), 0);
    }
}