package model.device;

import controller.TimeTracker;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.*;

public class ConsumptionTest {
    Consumption s;

    @Before
    public void setUp() throws Exception {
        s = new Consumption(100,20,0);

        TimeTracker.getInstance().setActualTime(Duration.ZERO);
        s.setActive();
        TimeTracker.getInstance().setActualTime(Duration.ofHours(6));
        s.setIdle();
        TimeTracker.getInstance().setActualTime(Duration.ofHours(8));
        s.setOff();
    }

    @Test
    public void getConsumptionIsCorrectInHoursFrom_0_to_1() {
        assertEquals(100, s.getConsumption(Duration.ofHours(0), Duration.ofHours(1)), 0);
    }

    @Test
    public void getConsumptionIsCorrectInHoursFrom_5_to_7() {
        assertEquals(120, s.getConsumption(Duration.ofHours(5), Duration.ofHours(7)), 0);
    }

    @Test
    public void getConsumptionIsCorrectInHoursFrom_0_to_8() {
        assertEquals(640, s.getConsumption(Duration.ofHours(0), Duration.ofHours(8)), 0);
    }

    @Test
    public void getConsumptionIsCorrectInHoursFrom_0_to_9() {
        assertEquals(640, s.getConsumption(Duration.ofHours(0), Duration.ofHours(9)), 0);
    }

    @Test
    public void getConsumptionIsCorrectInHoursFrom_0_to_10() {
        assertEquals(640, s.getConsumption(Duration.ofHours(0), Duration.ofHours(10)), 0);
    }

    @Test
    public void getConsumptionIsCorrectInHoursFrom_0_to_1000000() {
        assertEquals(640, s.getConsumption(Duration.ofHours(0), Duration.ofHours(1000000)), 0);
    }
}