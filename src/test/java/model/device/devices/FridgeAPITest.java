package model.device.devices;

import model.actors.Living;
import model.actors.livings.needs.Hunger;
import org.junit.Test;
import static org.mockito.Mockito.*;
import utils.Constants;

import static org.junit.Assert.*;

public class FridgeAPITest {
    FridgeAPI fridge = new FridgeAPI();

    @Test
    public void feedLiving() {
        Living living = mock(Living.class);
        when(living.getHunger()).thenReturn(new Hunger(living, 100, 72));

        living.getHunger().setCurrentPercentage(0.5f);
        float delta = living.getHunger().getFoodAmountForFullStomach();
        fridge.feedLiving(living);

        assertEquals(Constants.FRIDGE_REFILL_AMOUNT - delta, fridge.getFoodLeft(),0);
    }

    @Test
    public void withdraw() {
        float food = fridge.getFoodLeft();

        fridge.withdraw(40);

        assertEquals(food - 40, fridge.getFoodLeft(), 0);
    }

    @Test
    public void refill() {
        float food = fridge.getFoodLeft();

        fridge.refill();

        assertEquals(food + Constants.FRIDGE_REFILL_AMOUNT, fridge.getFoodLeft(), 0);
    }
}