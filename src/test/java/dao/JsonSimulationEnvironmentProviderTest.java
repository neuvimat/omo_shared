package dao;

import model.configuration.House;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonSimulationEnvironmentProviderTest {
    final String filepath = "src/test/resources/houses/simplestHouse.json";
    JsonSimulationEnvironmentProvider environmentProvider = new JsonSimulationEnvironmentProvider(filepath);

    @Test
    public void makeEnvironment() {
        House house = environmentProvider.makeEnvironment();
    }
}