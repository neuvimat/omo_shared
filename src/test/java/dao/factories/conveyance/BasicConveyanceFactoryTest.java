package dao.factories.conveyance;

import model.conveyance.Bicycle;
import org.junit.Test;

import static org.junit.Assert.*;

public class BasicConveyanceFactoryTest {

    BasicConveyanceFactory factory = new BasicConveyanceFactory();

    @Test
    public void makeBicycle() {
        Object o = factory.makeBicycle();
        assertNotNull(o);
    }
    @Test
    public void makeCar() {
        Object o = factory.makeCar();
        assertNotNull(o);
    }
    @Test
    public void makeSki() {
        Object o = factory.makeSki();
        assertNotNull(o);
    }
}