package dao.factories.device;

import org.junit.Test;

import static org.junit.Assert.*;

public class BasicDeviceFactoryTest {

    BasicDeviceFactory factory = new BasicDeviceFactory();

    @Test
    public void makeComputer() {
        Object o = factory.makeComputer();
        assertNotNull(o);
    }
    @Test
    public void makeCooker() {
        Object o = factory.makeCooker();
        assertNotNull(o);
    }
    @Test
    public void makeFridge() {
        Object o = factory.makeFridge();
        assertNotNull(o);
    }
    @Test
    public void makeHeating() {
        Object o = factory.makeHeating();
        assertNotNull(o);
    }
    @Test
    public void makeLight() {
        Object o = factory.makeLight();
        assertNotNull(o);
    }
    @Test
    public void makeTelevision() {
        Object o = factory.makeTelevision();
        assertNotNull(o);
    }
    @Test
    public void makeWashingMachine() {
        Object o = factory.makeWashingMachine();
        assertNotNull(o);
    }
    @Test
    public void makeWindow() {
        Object o = factory.makeWindow();
        assertNotNull(o);
    }
}