package dao.matchers;

import dao.factories.room.BasicRoomFactory;
import model.configuration.Room;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoomMatcherTest {
    RoomMatcher roomMatcher = new RoomMatcher(new BasicRoomFactory());

    @Test
    public void matchKitchenReturnsKitchen() {
        Room room = roomMatcher.match("kitchen");
        assertEquals("Kitchen", room.getName());
    }

    @Test
    public void matchNonsenseReturnsNull() {
        Room room = roomMatcher.match("nonsense");
        assertNull(room);
    }

    // This is more or less a test of a specific factory, not the matchers itself, but w/e
    @Test
    public void everythingInMapIsMappedProperlyAndDoesNotReturnNull() {
        for (String s : roomMatcher.getMappedKeywords()) {
            assertNotNull(roomMatcher.match(s));
        }
    }
}