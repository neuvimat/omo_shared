package view;

import controller.ActionDrivenSimulation;
import controller.Simulation;
import dao.JsonSimulationEnvironmentProvider;
import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.*;

public class BasicEventReporterTest {

    @Test
    public void generateReportTest() {
        final String filepath = "src/test/resources/houses/disfunctionalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(100));
        sim.generateReport(new BasicEventReporter(System.out));
    }
}