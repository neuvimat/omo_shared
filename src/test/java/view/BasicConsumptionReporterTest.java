package view;

import controller.ActionDrivenSimulation;
import controller.Simulation;
import dao.JsonSimulationEnvironmentProvider;
import model.configuration.House;
import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.*;

public class BasicConsumptionReporterTest {

    @Test
    public void generateReportTest() {
        final String filepath = "src/test/resources/houses/disfunctionalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(100));
        ConsumptionCosts costs = new ConsumptionCosts(8, 6, 2);
        sim.generateReport(new BasicConsumptionReporter(System.out, Duration.ZERO, Duration.ofDays(100), costs));
    }

    @Test
    public void correctValuesDisplayed() {
        // House house = new House();
    }
}