package controller;

import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.*;

public class TimeTrackerTest {

    TimeTracker tracker = TimeTracker.getInstance();

    @Test
    public void initializationTimeIsDurationZero() {
        assertEquals(0, tracker.getActualTime().compareTo(Duration.ZERO));
    }

    @Test
    public void setActualTimeThenReturnsTheCorrectTime() {
        Duration newTime = Duration.ofMinutes(64);
        tracker.setActualTime(newTime);
        assertEquals(newTime, tracker.getActualTime());
    }
}