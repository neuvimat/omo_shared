package controller;

import controller.action.Action;
import model.actors.Actor;
import model.device.Usable;
import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ActionQueueEntryTest {

    @Test
    public void compareTo() {
        ArrayList<ActionQueueEntry> actions = new ArrayList<>();
        ActionQueueEntry e1 = new ActionQueueEntry(null, Duration.ofDays(1));
        ActionQueueEntry e2 = new ActionQueueEntry(null, Duration.ofMillis(100));
        actions.add(e1);
        actions.add(e2);

        actions.sort(ActionQueueEntry::compareTo);
        assertEquals(e2, actions.get(0));
        assertEquals(e1, actions.get(1));
    }
}