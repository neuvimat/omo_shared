package controller;

import dao.JsonSimulationEnvironmentProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utils.Constants;
import view.BasicSurvivorReporter;

import java.time.Duration;

import static org.junit.Assert.*;

public class ActionDrivenSimulationTest {

    @Before
    public void setUp() throws Exception {
        Constants.VERBOSE = true;
    }

    @After
    public void tearDown() throws Exception {
        Constants.VERBOSE = false;
    }

    @Test
    public void weCanAtLeastRun() {
        final String filepath = "src/test/resources/houses/simplestHouse.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(100));
    }

    @Test
    public void disfunctionalFamily() {
        final String filepath = "src/test/resources/houses/disfunctionalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(100));

        sim.generateReport(new BasicSurvivorReporter(System.out));

        System.out.println("Stop");
    }

    @Test
    public void modernLGBTFamily() {
        final String filepath = "src/test/resources/houses/modernLGBTFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(21));
        System.out.println("Stop");
    }

    @Test
    public void normalFamily() {
        final String filepath = "src/test/resources/houses/normalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(1000));
        System.out.println("Stop");
    }

    @Test
    public void crazyCatLady() {
        final String filepath = "src/test/resources/houses/crazyCatLady.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofDays(21));
        System.out.println("Stop");
    }
}