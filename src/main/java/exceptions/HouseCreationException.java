package exceptions;

public class HouseCreationException extends Exception {
    public HouseCreationException() {
    }
    public HouseCreationException(String message) {
        super(message);
    }
    public HouseCreationException(String message, Throwable cause) {
        super(message, cause);
    }
    public HouseCreationException(Throwable cause) {
        super(cause);
    }
    public HouseCreationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
