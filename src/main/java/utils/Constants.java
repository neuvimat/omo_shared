package utils;

public class Constants {
    public static final long TIME_TO_DRIVE_TO_SHOP = 60*15; // seconds
    public static final float TIME_TO_DRIVE_TO_SHOP_VARIANCE = 0.15f;

    public static final long TIME_TO_SHOP = 60*35; // seconds
    public static final float TIME_TO_SHOP_VARIANCE = 0.25f;

    public static final long TIME_TO_PARK = 20; // seconds
    public static final float TIME_TO_PARK_VARIANCE = 0.50f;

    public static final float FRIDGE_REFILL_AMOUNT = 1000;
    public static final float FRIDGE_SPOIL_RATE = 120; // 1 food spoils per FRIDGE_SPOIL_RATE seconds

    public static final float COOKER_MAX_BEARABLE_FILTH = 60;
    public static final float COOKER_FILTH_PER_SESSION = 20;

    public static final float WASHING_MACHINE_CLOTH_CAPACITY = 10;


    public static boolean VERBOSE = false; // fixme: not that much constant but whatever
}
