package utils;

import java.time.Duration;

public class DurationUtil {
    private static StringBuilder sb = new StringBuilder(255);
    public static String format(Duration duration) {
        sb.setLength(0);
        long seconds = duration.getSeconds();
        long days, hours, minutes;
        days = (seconds / 86400);
        seconds = seconds % 86400;
        hours = (seconds / 3600);
        seconds = seconds % 3600;
        minutes = seconds / 60;
        seconds = seconds % 60;
        if (days < 10) sb.append("0").append(days); else sb.append(days);
        sb.append("d:");
        if (hours < 10) sb.append("0").append(hours); else sb.append(hours);
        sb.append(":");
        if (minutes < 10) sb.append("0").append(minutes); else sb.append(minutes);
        sb.append(":");
        if (seconds < 10) sb.append("0").append(seconds); else sb.append(seconds);
        return sb.toString();
    }
}
