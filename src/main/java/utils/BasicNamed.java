package utils;

public interface BasicNamed {

    /**
     * @return Should return some name representing object. It doesn't have to be unique.
     */
    String getName();
}
