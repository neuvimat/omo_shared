package utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Utils for random algorithms.
 */
public class RandomUtils {
    private String surname = "Doe" ;
    private ArrayList<String> maleNames;
    private ArrayList<String> femaleNames;
    private ArrayList<String> dogNames;
    private ArrayList<String> catNames;
    private ArrayList<String> sapperNames;
    private int outOfNames = 0;

    private String[] socialActionsDescriptions = {"Saying hi", "Telling a joke", "Gossiping", "Asking about something", "Making conversation", "Yelling at"};

    private static RandomUtils instance;
    private Random random;

    private RandomUtils(int seed){
        random = new Random(seed);
    }

    /**
     * For changing initial state of random.
     * @param seed seed for random
     */
    public static void create(int seed){
        instance = new RandomUtils(seed);
    }

    public String getSocialActionDescription() {
        return socialActionsDescriptions[nextInt(socialActionsDescriptions.length)];
    }

    public float variance(float plusMinusDifference) {
        return (1f - plusMinusDifference) + (random.nextFloat() / (1f/plusMinusDifference/2));
    }

    /**
     * @return same single instance of RandomUtils
     */
    public static synchronized RandomUtils getInstance(){
        if(instance == null){
            instance = new RandomUtils(0);
        }
        return instance;
    }

    /**
     * Random element in list.
     * @param list where to choose
     * @param <T> element type
     * @return random element in list
     */
    public <T> T getRandomFromList(List<T> list){
        if(!list.isEmpty()){
            return list.get(random.nextInt(list.size()));
        }
        return null;
    }

    /**
     * Random element in collection.
     * @param collection where to choose
     * @param <T> element type
     * @return random element in collection
     */
    public <T> T getRandomFromCollection(Collection<T> collection){
        if(!collection.isEmpty()){
            Object[] array = collection.toArray();
            return (T)array[nextInt(array.length)];
        }
        return null;
    }

    /**
     * Gets random number in [0, bound)
     * @param bound to number exclusive
     * @return random integer
     */
    public int nextInt(int bound){
        return random.nextInt(bound);
    }

    /**
     * @return random float
     */
    public float nextFloat() {
        return random.nextFloat();
    }

    /**
     * @return random boolean
     */
    public boolean nextBool() { return nextInt(2) == 1; }

    public <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public String getDogName() {
        if (dogNames == null) {
            dogNames = new ArrayList<>();
            dogNames.add("Rex");
            dogNames.add("Doggo");
            dogNames.add("Good boy");
        }
        if (dogNames.isEmpty()) {
            return "Dog#" + ++outOfNames;
        }
        return getName(dogNames);
    }

    public String getCatName() {
        if (catNames == null) {
            catNames = new ArrayList<>();
            catNames.add("Kit");
            catNames.add("Kat");
            catNames.add("Chunky");
            catNames.add("Passy");
        }
        if (catNames.isEmpty()) {
            return "Cat#" + ++outOfNames;
        }
        return getName(catNames);
    }

    public String getFemaleName() {
        if (femaleNames == null) {
            femaleNames = new ArrayList<>();
            femaleNames.add("Catherine");
            femaleNames.add("Susan");
            femaleNames.add("Lydia");
            femaleNames.add("Eve");
            femaleNames.add("Anabelle");
            femaleNames.add("Elizabeth");
        }
        if (femaleNames.isEmpty()) {
            return "Female#" + ++outOfNames;
        }
        return getName(femaleNames);
    }

    public String getMaleName() {
        if (maleNames == null) {
            maleNames = new ArrayList<>();
            maleNames.add("John");
            maleNames.add("Arthur");
            maleNames.add("Seymour");
            maleNames.add("Jack");
            maleNames.add("Patrick");
            maleNames.add("David");
        }
        if (maleNames.isEmpty()) {
            return "Male#" + ++outOfNames;
        }
        return getName(maleNames);
    }

    public String getSaboteurName() {
        if (sapperNames == null) {
            sapperNames = new ArrayList<>();
            sapperNames.add("Dick");
            sapperNames.add("Evil Nevil");
            sapperNames.add("Mean Dean");
        }
        if (sapperNames.isEmpty()) {
            return "Sapper#" + ++outOfNames;
        }
        return getName(sapperNames);
    }

    private String getName(List<String> list) {
        String name = getRandomFromList(list);
        list.remove(name);
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
