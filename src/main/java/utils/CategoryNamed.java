package utils;

public interface CategoryNamed {
    /**
     * @return Should be some unique name for category of similar objects.
     *         For example for Devices every instance of FridgeModel should return same name.
     */
    String getCategoryName();
}
