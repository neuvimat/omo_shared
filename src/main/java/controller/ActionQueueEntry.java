package controller;

import model.actors.Actor;

import java.time.Duration;

/**
 * Container that holds which actor is supposed to think at what time
 */
public class ActionQueueEntry implements Comparable<ActionQueueEntry>{
    private Actor actor;
    private Duration startAt;

    public ActionQueueEntry(Actor actor, Duration startAt) {
        this.actor = actor;
        this.startAt = startAt;
    }

    public ActionQueueEntry(Actor actor) {
        this.actor = actor;
        this.startAt = TimeTracker.getInstance().getActualTime();
    }

    public Actor getActor() {
        return actor;
    }

    public Duration getStartAt() {
        return startAt;
    }

    @Override
    public int compareTo(ActionQueueEntry o) {
        return startAt.compareTo(o.startAt);
    }
}
