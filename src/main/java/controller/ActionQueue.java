package controller;

import controller.action.Action;
import model.actors.Actor;

import java.time.Duration;
import java.util.PriorityQueue;

/**
 * Small little decorator to keep things together
 */
public class ActionQueue {
    PriorityQueue<ActionQueueEntry> queue;

    public ActionQueue() {
        queue = new PriorityQueue<ActionQueueEntry>();
    }

    /**
     * Adds a prompt to actor to think right now
     * @param actor that is supposed to think
     */
    public void addEntry(Actor actor) {
        queue.add(new ActionQueueEntry(actor));
    }

    /**
     * Adds a prompt to actor to think at the specified absolute duration. If the duration is shorter than the current
     * time, the actor has priority thinking
     * @param actor that is supposed to think
     * @param startAt time to add the think prompt at
     */
    public void addEntry(Actor actor, Duration startAt) {
        queue.add(new ActionQueueEntry(actor, startAt));
    }

    /**
     * Gets the next entry and removes it from the queue
     * @return ActionQueueEntry
     */
    public ActionQueueEntry getNext() {
        return queue.poll();
    }

    /**
     * Gets the next entry, but it stays in the queue
     * @return ActionQueueEntry
     */
    public ActionQueueEntry peekNext() {
        return queue.peek();
    }

    /**
     * Self explanatory
     * @return
     */
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    /**
     * Self explanatory
     * @return
     */
    public int size() {
        return queue.size();
    }
}
