package controller;


import java.time.Duration;

/**
 * Utility to easily access actual time.
 */
public class TimeTracker {
    private static TimeTracker instance = null;
    
    private Duration actualTime;

    private TimeTracker() {
        actualTime = Duration.ZERO;
    }

    /**
     * @return unique single instance
     */
    public synchronized static TimeTracker getInstance() {
        if(instance == null){
            instance = new TimeTracker();
        }
        return instance;
    }

    /**
     * @return actual time in simulation as copy of the one stored in the tracker
     */
    public Duration getActualTime() {
        return actualTime.abs();
    }

    /**
     * @param actualTime new actual time in simulation
     */
    public void setActualTime(Duration actualTime) {
        this.actualTime = actualTime;
    }
}
