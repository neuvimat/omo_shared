package controller.event;

import utils.DurationUtil;

import java.time.Duration;

/**
 * Represents execution of event in some time.
 */
public class EventExecution {
    private Event event;
    private Duration timeExecuted;
    private EventSource eventSource;

    public EventExecution(Event event, EventSource eventSource, Duration timeExecuted) {
        this.event = event;
        this.timeExecuted = timeExecuted;
        this.eventSource = eventSource;
    }

    /**
     * @return Event to be executed.
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @return time when executed
     */
    public Duration getTimeExecuted() {
        return timeExecuted;
    }

    /**
     * @return who executed the event
     */
    public EventSource getEventSource() {
        return eventSource;
    }

    @Override
    public String toString() {
        return DurationUtil.format(timeExecuted) + ": " +
                eventSource.getCategoryName() + " -> " + event.getCategoryName();
    }
}
