package controller.event;

import utils.CategoryNamed;

/**
 * Container for event data.
 */
public interface Event extends CategoryNamed {

}
