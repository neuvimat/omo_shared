package controller.event;

public interface DisposableEventListener <Args extends DisposableEvent> extends EventListener<Args> {
    /**
     * @return Rank number of listen representing its priority to be called on event execution.
     *         Only listener with biggest rank is called.
     */
    int getRank();
}
