package controller.event;

import utils.CategoryNamed;

/**
 * Marks that class executes events.
 */
public interface EventSource extends CategoryNamed {
}
