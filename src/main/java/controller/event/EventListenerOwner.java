package controller.event;

import utils.BasicNamed;
import utils.CategoryNamed;

/**
 * Marks that class listens to events.
 */
public interface EventListenerOwner extends BasicNamed {
}
