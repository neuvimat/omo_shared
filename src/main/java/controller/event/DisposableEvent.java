package controller.event;

/**
 * Event that is handled once from listener with highest rank.
 */
public interface DisposableEvent extends Event {
}
