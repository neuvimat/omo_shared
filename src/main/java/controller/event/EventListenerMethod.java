package controller.event;

/**
 * Method of event listener. Mostly created with lambda.
 * @param <E> argument of listener
 */
public interface EventListenerMethod<E>
{
    void update(E eventArgs);
}
