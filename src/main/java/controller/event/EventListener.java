package controller.event;

/**
 * Structure for saving event listeners.
 * @param <Args> type of event
 */
public interface EventListener <Args extends Event> {

    /**
     * @return Event listening to.
     */
    Event getEvent();

    /**
     * @return Gets interface representing owner of the listener method.
     */
    EventListenerOwner getOwner();

    /**
     * Should call the listener method.
     */
    void executeMethod(Args args);

}
