package controller.event;

/**
 * Basic DisposableEventListener implementation.
 */
public class DisposableEventListenerImpl<Args extends DisposableEvent> implements DisposableEventListener<Args> {

    private int rank;
    private Event event;
    private EventListenerOwner owner;
    private EventListenerMethod<Args> method;

    public DisposableEventListenerImpl(Event event, EventListenerOwner owner, EventListenerMethod<Args> method, int rank) {
        this.event = event;
        this.owner = owner;
        this.method = method;
        this.rank = rank;
    }

    @Override
    public Event getEvent() {
        return event;
    }

    @Override
    public EventListenerOwner getOwner() {
        return owner;
    }

    @Override
    public void executeMethod(Args args) {
        method.update(args);
    }

    @Override
    public int getRank() {
        return rank;
    }
}
