package controller.event;

import utils.Constants;
import view.BlankSimulationView;
import view.ConsoleSimulationView;
import view.SimulationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Event system global for all events in simulation.
 */
public class GlobalEventSystem implements EventSystem {

    private static GlobalEventSystem instance = null;

    private Map<String, List<EventListener>> eventNameToListenersMap;
    private Map<String, List<DisposableEventListener>> disposeableEventNameToListenersMap;
    private List<EventHistoryRecord> history;
    private SimulationView simulationView;

    private GlobalEventSystem() {
        eventNameToListenersMap = new HashMap<>();
        disposeableEventNameToListenersMap = new HashMap<>();
        history = new ArrayList<>();
        simulationView = new ConsoleSimulationView();
    }

    public static synchronized GlobalEventSystem getInstance(){
        if(instance == null){
            instance = new GlobalEventSystem();
        }
        return instance;
    }

    public void execute(EventExecution execution) {
        if (Constants.VERBOSE) simulationView.displayEvent(execution);
        String eventName = execution.getEvent().getCategoryName();
        if(eventNameToListenersMap.containsKey(eventName)){
            List<EventListener> listeners = eventNameToListenersMap.get(eventName);
            for(EventListener listener: listeners){
                listener.executeMethod(execution.getEvent());
                history.add(new EventHistoryRecord(execution, listener));
            }
        }
    }

    public void execute(DisposableEventExecution execution) {
        if (Constants.VERBOSE) simulationView.displayEvent(execution);
        String eventName = execution.getEvent().getCategoryName();
        if(disposeableEventNameToListenersMap.containsKey(eventName)){
            List<DisposableEventListener> listeners = disposeableEventNameToListenersMap.get(eventName);
            DisposableEventListener bestCandidate = null;
            for(DisposableEventListener listener: listeners){
                if(bestCandidate == null){
                    bestCandidate = listener;
                }
                else if(listener.getRank() > bestCandidate.getRank()){
                    bestCandidate = listener;
                }
            }
            if(bestCandidate != null){
                bestCandidate.executeMethod(execution.getEvent());
                history.add(new EventHistoryRecord(execution, bestCandidate));
            }
        }
    }

    public void attach(EventListener listener) {
        String eventName = listener.getEvent().getCategoryName();
        List<EventListener> listeners;
        if(!eventNameToListenersMap.containsKey(eventName)){
            eventNameToListenersMap.put(eventName, (listeners = new ArrayList<>()));
        }
        else {
            listeners = eventNameToListenersMap.get(eventName);
        }
        listeners.add(listener);
    }

    public void attach(DisposableEventListener listener) {
        String eventName = listener.getEvent().getCategoryName();
        List<DisposableEventListener> listeners;
        if(!disposeableEventNameToListenersMap.containsKey(eventName)){
            disposeableEventNameToListenersMap.put(eventName, (listeners = new ArrayList<>()));
        }
        else {
            listeners = disposeableEventNameToListenersMap.get(eventName);
        }
        listeners.add(listener);
    }

    public List<EventHistoryRecord> getHistory() {
        return history;
    }

    @Override
    public void setSimulationView(SimulationView simulationView) {
        this.simulationView = simulationView;
    }
}
