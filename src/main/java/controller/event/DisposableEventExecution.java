package controller.event;

import java.time.Duration;

/**
 * EventExecution for DisposableEvent.
 */
public class DisposableEventExecution extends EventExecution {

    public DisposableEventExecution(DisposableEvent event, EventSource eventSource, Duration timeExecuted) {
        super(event, eventSource, timeExecuted);
    }
}
