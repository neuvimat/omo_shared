package controller.event;

import view.SimulationView;

import java.util.List;

/**
 * System for handling events.
 */
public interface EventSystem {

    /**
     * Execute event => call listeners.
     */
    void execute(EventExecution execution);

    /**
     * Saves event listener, so when is called execute appropriate listener is called
     * @param listener event listener
     */
    void attach(EventListener listener);

    /**
     * @return History of all calls to listener methods.
     */
    List<EventHistoryRecord> getHistory();

    /**
     * Sets SimulationView for handling output messages.
     * @param simulationView simulation view
     */
    void setSimulationView(SimulationView simulationView);

}
