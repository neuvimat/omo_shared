package controller.event;

/**
 * Basic event listener implementation.
 * @param <Args>
 */
public class EventListenerImpl<Args extends Event> implements EventListener<Args> {

    private Event event;
    private EventListenerOwner owner;
    private EventListenerMethod<Args> method;

    public EventListenerImpl(Event event, EventListenerOwner owner, EventListenerMethod<Args> method) {
        this.event = event;
        this.owner = owner;
        this.method = method;
    }

    @Override
    public Event getEvent() {
        return event;
    }

    @Override
    public EventListenerOwner getOwner() {
        return owner;
    }

    @Override
    public void executeMethod(Args args) {
        method.update(args);
    }
}
