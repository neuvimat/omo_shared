package controller.event;

/**
 * Structure for saving history records of listener calls.
 */
public class EventHistoryRecord {
    private EventExecution execution;
    private EventListener listener;

    public EventHistoryRecord(EventExecution execution, EventListener listener) {
        this.execution = execution;
        this.listener = listener;
    }

    public EventExecution getExecution() {
        return execution;
    }

    public EventListener getListener() {
        return listener;
    }
}
