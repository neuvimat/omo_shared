package controller;

import java.time.Duration;
import java.util.ArrayList;
import java.util.logging.Logger;

import controller.action.Action;
import controller.action.ActionHistoryRecord;
import controller.event.EventSystem;
import controller.event.GlobalEventSystem;
import dao.SimulationEnvironmentProvider;
import model.actors.Actor;
import model.configuration.House;
import utils.Constants;
import view.*;

public class ActionDrivenSimulation implements Simulation {
    private ActionQueue actionQueue;
    private House house;
    private TimeTracker timeTracker;
    private EventSystem eventSystem;
    private SimulationView simulationView;

    private ArrayList<ActionHistoryRecord> actionHistoryRecords;

    public ActionDrivenSimulation(SimulationEnvironmentProvider simulationEnvironmentProvider) {
        this.house = simulationEnvironmentProvider.makeEnvironment();
        this.timeTracker = TimeTracker.getInstance();
        this.actionQueue = new ActionQueue();
        this.actionHistoryRecords = new ArrayList<>(100);
        eventSystem = GlobalEventSystem.getInstance();

        // Make all actors do stuff at their spawn
        for (Actor actor : house.getAllActors()) {
            actionQueue.addEntry(actor);
        }
        simulationView = new ConsoleSimulationView();
        eventSystem.setSimulationView(simulationView);
    }

    public ActionDrivenSimulation(SimulationEnvironmentProvider simulationEnvironmentProvider, SimulationView simulationView){
        this(simulationEnvironmentProvider);
        this.simulationView = simulationView;
        eventSystem.setSimulationView(simulationView);
    }

    /**
     * Does a single step in the simulation
     * @return True if another step is possible
     */
    @Override
    public boolean doStep() {
        if (actionQueue.isEmpty() || actionQueue.size() == 1 && actionQueue.peekNext().getActor().getName().equals("The Weather")) {
            Logger.getLogger("").warning("Action queue is empty! This means there is nothing to happen in the simulation. The end.");
            return false;
        } else {
            ActionQueueEntry entry = actionQueue.getNext();
            Action action = entry.getActor().getNextAction();

            Duration now = timeTracker.getActualTime();
            Duration delta = entry.getStartAt().minus(now);
            timeTracker.setActualTime(now.plus(delta));

            // Check if the actor survives the update
            if (!entry.getActor().update(now)) {
                return true;
            }

            if (action != null) {
                ActionHistoryRecord record = new ActionHistoryRecord(action, timeTracker.getActualTime());
                if (Constants.VERBOSE) simulationView.displayAction(record);
                actionHistoryRecords.add(record);
                action.doAction();
                actionQueue.addEntry(entry.getActor(), timeTracker.getActualTime().plus(action.getDuration()));
            }
            else {
                Logger.getLogger("").warning("Action stored in ActionQueue was empty. Guessing the actor doesn't" +
                        " wish to act any longer until some further event or something.");
            }

            return true;
        }
    }

    /**
     * Do steps until the simulation reaches the inputted time
     * @param duration time which to stop the simulation at
     * @return True if another step is possible
     */
    @Override
    public boolean doSteps(Duration duration) {
//        while (!actionQueue.isEmpty() && actionQueue.peekNext().getStartAt().compareTo(duration) < 0) {
//            if (!doStep()) return false;
//        }
//        return true;
        while (true) {
            if (actionQueue.isEmpty()) {
                Logger.getLogger("").warning("Action queue is empty! This means there is nothing to happen in the simulation. The end.");
                return false;
            }
            if (actionQueue.peekNext().getStartAt().compareTo(duration) > 0) {
                Logger.getLogger("").warning("Reached the boundary duration. Simulation is stopping.");
                return true;
            }
            if (!doStep()) return false;
        }
    }

    /**
     * Does n steps of the simulation
     * @param numberOfSteps number of steps to perform
     * @return True if another step is possible
     */
    @Override
    public boolean doSteps(int numberOfSteps) {
        for (int i = 0; i < numberOfSteps; i++) {
            if (!doStep()) return false;
        }
        return true;
    }

    @Override
    public void generateReport(HouseConfigurationReporter reporter) {
        reporter.generateReport(house);
    }

    @Override
    public void generateReport(ActivityAndUsageReporter reporter) {
        reporter.generateReport(actionHistoryRecords);
    }

    @Override
    public void generateReport(EventReporter reporter) {
        reporter.generateReport(eventSystem.getHistory());
    }

    @Override
    public void generateReport(ConsumptionReporter reporter) {
        reporter.generateReport(house);
    }

    @Override
    public void generateReport(SurvivorReporter reporter) {
        reporter.generateReport(house.getAllLivings());
    }
}
