package controller.action;

import java.time.Duration;
import model.actors.Actor;
import model.device.Usable;

/**
 * Implementation of action with two arguments.
 */
public class ActionTwoArgs <Args1, Args2> implements Action {
    private String name;

    private Actor actor;
    private Duration duration;

    private Usable used = null;
    private Actor interactedWith = null;

    private ActionMethodTwoArgs<Args1, Args2> method;
    private Args1 args1;
    private Args2 args2;

    public ActionTwoArgs(String name, Actor actor, Duration duration, ActionMethodTwoArgs<Args1, Args2> method, Args1 args1, Args2 args2) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.method = method;
        this.args1 = args1;
        this.args2 = args2;
    }

    public ActionTwoArgs(String name, Actor actor, Duration duration, Usable used, ActionMethodTwoArgs<Args1, Args2> method, Args1 args1, Args2 args2) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.used = used;
        this.method = method;
        this.args1 = args1;
        this.args2 = args2;
    }

    public ActionTwoArgs(String name, Actor actor, Duration duration, Actor interactedWith, ActionMethodTwoArgs<Args1, Args2> method, Args1 args1, Args2 args2) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.interactedWith = interactedWith;
        this.method = method;
        this.args1 = args1;
        this.args2 = args2;
    }

    public String getCategoryName() {
        return name;
    }

    public Actor getActor() {
        return actor;
    }

    public Duration getDuration() {
        return duration;
    }

    public Usable getUsed() {
        return used;
    }

    public Actor getActorInteractedWith() {
        return interactedWith;
    }

    public void doAction() {
        method.doAction(args1, args2);
    }
}
