package controller.action;

import java.time.Duration;
import model.actors.Actor;
import model.device.Usable;

/**
 * Implementation of basic action without arguments.
 */
public class ActionNoArgs implements Action {
    private String name;

    private Actor actor;
    private Duration duration;

    private Usable used = null;
    private Actor interactedWith = null;

    private ActionMethodNoArg method;

    public ActionNoArgs(String name, Actor actor, Duration duration, ActionMethodNoArg method) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.method = method;
    }

    public ActionNoArgs(String name, Actor actor, Duration duration, Usable used, ActionMethodNoArg method) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.used = used;
        this.method = method;
    }

    public ActionNoArgs(String name, Actor actor, Duration duration, Actor interactedWith, ActionMethodNoArg method) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.interactedWith = interactedWith;
        this.method = method;
    }

    public String getCategoryName() {
        return name;
    }

    public Actor getActor() {
        return actor;
    }

    public Duration getDuration() {
        return duration;
    }

    public Usable getUsed() {
        return used;
    }

    public Actor getActorInteractedWith() {
        return interactedWith;
    }

    public void doAction() {
        method.doAction();
    }
}
