package controller.action;

/**
 * Action execution with one argument. Mostly created by lambda.
 */
public interface ActionMethodSingleArg<Args> {
    void doAction(Args args);
}
