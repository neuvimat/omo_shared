package controller.action;

import model.actors.Actor;
import model.device.Usable;
import utils.DurationUtil;

import java.time.Duration;

/**
 * Structure for saving history records of actions.
 */
public class ActionHistoryRecord implements Comparable<ActionHistoryRecord> {
    final private String actionName;
    final private Actor actor;
    final private Duration duration;
    final private Usable used;
    final private Actor interactedWith;
    final private Duration startAt;

    public ActionHistoryRecord(Action action, Duration startAt) {
        this.actionName = action.getCategoryName();
        this.actor = action.getActor();
        this.duration = action.getDuration();
        this.used = action.getUsed();
        this.interactedWith = action.getActorInteractedWith();
        this.startAt = startAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append(DurationUtil.format(startAt)).append(": ").append(actor.getName()).append(" - ").append(actionName).append(", duration: ").append(duration);
        if (used != null) sb.append(", used: ").append(used.getCategoryName());
        if (interactedWith != null) sb.append(", interacted with: ").append(interactedWith.getName());
        return sb.toString();
    }
    public String getActionName() {
        return actionName;
    }
    public Actor getActor() {
        return actor;
    }
    public Duration getDuration() {
        return duration;
    }
    public Usable getUsed() {
        return used;
    }
    public Actor getInteractedWith() {
        return interactedWith;
    }
    public Duration getStartAt() {
        return startAt;
    }

    @Override
    public int compareTo(ActionHistoryRecord o) {
        return startAt.compareTo(o.startAt);
    }
}
