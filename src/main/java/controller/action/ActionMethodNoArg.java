package controller.action;

/**
 * Action execution procedure. Mostly created by lambda.
 */
public interface ActionMethodNoArg {
    void doAction();
}
