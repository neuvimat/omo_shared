package controller.action;

/**
 * Action execution with two arguments. Mostly created by lambda.
 */
public interface ActionMethodTwoArgs<Args1, Args2> {
    void doAction(Args1 args1, Args2 args2);
}
