package controller.action;

import java.time.Duration;
import model.actors.Actor;
import model.device.Usable;
import utils.CategoryNamed;

/**
 * Represents some atomic action made by Actor.
 */
public interface Action extends CategoryNamed {
    /**
     * @return Actor that made this Action.
     */
    Actor getActor();

    /**
     * @return duration of action
     */
    Duration getDuration();

    /**
     * Device used in action.
     * @return null if no device used
     */
    Usable getUsed();

    /**
     * Actor interaction in action.
     * @return null if there was no interaction with another actor
     */
    Actor getActorInteractedWith();

    /**
     * Executes action.
     */
    void doAction();
}
