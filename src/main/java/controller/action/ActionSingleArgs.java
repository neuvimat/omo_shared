package controller.action;

import java.time.Duration;
import model.actors.Actor;
import model.device.Usable;

/**
 * Implementation of action with one argument.
 */
public class ActionSingleArgs<Args> implements Action{
    private String name;

    private Actor actor;
    private Duration duration;

    private Usable used = null;
    private Actor interactedWith = null;

    private ActionMethodSingleArg<Args> method;
    private Args args;

    public ActionSingleArgs(String name, Actor actor, Duration duration, ActionMethodSingleArg<Args> method, Args args) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.method = method;
        this.args = args;
    }

    public ActionSingleArgs(String name, Actor actor, Duration duration, Usable used, ActionMethodSingleArg<Args> method, Args args) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.used = used;
        this.method = method;
        this.args = args;
    }

    public ActionSingleArgs(String name, Actor actor, Duration duration, Actor interactedWith, ActionMethodSingleArg<Args> method, Args args) {
        this.name = name;
        this.actor = actor;
        this.duration = duration;
        this.interactedWith = interactedWith;
        this.method = method;
        this.args = args;
    }

    public String getCategoryName() {
        return name;
    }

    public Actor getActor() {
        return actor;
    }

    public Duration getDuration() {
        return duration;
    }

    public Usable getUsed() {
        return used;
    }

    public Actor getActorInteractedWith() {
        return interactedWith;
    }

    public void doAction() {
        method.doAction(args);
    }
}
