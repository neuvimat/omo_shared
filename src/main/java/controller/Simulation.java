package controller;

import java.time.Duration;

import view.*;

/**
 * Core controller managing actions execution of actors.
 */
public interface Simulation {

    /**
     * Does a single step in the simulation
     * @return True if another step is possible
     */
    boolean doStep();
    /**
     * Do steps until the simulation reaches the inputted time
     * @param duration time which to stop the simulation at
     * @return True if another step is possible
     */
    boolean doSteps(Duration duration);
    /**
     * Does n steps of the simulation
     * @param numberOfSteps number of steps to perform
     * @return True if another step is possible
     */
    boolean doSteps(int numberOfSteps);

    /**
     * Generates report of house structure.
     */
    void generateReport(HouseConfigurationReporter reporter);

    /**
     * Generates report of actions from simulation.
     */
    void generateReport(ActivityAndUsageReporter reporter);

    /**
     * Generates report of events that occurred in simulation.
     */
    void generateReport(EventReporter reporter);

    /**
     * Generates report from some date in some period of house in simulation.
     */
    void generateReport(ConsumptionReporter reporter);

    /**
     * Generates a report of all Livings and if they are alive. If not, says their time of death.
     * @param reporter
     */
    void generateReport(SurvivorReporter reporter);

}
