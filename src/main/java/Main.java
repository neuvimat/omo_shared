import controller.ActionDrivenSimulation;
import controller.Simulation;
import dao.JsonSimulationEnvironmentProvider;
import dao.StaticSimulationEnvironmentProvider;
import view.*;

import java.time.Duration;

public class Main {

    public static void main(String[] args) {
        final String filepath = "src/main/resources/houses/normalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofHours(24 * 150));

        ConsumptionCosts costs = new ConsumptionCosts(8, 6, 2);

        sim.generateReport(new BasicActivityAndUsageReporter(System.out));
        // sim.generateReport(new BasicSurvivorReporter(System.out));
        // sim.generateReport(new BasicConsumptionReporter(System.out, Duration.ZERO, Duration.ofDays(100), costs));
        // sim.generateReport(new BasicEventReporter(System.out));
        // sim.generateReport(new BasicHouseConfigurationReporter(System.out));
    }
}
