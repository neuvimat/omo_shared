package OLD.eventSystem;


public class Main
{
    class TestEvent implements Event
    {
        @Override
        public String getName()
        {
            return "TestEvent";
        }
    }

    // just for testing purpose in static context
    public TestEvent getEvent()
    {
        return new TestEvent();
    }

    public static void testEventListener(TestEvent event)
    {
        System.out.println("Listened to: " + event.getName());
    }

    public static void main(String[] args)
    {
        EventHandler<TestEvent> testEventHandler = new EventHandler<>();
        EventListener<TestEvent> listener = Main::testEventListener;
        testEventHandler.attach(listener);

        TestEvent event = new Main().getEvent();
        testEventHandler.execute(event);

        testEventHandler.detach(listener);
        testEventHandler.execute(event);
    }
}
