package OLD.eventSystem;

public interface EventListener<E extends Event>
{
    void update(E event);
}
