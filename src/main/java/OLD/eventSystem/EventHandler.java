package OLD.eventSystem;

import java.util.HashSet;
import java.util.Set;

public class EventHandler<E extends Event>
{
    private Set<EventListener<E>> listeners;

    public EventHandler()
    {
        listeners = new HashSet<>();
    }

    public void execute(E event)
    {
        for (EventListener<E> listener: listeners){
            listener.update(event);
        }
    }

    public void attach(EventListener<E> listener)
    {
        listeners.add(listener);
    }

    public void detach(EventListener<E> listener)
    {
        listeners.remove(listener);
    }
}
