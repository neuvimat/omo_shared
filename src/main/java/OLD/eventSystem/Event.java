package OLD.eventSystem;

public interface Event
{
    /**
     * Gets name of event for report purposes.
     * Name should be same for every instance of same class.
     * @return name of event class
     */
    String getName();
}
