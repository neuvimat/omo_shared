package OLD.devices;

import utils.RandomUtils;

abstract public class Device implements DeviceSubject, Interactable {
    private double deteriorationRateActual = 1; // % per hour;
    private double deteriorationRateBase = 1; // % per hour;
    private double health = 100;
    private boolean occupied = false;
    private long lastUpdate = 0;

    public Device() {
        randomizeDeterioration();
    }

    public Device(double deteriorationRate, double health, boolean occupied) {
        this.deteriorationRateBase = deteriorationRate;
        this.health = health;
        this.occupied = occupied;
        randomizeDeterioration();
    }

    public void randomizeDeterioration() {
        double variance = 2;
        deteriorationRateActual = deteriorationRateBase * (RandomUtils.getInstance().nextFloat() / variance + 1 - 1/variance/2);
    }

    public double getHealth() {
        return health;
    }
    public boolean isBroken() {
        return health < 0;
    }
    public boolean isOccupied() {
        return occupied;
    }

    @Override
    public void accept(DeviceVisitor v) {
        v.visit(this);
    }
}
