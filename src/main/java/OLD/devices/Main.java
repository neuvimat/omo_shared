package OLD.devices;

import java.util.PriorityQueue;

public class Main {
    public static void main(String[] args) {
        // ArrayList<Device> devices = new ArrayList<>();
        // devices.add(new FridgeModel());
        // devices.add(new Television());
        //
        // BasicVisitor v = new BasicVisitor();
        // System.out.println("Subject accepts visitor, calls visit on the visitor inside its accept.");
        //
        // for (Device d : devices) {
        //     d.accept(v);
        // }

        // ==============================
        PriorityQueue<Integer> q = new PriorityQueue<>();
        q.add(260);
        q.add(0);
        q.add(265);
        q.add(200);
        q.add(10);
        q.add(15);
        q.add(225);
        q.add(55);

        System.out.println("First for:");
        for (Integer i : q) {
            System.out.println("Integer: " + i);
        }
        System.out.println("Second for:");
        for (Integer i : q) {
            System.out.println("Integer: " + i);
        }
    }
}
