package OLD.devices;

import java.util.PriorityQueue;

public class Consumption {
    // Todo: btw, what unit is that? Some arbitrary?
    private int actual;
    private int on;
    private int off;
    private int idle;
    private ConsumptionType consumptionType;

    public Consumption(int on, int off, int idle, ConsumptionType consumptionType) {
        this.on = on;
        this.off = off;
        this.idle = idle;
        this.consumptionType = consumptionType;
    }

    // Todo: do we need to store consumption's on/off/idle tho? The device itself can set these things from inside code
    // Todo: this way, it can even be more variable, such as adding consumption based on light intensity
    public void setActualConsumption(int consumption) {

    }

    // Todo: fix this
    private PriorityQueue<Integer> history;
}
