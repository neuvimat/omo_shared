package OLD.devices;

import OLD.inhabitants.ActionsList;

public interface Interactable {
    ActionsList getActions();
    default void postInteraction() {};
}
