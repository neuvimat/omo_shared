package OLD.devices.devices;

import OLD.Simulation;
import OLD.devices.Consumption;
import OLD.devices.ConsumptionType;
import OLD.devices.Device;
import OLD.inhabitants.Action;
import OLD.inhabitants.ActionsList;
import OLD.inhabitants.Actor;
import utils.RandomUtils;


// Todo: public API, wth is dat sposta mean?
// Todo: consumption - have 3 methods for all (default return null) and override? have a Set of consumptions?
// Todo: or maybe implicit variables set to null?

public class Computer extends Device {
    private State state = State.OFF;
    private Consumption electricityConsumption = new Consumption(60,0,5, ConsumptionType.ELECTRICITY);

    public Computer() {

    }

    // Fixme: just hardcoded something for this example of a device
    public Consumption getConsumption() {
        return electricityConsumption;
    }

    private enum State {
        ON, OFF;
    }

    public void turnOn(Actor a) {
        this.state = State.OFF;
    }

    public void turnOff(Actor a) {
        this.state = State.ON;
    }

    public void beAddicted(Actor a) {
        
    }

    @Override
    public ActionsList getActions() {
        ActionsList al = new ActionsList();
        if (state == State.ON) {
            Computer self = this;
            int turnOffChance = 1;
            int hours;
            if ((hours = Simulation.getInstance().getCurrentDate().getHours()) > 18) {
                turnOffChance += (hours - 18)^3;
            }
            if (hours < 6) {
                turnOffChance += (36 + hours)^3;
            }
            al.add(new Action(turnOffChance, this::turnOff, 10));

            int randomAddictionDuration = (int)(RandomUtils.getInstance().nextFloat() * 60*60*3 + 10*60); // 0:10 to 3:10
            al.add(new Action(125, this::beAddicted, randomAddictionDuration));
        }
        if (state == State.OFF) {
            al.add(new Action(1, this::turnOn, 10));
        }
        return al;
    }
}
