package OLD.devices;

import OLD.devices.devices.*;

public interface DeviceVisitor {
    void visit(DeviceSubject s);

    default void visit(Computer f) {};
    default void visit(Cooker f) {};
    default void visit(Fridge f) {};
    default void visit(Light f) {};
    default void visit(Switch f) {};
    default void visit(Television f) {};
    default void visit(WashingMachine f) {};
    default void visit(Window f) {};
}
