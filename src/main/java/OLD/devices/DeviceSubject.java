package OLD.devices;

public interface DeviceSubject {
    void accept(DeviceVisitor v);
}
