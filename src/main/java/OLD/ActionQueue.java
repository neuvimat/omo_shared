package OLD;

import OLD.inhabitants.Actor;

import java.util.Optional;
import java.util.PriorityQueue;

public class ActionQueue {
    private PriorityQueue<ActionQueueEntry> queue;

    public ActionQueue() {
        this.queue = new PriorityQueue<>();
    }

    public long handle() {
        ActionQueueEntry entry = queue.poll();
        entry.getAction().doAction(entry.getActor());
        entry.getActor().actionDoneCallback();
        return entry.getAction().getDuration();
    }

    public void add(ActionQueueEntry aqe) {
        queue.add(aqe);
    }

    /**
     * Finds an entry based on actor
     * @param a actor to look for
     * @param triggerCallback false to disable callback trigger
     * @return true if found, false if it wasn't present
     */
    public boolean removeByActor(Actor a, boolean triggerCallback) {
        Optional<ActionQueueEntry> opt  = queue.stream()
                .filter((e)->e.getActor() == a)
                .findFirst();
        if (opt.isPresent()) {
            queue.remove(opt.get());
            if (triggerCallback) opt.get().getActor().interruptActionCallback();
            return true;
        }
        return false;
    }

    public PriorityQueue<ActionQueueEntry> getQueue() {
        return queue;
    }
}
