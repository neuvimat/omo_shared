package OLD;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Simulation sim = Simulation.getInstance();
        sim.initialize("regularHouse", null, 60 * 60 * 24 * 7);
        sim.start();
    }
}
