package OLD.reports;

import OLD.Simulation;

import java.text.SimpleDateFormat;
import java.util.Date;

abstract public class Report {
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd - HH:mm:ss");
    private Date date;

    // ===========================
    // Abstract methods that need to be implemented
    abstract public String getReport();


    // ===========================
    // Predefined methods
    public Report() {
        this.date = Simulation.getInstance().getCurrentDate();
    }

    public Date getDate() {
        // Immutable
        return (Date)date.clone();
    }

    protected String getFormattedDate() {
        return getFormattedDate(true);
    }

    protected String getFormattedDate(boolean suffix) {
        if (suffix) return formatter.format(date) + " - ";
        else return formatter.format(date);
    }
}
