package OLD.reports;

public class StringReport extends Report {
    private String message;

    public StringReport(String message) {
        super();
        this.message = message;
    }

    @Override
    public String getReport() {
        return getFormattedDate() + " - " + message;
    }
}
