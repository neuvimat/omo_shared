package OLD;

import OLD.house.House;
import OLD.house.HouseBuilder;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

public class Simulation {
    private long seconds = 0;
    private Calendar time;
    private Calendar end;
    private boolean running = false;
    private Logger logger = Logger.getLogger("");
    private ActionQueue actionQueue;

    private House house = new House();

    public boolean initialize(String filePath, Calendar time,  int duration) {
        if (!running) {
            if (time == null) {
                this.time = new GregorianCalendar(2018, 12, 1, 8,0,0);
            }
            else {
                this.time = time;
            }
            this.end = ((Calendar)this.time.clone());
            end.add(Calendar.SECOND, duration);

            HouseBuilder houseBuilder = new HouseBuilder(house);
            houseBuilder.buildHouse(filePath);

            return true;
        }
        else {
            logger.warning("Trying to initialize OLD.simulation while it is already running!");
            return false;
        }
    }

    public void start() {

    }

    private void doStep() {

    }

    public void runSimulation() {

    }

    public void doOneStep() {
        doStep();
    }

    public void doSteps(int steps) {

    }

    public Date getCurrentDate() {
        return time.getTime();
    }
    public long getSeconds() {
        return seconds;
    }

    @Deprecated
    /**
     * Use for tests only cuz it's hard to mock singleton
     */
    public void setSeconds(long s) {
        this.seconds = s;
    }

    private static Simulation ourInstance = new Simulation();
    public static Simulation getInstance() {return ourInstance;}
    private Simulation() {

    }
}
