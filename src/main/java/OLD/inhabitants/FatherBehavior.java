package OLD.inhabitants;

import OLD.inhabitants.behaviors.Behavior;

public class FatherBehavior extends Behavior {
    int timeSpendInOneRoom = 0;

    public FatherBehavior(int timeSpendInOneRoom) {
        this.timeSpendInOneRoom = timeSpendInOneRoom;
    }

    @Override
    public void think() {
        if (mustSleep()) {
            if (seekBed()) {
                // teleport to bed and make sleeping action
            }
            else {
                // sleep on the spot
            }
            return;
        }
        if (mustEat()) {
            if (seekFoodSource()) {
                // guzzle yourself
            }
            else {
                // guess I'll just die
//                                              .,,,,,.
//                                           ,*/(/////(/**.
//                                        ,/*************//**.
//                                      ************* *////(/*.
//                                      **************/////((/*
//                                      **************////(((//*
//                                      ***(((/*****/(#(#((((//*,
//                                      .,****///*///((((//(((/(##(
//                                      ***//(((/*/(//*#(//((##(//
//                                      */****///*/(///////(((#(//
//                                      ,*//***//*/((/////(((#((/
//                                       *////////(###(//((((#(*
//                                       ,//////////((/(#((#%/*,..,//**,.
//                                    .,*(///(((((###(/(((#%%%#############(
//                                /((#######/*///(/((((//(((%%&%###############,
//                                *(###########/*****/*/*/(##%%###########%#####*
//                             *###############(****///(%%%%%#############%######,
//                            *##############%####%%%%%%%%%%#%%###########%%####%#
//                            (#################%#%%%%%%#%%#%%############%%#####%
//                            (######################%%%%##%#########%###%%%######
//                            ###########################%%##########%###%%%######
//                           .#########################%############%%##%%%%##%%##
//                           ,##%####################%##############%%##&%%#%%%#
//                           *#%%##################################%%%##%%%###%%
//                 ,         /#%%######################%##########%%%%##%%%&%#####
//                 **//,     #%%%%###################%%#########*,***###%&%%%#%%##
//                 ***//##  /%%%%%############################****/(/(##%&%%%%%%#%
//                 ******/#%#%%%%%##########/#/#/#(######(#/(****/(#%%##%%%%%#####
//                 ** . (./(,%..,% .*( .(### ##% % %##(  .( (.  */(#%%%#%%&%%%%%##
//                 /( #(@.%/,*/&(&&( #&(,### ### % #,((./ (.(*%/#%%&%%%%%%%%%%%%
//                 .# #((%@&@%@&@&@%&%@%@#####%&%##@(&/#(#%#&(%%%&&%%%%%%%%%%%
            }
            return;
        }
        // No need for specific action, do some random shit
        doRandomShit();
    }

    @Override
    public void interruptAction() {

    }

    @Override
    public void interruptTask() {

    }

    private void doRandomShit() {
        // multiply ranom by *100 so we don't have to write decimal numbers in if
        // Increase the chance to leave a room by 5% an hour, with minimal chance of 5%
        if (Math.random()*100 < 5 +  timeSpendInOneRoom / 720) {
            // actor.goToRandomRoom();
        }
        // fixme: commented example will not work as intented
        // if (actor.getRoom().getDevices().getRandomDevice().getActionsList().doRandomAction()) {
        //
        // };
        // else {
        //     // if there is no suitable device in the room, either do nothing for an hour (skyrim wait)
        //     // or keep seeking an usable device
        // }
    }

    private boolean seekBed() {
        return false;
    }

    private boolean seekFoodSource() {
        return false;
    }

    private boolean mustEat() {
        // actor.hunger  < 10;
        return false;
    }

    private boolean mustSleep() {
        // actor.sleep  < 15;
        return false;
    }
}
