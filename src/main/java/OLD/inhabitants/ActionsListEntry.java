package OLD.inhabitants;

public class ActionsListEntry {
    private Action action;
    private int chance;

    public ActionsListEntry(Action action, int chance) {
        this.action = action;
        this.chance = chance;
    }
    public Action getAction() {
        return action;
    }
    public int getChance() {
        return chance;
    }
    public int getOuterBound() {
        return chance + action.getRatio();
    }
}
