package OLD.inhabitants;

import OLD.Simulation;

abstract public class Actor {
    protected long lastThink;

    public Actor() {
        lastThink = Simulation.getInstance().getSeconds();
    }

    abstract public void think();

    /**
     * Finds if this actor is in ActionQueue, if yes, remove it from it
     */
    final public void interruptAction() {
        Simulation.getInstance();
        interruptActionCallback();
    }
    public void interruptActionCallback() {
        // override me in children if needed
    }

    public void actionDoneCallback() {

    }
}
