package OLD.inhabitants;

import OLD.inhabitants.behaviors.LivingBehavior;

public class Human extends LivingActor {
    protected LivingBehavior behavior;

    public Human(String name, double hungerMax, double hungerDrain, double sleepMax, double sleepDrain, LivingBehavior behavior) {
        super(name, hungerMax, hungerDrain, sleepMax, sleepDrain);
        this.behavior = behavior;
        behavior.setActor(this);
    }

    @Override
    public void think() {
        behavior.think();
    }

    public void interruptActionCallback() {
        think();
    }

    public void interruptTask() {
        behavior.interruptTask();
    }
}
