package OLD.inhabitants;

import java.util.ArrayList;
import java.util.Random;

public class ActionsList {
    private static Random random = new Random();
    ArrayList<ActionsListEntry> list = new ArrayList<>();

    public void add(Action a) {
        // todo: add chance calculation
        int chance = 10;
        ActionsListEntry ale = new ActionsListEntry(a, chance);
    }

    public Action getRandomAction() {
        ActionsListEntry last = list.get(list.size()-1);
        int rng = random.nextInt(last.getChance() + last.getAction().getRatio());

        for (ActionsListEntry entry : list) {
            if (entry.getOuterBound() > rng) {
                return entry.getAction();
            }
        }
        return null;
        // Todo: add binary search instead of linear?
    };
}
