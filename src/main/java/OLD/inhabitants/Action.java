package OLD.inhabitants;

public class Action implements Comparable<Action> {
    private int ratio;
    private int duration; // in seconds
    private ActionPseudoPointer action;

    public void doAction(Actor actor) {
        action.act(actor);
    }
    public int getRatio() {
        return ratio;
    }

    public Action(int ratio, ActionPseudoPointer action, int duration) {
        this.ratio = ratio;
        this.action = action;
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public int compareTo(Action o) {
        return ratio - o.ratio;
    }
}
