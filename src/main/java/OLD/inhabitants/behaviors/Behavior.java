package OLD.inhabitants.behaviors;

abstract public class Behavior {
    abstract public void think();
    abstract public void interruptAction();
    abstract public void interruptTask();
}
