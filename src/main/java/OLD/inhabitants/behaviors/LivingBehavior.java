package OLD.inhabitants.behaviors;

import OLD.inhabitants.LivingActor;

public class LivingBehavior extends Behavior {
    private LivingActor actor;

    public LivingBehavior() {
        super();
    }

    public void setActor(LivingActor a) {
        if (actor == null) {
            actor = a;
        }
    }

    @Override
    public void think() {

    }

    @Override
    public void interruptAction() {

    }

    @Override
    public void interruptTask() {

    }

    private boolean mustEat() {
        return actor.getHungerPercentage() < .5;
    }

    private boolean mustSleep() {
        return actor.getSleepPercentage() < .2;
    }

    private void doRandomAction() {

    }
}
