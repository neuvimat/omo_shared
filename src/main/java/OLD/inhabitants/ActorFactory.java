package OLD.inhabitants;

import OLD.inhabitants.behaviors.LivingBehavior;

public class ActorFactory {
    public Human makeFather(String name, LivingBehavior b) {
        return new Human(name, 150, 3.25, 100, 4.1, b);
    }
    public Human makeMother(String name, LivingBehavior b) {
        return new Human(name, 100, 2.145, 100, 4.1, b);
    }
    public Human makeKid(String name, LivingBehavior b) {
        return new Human(name, 75, 1.60875, 100, 5.2, b);
    }
    // Eats once in a while but a lot
    public Human makeFoodStorage(String name, LivingBehavior b) {
        return new Human(name, 500, 3.5, 100, 4.1, b);
    }
}
