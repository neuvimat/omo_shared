package OLD.utility;

public class Random {
    /**
     * Generates random int in interval [from, to] (including both)
     * @return
     */
    public static int getInt(int from, int to) {
        return (int)Math.floor(Math.random() * to-from+1) + from;
    }
}
