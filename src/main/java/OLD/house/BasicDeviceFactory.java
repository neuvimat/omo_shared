package OLD.house;

import OLD.devices.Device;
import OLD.devices.devices.*;

public class BasicDeviceFactory implements AbstractDeviceFactory {

    @Override
    public Device makeComputer() {
        return new Computer();
    }

    @Override
    public Device makeCooker() {
        return new Cooker();
    }

    @Override
    public Device makeFridge() {
        return new Fridge();
    }

    @Override
    public Device makeLight() {
        return new Light();
    }

    @Override
    public Device makeTelevision() {
        return new Television();
    }

    @Override
    public Device makeWashingMachine() {
        return new WashingMachine();
    }

    @Override
    public Device makeWindow() {
        return null;
    }
}
