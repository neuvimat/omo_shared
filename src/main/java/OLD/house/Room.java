package OLD.house;

import OLD.devices.Device;

import java.util.HashSet;
import java.util.Iterator;

public class Room {
    private HashSet<Device> devices;
    private HashSet<Room> connectedRooms;
    private Floor floor;

    public Room() {
        devices = new HashSet<>();
        connectedRooms = new HashSet<>();
    }
    public Room(Floor floor) {
        devices = new HashSet<>();
        connectedRooms = new HashSet<>();
        this.floor = floor;
    }

    public void connectWith(Room room) {
        connectWith(room, true);
    }
    public void connectWith(Room room, boolean reciprocal) {
        connectedRooms.add(room);
        if (!reciprocal)
            room.connectWith(this, false);
    }

    public void disconnectWith(Room room) {
        disconnectWith(room, true);
    }
    public void disconnectWith(Room room, boolean reciprocal) {
        connectedRooms.remove(room);
        if (!reciprocal)
            room.disconnectWith(this, false);
    }

    public void addDevice(Device device) {
        devices.add(device);
    }

    public void removeDevice(Device device) {
        devices.remove(device);
    }

    public void clearDevices() {
        devices = new HashSet<>();
    }

    public void clearRoomConnections() {
        clearRoomConnections(true);
    }
    public void clearRoomConnections(boolean reciprocal) {
        Iterator<Room> it = connectedRooms.iterator();

        if (reciprocal) {
            while(it.hasNext()) {
                it.next().connectedRooms.remove(this);
                it.remove();
            }
        }
        else {
            while(it.hasNext()) {
                it.remove();
            }
        }
    }

    public Floor getFloor() {
        return floor;
    }
    public void setFloor(Floor floor) {
        this.floor = floor;
    }
    public HashSet<Device> getDevices() {
        return devices;
    }
    public HashSet<Room> getConnectedRooms() {
        return connectedRooms;
    }
}
