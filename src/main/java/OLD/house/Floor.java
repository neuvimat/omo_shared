package OLD.house;

import OLD.utility.Random;

import java.util.ArrayList;
import java.util.logging.Logger;

public class Floor {
    ArrayList<Room> rooms;

    public Floor() {
        rooms = new ArrayList<>();
    }

    public void addRoom(Room room) {
        rooms.add(room);

        if (room.getFloor() != null) {
            room.getFloor().rooms.remove(room);
        }
        room.setFloor(this);
    }

    public void removeRoom(Room room) {
        for (Room r : room.getConnectedRooms()) {
            r.disconnectWith(room);
        }
        rooms.remove(room);
    }

    /**
     * Randomly generates connections between rooms on the floor
     * Every room will end up with at least one connection
     * It is guaranteed that it will be possible to get to any room from any room
     * Also: Method name is probably too long, but w/e ¯\_(ツ)_/¯
     */
    public void generateConnectionsBetweenRooms() {
        // First, connect them in a way that does not allow creation of islands
        // Second, randomly connect rooms
        int[] CONNECTION_CHANCES = {100, 30, 10};
        ArrayList<Room> islands = new ArrayList<>(rooms);
        for (Room r : rooms) {
            islands.remove(r);
            Room randomRoom = islands.get(Random.getInt(0,islands.size()-1));
            if (randomRoom != null) {
                islands.remove(randomRoom);
            }
        }
    }

    /**
     * Randomly selects two rooms, one from each floor, and connects them together
     * Can be called multiple times on the same floor, it will try to connect yet unconnected rooms together
     * If all rooms have at least one connection to the floor, do nothing
     */
    public void generateConnectionBetweenFloor(Floor f) {
        if (f == this) {
            Logger.getLogger("").warning("Trying to connect a floor to itself!");
            return;
        }
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }
}
