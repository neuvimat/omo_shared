package OLD.house;

public interface AbstractRoomFactory {
    void setDeviceFactory(AbstractDeviceFactory factory);

    Room makeKitchen();
    Room makeBathroom();
    Room makeBedroom();
    Room makeLivingRoom();
    Room makeStudyRoom();
    Room makeEmptyRoom();
}
