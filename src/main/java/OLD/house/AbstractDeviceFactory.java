package OLD.house;

import OLD.devices.Device;

public interface AbstractDeviceFactory {
    Device makeComputer();
    Device makeCooker();
    Device makeFridge();
    Device makeLight();
    Device makeTelevision();
    Device makeWashingMachine();
    Device makeWindow();
}
