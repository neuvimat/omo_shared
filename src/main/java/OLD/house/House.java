package OLD.house;

import OLD.inhabitants.LivingActor;

import java.util.ArrayList;

/**
 * Let's say the OLD.house has only one staircase, meaning that every floor is connected to any floor.
 */
public class House {
    private ArrayList<Floor> floors;
    private ArrayList<LivingActor> inhabitants;

    public void addFloor(Floor floor) {
        floors.add(floor);
    }

    @Deprecated
    public void removeFloor() {
        // todo: implement me, senpai
        // well we will hardly remove floors from the house, no?
        // and implementing this cleanly without breaking something is not worth the hassle
        // fuck this method
    }

    public ArrayList<Floor> getFloors() {
        return floors;
    }

    public ArrayList<Room> getAllRooms() {
        ArrayList<Room> rooms = new ArrayList<>();
        for (Floor f : floors) {
                rooms.addAll(f.getRooms());
        }
        return rooms;
    }

    public void addInhabitant(LivingActor a) {
        inhabitants.add(a);
    }

    public ArrayList<LivingActor> getInhabitants() {
        return inhabitants;
    }
}
