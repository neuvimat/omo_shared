package OLD.house;

import OLD.inhabitants.Actor;
import OLD.inhabitants.ActorFactory;
import OLD.inhabitants.LivingActor;
import OLD.inhabitants.behaviors.LivingBehavior;
import OLD.utility.Random;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

@SuppressWarnings("WeakerAccess")
public class HouseBuilder {
    private House house;
    private JsonParser parser;

    private AbstractRoomFactory roomFactory;
    private ActorFactory humanFactory = new ActorFactory();

    public HouseBuilder(House house) {
        this.house = house;
    }

    public void buildHouse(String filePath) {
        if ("".equals(filePath)) {
            buildDummyHouse();
        }
        else {
            try {
                buildJsonHouse(filePath);
            } catch (Exception e) {
                e.printStackTrace();
                buildDummyHouse();
            }
        }
    }

    public void buildDummyHouse() {
        roomFactory = new BasicRoomFactory();

        buildDummyFloors();
        buildDummyInhabitants();
        distributeInhabitantsRandomly();
    }

    private void distributeInhabitantsRandomly() {
        ArrayList<Room> rooms = house.getAllRooms();
        for (LivingActor a : house.getInhabitants()) {
            a.setRoom(rooms.get(Random.getInt(0, rooms.size() - 1)));
        }
    }

    private void buildDummyInhabitants() {
        // todo: uncomment me when ready
        // house.addInhabitant(humanFactory.makeFather("Henry", new LivingBehavior()));
        // house.addInhabitant(humanFactory.makeMother("Antoinette", new LivingBehavior()));
        // house.addInhabitant(humanFactory.makeKid("Billy", new LivingBehavior()));
        // house.addInhabitant(humanFactory.makeKid("Billietta", new LivingBehavior()));
        // house.addInhabitant(humanFactory.makeFather("Seymour", new LivingBehavior()));
    }

    private void buildDummyFloors() {
        Floor f1 = new Floor();
        f1.addRoom(roomFactory.makeLivingRoom());
        f1.addRoom(roomFactory.makeBathroom());
        f1.addRoom(roomFactory.makeKitchen());
        f1.addRoom(roomFactory.makeBedroom());
        Floor f2 = new Floor();
        f2.addRoom(roomFactory.makeStudyRoom());
        f2.addRoom(roomFactory.makeBedroom());
        f2.addRoom(roomFactory.makeBathroom());

        f1.generateConnectionsBetweenRooms();
        f2.generateConnectionsBetweenRooms();

        f1.generateConnectionBetweenFloor(f2);
    }


    //===================================
    // BEWARE: JSON BELOW!
    //===================================
    private void buildJsonHouse(String filePath) throws Exception {
        JsonObject jsonHouse = loadJsonData(filePath);

        prepareFactories(jsonHouse);
        buildJsonFloors(jsonHouse);

        JsonArray actors = jsonHouse.get("actors").getAsJsonArray();
        for (JsonElement e : actors) {
            e.getAsString();
        }

        distributeInhabitantsRandomly();
    }

    private void buildJsonFloors(JsonObject jsonHouse) {
        JsonArray floors = jsonHouse.getAsJsonArray("floors");
        for (JsonElement floor : floors) {
            buildJsonFloor(floor);
        }
        connectJsonFloors();
    }

    private void prepareFactories(JsonObject jsonHouse) {
        roomFactory = jsonSetRoomFactory(jsonHouse.get("deviceFactory").getAsString());
        AbstractDeviceFactory deviceFactory = jsonSetDeviceFactory(jsonHouse.get("deviceFactory").getAsString());
        roomFactory.setDeviceFactory(deviceFactory);
    }

    private void buildJsonFloor(JsonElement _floor) {
        Floor floor = new Floor();
        house.addFloor(floor);
        for (JsonElement r : _floor.getAsJsonArray()) {
            floor.addRoom(jsonMakeRoom(r.getAsString()));
        }
        floor.generateConnectionsBetweenRooms();
    }

    private void connectJsonFloors() {
        int floorsCount = house.getFloors().size();
        if (floorsCount > 1) {
            for (int i = 1; i < floorsCount; i++) {
                house.getFloors().get(i).generateConnectionBetweenFloor(house.getFloors().get(i - 1));
            }
        }
    }

    private JsonObject loadJsonData(String filePath) throws IOException {
        FileReader fr = new FileReader("src/main/resources/behaviors/father.json");
        JsonObject JsonHouse = parser.parse(fr).getAsJsonObject();
        fr.close();

        return JsonHouse;
    }

    private AbstractRoomFactory jsonSetRoomFactory(String json) {
        if (json == null) return new BasicRoomFactory();
        ;
        switch (json) {
            case "rich":
                return new BasicRoomFactory(); // todo: Replace with rich
            case "basic":
                return new BasicRoomFactory();
            default:
                Logger.getLogger("").warning("Badly structured JSON house config. Could not match property \'roomFactory\'");
                return new BasicRoomFactory();
        }
    }

    private AbstractDeviceFactory jsonSetDeviceFactory(String json) {
        if (json == null) return new BasicDeviceFactory();
        ;
        switch (json) {
            case "rich":
                return new BasicDeviceFactory(); // todo: Replace with rich
            case "basic":
                return new BasicDeviceFactory();
            default:
                Logger.getLogger("").warning("Badly structured JSON house config. Could not match property \'roomFactory\'");
                return new BasicDeviceFactory();
        }
    }

    private Room jsonMakeRoom(String json) {
        switch (json) {
            case "bathroom":
                return roomFactory.makeBathroom();
            case "bedroom":
                return roomFactory.makeBedroom();
            case "kitchen":
                return roomFactory.makeKitchen();
            case "livingroom":
                return roomFactory.makeLivingRoom();
            case "studyroom":
                return roomFactory.makeStudyRoom();
            default:
                Logger.getLogger("").warning("Could to match room identifier - \'" + json + "\', creating empty room instead!");
                return roomFactory.makeEmptyRoom();
        }
    }

    private Actor jsonMakeActor(String type, String name) {
        switch (type) {
            case "father":
                return humanFactory.makeFather(name, new LivingBehavior());
            default:
                return null;
        }
    }
}
// todo: finish actor build
