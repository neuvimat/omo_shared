package OLD.house;

public class BasicRoomFactory implements AbstractRoomFactory {
    private AbstractDeviceFactory deviceFactory;

    public BasicRoomFactory() {
        deviceFactory = new BasicDeviceFactory();
    }

    public BasicRoomFactory(AbstractDeviceFactory deviceFactory) {
        this.deviceFactory = deviceFactory;
    }

    @Override
    public Room makeKitchen() {
        Room r = new Room();
        r.addDevice(deviceFactory.makeCooker());
        r.addDevice(deviceFactory.makeLight());
        return r;
    }

    @Override
    public Room makeBathroom() {
        return null;
    }

    @Override
    public Room makeBedroom() {
        return null;
    }

    @Override
    public Room makeLivingRoom() {
        Room r = new Room();
        r.addDevice(deviceFactory.makeTelevision());
        r.addDevice(deviceFactory.makeLight());
        r.addDevice(deviceFactory.makeLight());
        return r;
    }

    @Override
    public Room makeStudyRoom() {
        return null;
    }

    @Override
    public Room makeEmptyRoom() {
        Room r = new Room();
        return r;
    }

    public AbstractDeviceFactory getDeviceFactory() {
        return deviceFactory;
    }
    public void setDeviceFactory(AbstractDeviceFactory deviceFactory) {
        this.deviceFactory = deviceFactory;
    }
}
