package OLD.house;

import OLD.devices.Device;
import OLD.inhabitants.Actor;

public interface HouseVisitor {
    void visit(House room);
    void visit(Floor room);
    void visit(Room room);
    void visit(Device room);
    void visit(Actor room);
}
