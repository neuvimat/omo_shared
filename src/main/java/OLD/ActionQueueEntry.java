package OLD;

import OLD.inhabitants.Action;
import OLD.inhabitants.Actor;

public class ActionQueueEntry implements Comparable<ActionQueueEntry> {
    private Action action;
    private Actor actor;
    private long seconds;

    public ActionQueueEntry(Action action, Actor actor) {
        this.action = action;
        this.actor = actor;
        this.seconds = action.getDuration() + Simulation.getInstance().getSeconds();
    }

    public Action getAction() {
        return action;
    }

    public Actor getActor() {
        return actor;
    }

    @Override
    public int compareTo(ActionQueueEntry o) {
        return (int)(this.seconds - o.seconds);
    }
}
