package dao.matchers;

import javafx.css.Match;
import model.actors.Living;
import model.configuration.House;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class AbstractMatcher <T> implements Matcher<T> {
    protected Map<String, makeMethod> map = new HashMap<>();

    public Set<String> getMappedKeywords() {
        return map.keySet();
    }

    protected interface makeMethod <T> {
        // fixme: T is not of the AbstractMatcher <T>
        T makeMethod();
    }
}
