package dao.matchers;

import dao.factories._interfaces.AbstractLivingFactory;
import model.actors.Living;
import model.configuration.House;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LivingMatcher implements Matcher<Living> {
    private Map<String, makeMethod> map = new HashMap<>();

    private House house;

    public LivingMatcher(AbstractLivingFactory abstractLivingFactory, House house) {
        map.put("father", abstractLivingFactory::makeFather);
        map.put("mother", abstractLivingFactory::makeMother);
        map.put("son", abstractLivingFactory::makeSon);
        map.put("daughter", abstractLivingFactory::makeDaughter);
        map.put("grandfather", abstractLivingFactory::makeGrandfather);
        map.put("grandmother", abstractLivingFactory::makeGrandmother);
        map.put("saboteur", abstractLivingFactory::makeSaboteur);
        map.put("dog", abstractLivingFactory::makeDog);
        map.put("cat", abstractLivingFactory::makeCat);
        this.house = house;
    }

    public Living match(String livingString) {
        makeMethod matched = map.get(livingString);
        if (matched == null) return null;
        Living living = matched.makeLiving(house);
        return living;
    }

    public Set<String> getMappedKeywords() {
        return map.keySet();
    }

    private interface makeMethod {
        Living makeLiving(House house);
    }
}
