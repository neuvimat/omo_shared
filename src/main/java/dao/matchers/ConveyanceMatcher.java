package dao.matchers;

import dao.factories._interfaces.AbstractConveyanceFactory;
import model.conveyance.Conveyance;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConveyanceMatcher implements Matcher<Conveyance> {
    private Map<String, makeMethod> map = new HashMap<>();

    public ConveyanceMatcher(AbstractConveyanceFactory abstractConveyanceFactory) {
        map.put("bicycle", abstractConveyanceFactory::makeBicycle);
        map.put("car", abstractConveyanceFactory::makeCar);
        map.put("ski", abstractConveyanceFactory::makeSki);
    }

    /**
     * Tries to match the corresponding string to its java class counterpart and return its instance
     * @param conveyanceString
     * @return
     */
    public Conveyance match(String conveyanceString) {
        makeMethod matched = map.get(conveyanceString);
        if (matched == null) return null;
        return matched.makeConveyance();
    }

    public Set<String> getMappedKeywords() {
        return map.keySet();
    }

    private interface makeMethod {
        Conveyance makeConveyance();
    }
}
