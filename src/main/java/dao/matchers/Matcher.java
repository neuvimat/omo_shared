package dao.matchers;

import java.util.HashMap;

public interface Matcher<T> {
    /**
     * Tries to match the inputted string to its Java Class counterpart
     * @param code code to try to find counterpart to
     * @return new instance of class corresponding to the code
     */
    T match(String code);
}
// todo: maybe finish this later