package dao.matchers;

import dao.factories._interfaces.AbstractRoomFactory;
import model.configuration.Room;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RoomMatcher implements Matcher<Room> {
    private Map<String, makeMethod> map = new HashMap<>();

    public RoomMatcher(AbstractRoomFactory abstractRoomFactory) {
        map.put("bathroom", abstractRoomFactory::makeBathroom);
        map.put("bedroom", abstractRoomFactory::makeBedroom);
        map.put("empty_room", abstractRoomFactory::makeEmptyRoom);
        map.put("kitchen", abstractRoomFactory::makeKitchen);
        map.put("living_room", abstractRoomFactory::makeLivingRoom);
        map.put("study", abstractRoomFactory::makeStudy);
        map.put("office", abstractRoomFactory::makeOffice);
    }

    public Room match(String roomString) {
        makeMethod matched = map.get(roomString);
        if (matched == null) return null;
        Room room = matched.makeRoom();
        return room;
    }

    public Set<String> getMappedKeywords() {
        return map.keySet();
    }

    private interface makeMethod {
        Room makeRoom();
    }
}
