package dao.matchers;

import model.configuration.House;
import model.sensor.HusbandrySensor;
import model.sensor.Sensor;
import model.sensor.TemperatureSensor;
import model.sensor.WindSensor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SensorMatcher implements Matcher<Sensor> {
    private Map<String, makeMethod> map = new HashMap<>();

    private House house;

    public SensorMatcher(House house) {
        map.put("wind", WindSensor::new);
        map.put("temperature", TemperatureSensor::new);
        map.put("husbandry", HusbandrySensor::new);
        this.house = house;
    }

    public Sensor match(String sensorString) {
        makeMethod matched = map.get(sensorString);
        if (matched == null) return null;
        Sensor sensor = matched.makeSensor(house);
        return sensor;
    }

    public Set<String> getMappedKeywords() {
        return map.keySet();
    }

    private interface makeMethod {
        Sensor makeSensor(House house);
    }
}
