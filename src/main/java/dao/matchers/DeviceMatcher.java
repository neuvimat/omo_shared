package dao.matchers;

import dao.factories._interfaces.AbstractDeviceFactory;
import model.device.Device;

import java.util.*;

public class DeviceMatcher implements Matcher<Device> {
    private Map<String, makeMethod> map = new HashMap<>();

    public DeviceMatcher(AbstractDeviceFactory abstractDeviceFactory) {
        map.put("computer", abstractDeviceFactory::makeComputer);
        map.put("cooker", abstractDeviceFactory::makeCooker);
        map.put("fridge", abstractDeviceFactory::makeFridge);
        map.put("heating", abstractDeviceFactory::makeHeating);
        map.put("light", abstractDeviceFactory::makeLight);
        map.put("television", abstractDeviceFactory::makeTelevision);
        map.put("washing_machine", abstractDeviceFactory::makeWashingMachine);
        map.put("window", abstractDeviceFactory::makeWindow);
    }

    public Device match(String deviceString) {
        makeMethod matched = map.get(deviceString);
        if (matched == null) return null;
        return matched.makeDevice();
    }

    public Set<String> getMappedKeywords() {
        return map.keySet();
    }

    private interface makeMethod {
        Device makeDevice();
    }
}
