package dao;

import dao.factories._interfaces.AbstractConveyanceFactory;
import dao.factories._interfaces.AbstractDeviceFactory;
import dao.factories._interfaces.AbstractLivingFactory;
import dao.factories._interfaces.AbstractRoomFactory;
import model.configuration.House;

public interface SimulationEnvironmentProvider {
    /**
     * Provides not only the house, but includes actors and sensors as well.
     * @return
     */
    House makeEnvironment();
    SimulationEnvironmentProvider setAbstractRoomFactory(AbstractRoomFactory abstractRoomFactory);
    SimulationEnvironmentProvider setAbstractConveyanceFactory(AbstractConveyanceFactory abstractConveyanceFactory);
    SimulationEnvironmentProvider setAbstractLivingFactory(AbstractLivingFactory abstractLivingFactory);
}
