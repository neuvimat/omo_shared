package dao;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dao.factories._interfaces.AbstractConveyanceFactory;
import dao.factories._interfaces.AbstractDeviceFactory;
import dao.factories._interfaces.AbstractLivingFactory;
import dao.factories._interfaces.AbstractRoomFactory;
import dao.factories.conveyance.BasicConveyanceFactory;
import dao.factories.device.BasicDeviceFactory;
import dao.factories.living.BasicLivingFactory;
import dao.factories.room.BasicRoomFactory;
import dao.matchers.ConveyanceMatcher;
import dao.matchers.LivingMatcher;
import dao.matchers.RoomMatcher;
import dao.matchers.SensorMatcher;
import model.actors.Living;
import model.configuration.House;
import model.configuration.Outside;
import model.configuration.Room;
import model.conveyance.Conveyance;
import model.sensor.Environment;
import utils.RandomUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonSimulationEnvironmentProvider implements SimulationEnvironmentProvider {
    private AbstractDeviceFactory deviceFactory = new BasicDeviceFactory();
    private AbstractRoomFactory roomFactory = new BasicRoomFactory();
    private AbstractConveyanceFactory conveyanceFactory = new BasicConveyanceFactory();
    private AbstractLivingFactory livingFactory = new BasicLivingFactory();

    private String filepath;
    private JsonObject parsedData;

    public JsonSimulationEnvironmentProvider(String filepath) {
        this.filepath = filepath;
    }

    @Override
    public House makeEnvironment() {
        try {
            loadJsonData(filepath);
        }
        catch (IOException e) {
            throw new RuntimeException(e); // todo: is this okay or kinda sketchy?
        }
        House house = prepareHouseBuilder().build();
        populateHouse(house);
        sensorateHouse(house);

        new Environment(house);

        return house;
    }

    private void loadJsonData(String filePath) throws IOException {
        FileReader fr = new FileReader(filePath);
        JsonParser parser = new JsonParser();
        parsedData = parser.parse(fr).getAsJsonObject();
        fr.close();
    }

    private House.HouseBuilder prepareHouseBuilder() {
        House.HouseBuilder houseBuilder = new House.HouseBuilder();

        prepareFloorForHouseBuilder(houseBuilder);
        prepareOutsideForHouseBuilder(houseBuilder);

        return houseBuilder;
    }

    private void prepareOutsideForHouseBuilder(House.HouseBuilder houseBuilder) {
        JsonArray conveyances = parsedData.getAsJsonArray("conveyances");
        ConveyanceMatcher conveyanceMatcher = new ConveyanceMatcher(conveyanceFactory);
        Outside outside = new Outside();
        for (JsonElement conveyanceElement : conveyances) {
            Conveyance conveyance = conveyanceMatcher.match(conveyanceElement.getAsString());
            if (conveyance != null) outside.addConveyance(conveyance);
        }
        houseBuilder.setOutside(outside);
    }

    private void prepareFloorForHouseBuilder(House.HouseBuilder houseBuilder) {
        JsonArray floors = parsedData.getAsJsonArray("floors");

        for (JsonElement floorElement : floors) {
            JsonArray floorRooms = floorElement.getAsJsonArray();
            ArrayList<Room> rooms = new ArrayList<>();
            RoomMatcher matcher = new RoomMatcher(roomFactory);
            for (JsonElement roomElement : floorRooms) {
                Room room = matcher.match(roomElement.getAsString());
                if (room != null) rooms.add(room);
            }
            houseBuilder.addFloor(rooms);
        }
    }

    private void populateHouse(House house) {
        JsonArray livings = parsedData.getAsJsonArray("livings");
        LivingMatcher matcher = new LivingMatcher(livingFactory, house);
        List<Room> rooms = house.getAllRooms();

        RandomUtils randomUtils = RandomUtils.getInstance();
        for (JsonElement livingElement : livings) {
            Living living = matcher.match(livingElement.getAsString());
            if (living != null) {
                house.moveLivingToRoom(living, randomUtils.getRandomFromList(rooms));
            }
        }
    }

    private void sensorateHouse(House house) {
        JsonArray sensors = parsedData.getAsJsonArray("sensors");
        SensorMatcher matcher = new SensorMatcher(house);

        for (JsonElement sensorElement : sensors) {
            matcher.match(sensorElement.getAsString());
        }
    }

    @Override
    public SimulationEnvironmentProvider setAbstractRoomFactory(AbstractRoomFactory roomFactory) {
        this.roomFactory = roomFactory;
        return this;
    }

    @Override
    public SimulationEnvironmentProvider setAbstractConveyanceFactory(AbstractConveyanceFactory conveyanceFactory) {
        this.conveyanceFactory = conveyanceFactory;
        return this;
    }

    @Override
    public SimulationEnvironmentProvider setAbstractLivingFactory(AbstractLivingFactory livingFactory) {
        this.livingFactory = livingFactory;
        return this;
    }
}
