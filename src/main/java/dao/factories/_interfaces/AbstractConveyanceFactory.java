package dao.factories._interfaces;


import model.conveyance.Bicycle;
import model.conveyance.Car;
import model.conveyance.Ski;

public interface AbstractConveyanceFactory {
    Bicycle makeBicycle();
    Car makeCar();
    Ski makeSki();
}
