package dao.factories._interfaces;

import model.device.devices.*;

public interface AbstractDeviceFactory {
    ComputerAPI makeComputer();
    CookerAPI makeCooker();
    FridgeAPI makeFridge();
    Heating makeHeating();
    LightAPI makeLight();
    Television makeTelevision();
    WashingMachineAPI makeWashingMachine();
    Window makeWindow();
}
