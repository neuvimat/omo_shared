package dao.factories._interfaces;


import model.actors.Living;
import model.configuration.House;
import model.configuration.Room;

public interface AbstractLivingFactory {
    Living makeDog(House house);
    Living makeCat(House house);

    Living makeFather(House house);
    Living makeMother(House house);
    Living makeSon(House house);
    Living makeDaughter(House house);
    Living makeGrandfather(House house);
    Living makeGrandmother(House house);

    Living makeSaboteur(House house);
}
