package dao.factories._interfaces;


import model.configuration.Room;

public interface AbstractRoomFactory {
    Room makeKitchen();
    Room makeBathroom();
    Room makeBedroom();
    Room makeLivingRoom();
    Room makeStudy();
    Room makeOffice();
    Room makeEmptyRoom();
}
