package dao.factories.device;


import dao.factories._interfaces.AbstractDeviceFactory;
import model.device.Functionality;
import model.device.devices.*;

public class ShoddyDeviceFactory implements AbstractDeviceFactory {
    @Override
    public ComputerAPI makeComputer() {
        return new ComputerAPI(7f);
    }
    @Override
    public CookerAPI makeCooker() {
        return new CookerAPI(10f);
    }
    @Override
    public FridgeAPI makeFridge() {
        return new FridgeAPI(5f);
    }
    @Override
    public Heating makeHeating() {
        return new Heating();
    }
    @Override
    public LightAPI makeLight() {
        return new LightAPI(6f,3f);
    }
    @Override
    public Television makeTelevision() {
        return new Television(new Functionality(2.5f));
    }
    @Override
    public WashingMachineAPI makeWashingMachine() {
        return new WashingMachineAPI();
    }
    @Override
    public Window makeWindow() {
        return new Window();
    }
}
