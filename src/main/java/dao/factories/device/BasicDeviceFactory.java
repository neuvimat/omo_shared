package dao.factories.device;


import dao.factories._interfaces.AbstractDeviceFactory;
import model.device.devices.*;

public class BasicDeviceFactory implements AbstractDeviceFactory {
    @Override
    public ComputerAPI makeComputer() {
        return new ComputerAPI();
    }
    @Override
    public CookerAPI makeCooker() {
        return new CookerAPI(10f);
    }
    @Override
    public FridgeAPI makeFridge() {
        return new FridgeAPI();
    }
    @Override
    public Heating makeHeating() {
        return new Heating();
    }
    @Override
    public LightAPI makeLight() {
        return new LightAPI();
    }
    @Override
    public Television makeTelevision() {
        return new Television();
    }
    @Override
    public WashingMachineAPI makeWashingMachine() {
        return new WashingMachineAPI();
    }
    @Override
    public Window makeWindow() {
        return new Window();
    }
}
