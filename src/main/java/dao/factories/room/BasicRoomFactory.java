package dao.factories.room;

import dao.factories._interfaces.AbstractDeviceFactory;
import dao.factories._interfaces.AbstractRoomFactory;
import dao.factories.device.BasicDeviceFactory;
import model.configuration.Room;

public class BasicRoomFactory implements AbstractRoomFactory {
    private AbstractDeviceFactory deviceFactory;

    public BasicRoomFactory() {
        deviceFactory = new BasicDeviceFactory();
    }

    public BasicRoomFactory(AbstractDeviceFactory deviceFactory) {
        this.deviceFactory = deviceFactory;
    }

    @Override
    public Room makeKitchen() {
        Room room = new Room("Kitchen");
        room.addDevice(deviceFactory.makeCooker());
        room.addDevice(deviceFactory.makeFridge());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeWindow());
        room.addDevice(deviceFactory.makeWindow());
        return room;
    }

    @Override
    public Room makeBathroom() {
        Room room = new Room("Bathroom");
        room.addDevice(deviceFactory.makeHeating());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeWashingMachine());
        room.addDevice(deviceFactory.makeWindow());
        return room;
    }

    @Override
    public Room makeBedroom() {
        Room room = new Room("Bedroom");
        room.addDevice(deviceFactory.makeHeating());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeWindow());
        room.addDevice(deviceFactory.makeWindow());
        return room;
    }

    @Override
    public Room makeLivingRoom() {
        Room room = new Room("Living Room");
        room.addDevice(deviceFactory.makeHeating());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeTelevision());
        room.addDevice(deviceFactory.makeWindow());
        room.addDevice(deviceFactory.makeWindow());
        return room;
    }

    @Override
    public Room makeStudy() {
        Room room = new Room("Study");
        room.addDevice(deviceFactory.makeComputer());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeWindow());
        room.addDevice(deviceFactory.makeWindow());
        return room;
    }

    @Override
    public Room makeOffice() {
        Room room = new Room("Study");
        room.addDevice(deviceFactory.makeComputer());
        room.addDevice(deviceFactory.makeComputer());
        room.addDevice(deviceFactory.makeHeating());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeLight());
        room.addDevice(deviceFactory.makeWindow());
        room.addDevice(deviceFactory.makeWindow());
        return room;
    }

    @Override
    public Room makeEmptyRoom() {
        Room room = new Room("Empty Room");
        return room;
    }

    public AbstractDeviceFactory getDeviceFactory() {
        return deviceFactory;
    }
    public void setDeviceFactory(AbstractDeviceFactory deviceFactory) {
        this.deviceFactory = deviceFactory;
    }
}
