package dao.factories.living;

import dao.factories._interfaces.AbstractLivingFactory;
import model.actors.Living;
import model.actors.behaviours.*;
import model.actors.livings.Animal;
import model.actors.livings.Human;
import model.actors.livings.needs.Hunger;
import model.configuration.House;
import utils.RandomUtils;

public class BasicLivingFactory implements AbstractLivingFactory {
    private int livingsMade = 0;
    RandomUtils random = RandomUtils.getInstance();

    @Override
    public Living makeDog(House house) {
        livingsMade++;
        return new Animal(house, new DogBehaviour(), random.getDogName(), "Dog");
    }
    @Override
    public Living makeCat(House house) {
        livingsMade++;
        return new Animal(house, new CatBehaviour(), random.getCatName(), "Cat");
    }
    @Override
    public Living makeFather(House house) {
        livingsMade++;

        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0.05f).setFridgeRecheck(6).setHungryAtPercent(0.84f).setRefillFridge(true).setCanCook(true, .03f);

        return new Human(house, behavior.build(), random.getMaleName() + " (Father)", random.getSurname(), true, 110f);
    }
    @Override
    public Living makeMother(House house) {
        livingsMade++;

        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0.12f).setFridgeRecheck(6).setHungryAtPercent(0.84f).setRefillFridge(true).setSocializeFactor(0.6f).setCanCook(true, .25f);

        return new Human(house, behavior.build(), random.getFemaleName() + " (Mother)", random.getSurname(), true);
    }
    @Override
    public Living makeSon(House house) {
        livingsMade++;

        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0.01f).setFridgeRecheck(4).setHungryAtPercent(0.90f).setRefillFridge(false).setPlaysWithLights(true);

        return new Human(house, behavior.build(), random.getMaleName() + " (Son)", random.getSurname(), false, 87);
    }
    @Override
    public Living makeDaughter(House house) {
        livingsMade++;

        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0.02f).setFridgeRecheck(3).setHungryAtPercent(0.90f).setRefillFridge(true).setPlaysWithLights(true).setCanCook(true, .05f);

        return new Human(house, behavior.build(), random.getFemaleName() + " (Daughter)", random.getSurname(), false, 80);
    }
    @Override
    public Living makeGrandfather(House house) {
        livingsMade++;

        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0.08f).setFridgeRecheck(1).setHungryAtPercent(0.95f).setRefillFridge(false).setSocializeFactor(0.3f).setCanCook(true, .15f);

        return new Human(house, behavior.build(), random.getMaleName() + " (Grandpa)", random.getSurname(), true);
    }
    @Override
    public Living makeGrandmother(House house) {
        livingsMade++;

        // Grandma is anorexic and talks to animals
        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0.12f).setFridgeRecheck(18).setHungryAtPercent(0.3f).setRefillFridge(false).setSocializeFactor(1.35f).setTalksToAnimals(true).setPlaysWithLights(true);

        Human grandma = new Human(house, behavior.build(), random.getFemaleName() + " (Grandma)", random.getSurname(), true);
        grandma.overrideBasicHunger(new Hunger(grandma, 50, 180));



        return grandma;
    }
    @Override
    public Living makeSaboteur(House house) {
        livingsMade++;

        // Eats a lot, turns off fridges
        ParametrizedHumanBehavior.Builder behavior = new ParametrizedHumanBehavior.Builder();
        behavior.setChoirsChance(0).setFridgeRecheck(8).setHungryAtPercent(0.96f).setRefillFridge(false).setSocializeFactor(0.1f).setCanCook(false);

        return new Human(house, new SaboteurHumanBehavior(behavior, 1), random.getSaboteurName() + " (Saboteur)", random.getSurname(), true, 180);
    }
}
