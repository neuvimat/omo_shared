package dao.factories.conveyance;

import dao.factories._interfaces.AbstractConveyanceFactory;
import model.conveyance.Bicycle;
import model.conveyance.Car;
import model.conveyance.Ski;
import utils.RandomUtils;

public class BasicConveyanceFactory implements AbstractConveyanceFactory {

    @Override
    public Bicycle makeBicycle() {
        return new Bicycle();
    }
    @Override
    public Car makeCar() {
        return new Car(RandomUtils.getInstance().nextInt(4));
    }
    @Override
    public Ski makeSki() {
        return new Ski();
    }
}
