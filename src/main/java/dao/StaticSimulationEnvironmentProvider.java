package dao;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dao.factories._interfaces.AbstractConveyanceFactory;
import dao.factories._interfaces.AbstractDeviceFactory;
import dao.factories._interfaces.AbstractLivingFactory;
import dao.factories._interfaces.AbstractRoomFactory;
import dao.factories.conveyance.BasicConveyanceFactory;
import dao.factories.device.BasicDeviceFactory;
import dao.factories.living.BasicLivingFactory;
import dao.factories.room.BasicRoomFactory;
import dao.matchers.ConveyanceMatcher;
import dao.matchers.LivingMatcher;
import dao.matchers.RoomMatcher;
import dao.matchers.SensorMatcher;
import model.actors.Living;
import model.configuration.House;
import model.configuration.Outside;
import model.configuration.Room;
import model.conveyance.Bicycle;
import model.conveyance.Car;
import model.conveyance.Conveyance;
import model.conveyance.Ski;
import model.sensor.Environment;
import model.sensor.TemperatureSensor;
import model.sensor.WindSensor;
import utils.RandomUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StaticSimulationEnvironmentProvider implements SimulationEnvironmentProvider {
    private AbstractRoomFactory roomFactory = new BasicRoomFactory(new BasicDeviceFactory());
    private AbstractConveyanceFactory conveyanceFactory = new BasicConveyanceFactory();
    private AbstractLivingFactory livingFactory = new BasicLivingFactory();

    @Override
    public House makeEnvironment() {

        House house = prepareHouseBuilder().build();
        populateHouse(house);
        sensorateHouse(house);

        new Environment(house);

        return house;
    }

    private House.HouseBuilder prepareHouseBuilder() {
        House.HouseBuilder houseBuilder = new House.HouseBuilder();

        prepareFloorForHouseBuilder(houseBuilder);
        prepareOutsideForHouseBuilder(houseBuilder);



        return houseBuilder;
    }

    private void prepareOutsideForHouseBuilder(House.HouseBuilder houseBuilder) {
        Outside outside = new Outside();
        outside.addConveyance(conveyanceFactory.makeBicycle(), conveyanceFactory.makeSki(), conveyanceFactory.makeCar());
        houseBuilder.setOutside(outside);
    }

    private void prepareFloorForHouseBuilder(House.HouseBuilder houseBuilder) {
        houseBuilder.addFloor(roomFactory.makeBathroom(), roomFactory.makeKitchen(), roomFactory.makeLivingRoom());
        houseBuilder.addFloor(roomFactory.makeOffice(), roomFactory.makeStudy(), roomFactory.makeBedroom());
    }

    private void populateHouse(House house) {
        List<Room> rooms = house.getAllRooms();
        List<Living> livings = new ArrayList<>();
        livings.add(livingFactory.makeFather(house));
        livings.add(livingFactory.makeMother(house));
        livings.add(livingFactory.makeSon(house));
        livings.add(livingFactory.makeDaughter(house));
        livings.add(livingFactory.makeDog(house));
        livings.add(livingFactory.makeCat(house));

        RandomUtils randomUtils = RandomUtils.getInstance();
        for (Living living : livings) {
            house.moveLivingToRoom(living, randomUtils.getRandomFromList(rooms));
        }
    }

    private void sensorateHouse(House house) {
        new WindSensor(house);
        new TemperatureSensor(house);
    }

    @Override
    public SimulationEnvironmentProvider setAbstractRoomFactory(AbstractRoomFactory roomFactory) {
        this.roomFactory = roomFactory;
        return this;
    }

    @Override
    public SimulationEnvironmentProvider setAbstractConveyanceFactory(AbstractConveyanceFactory conveyanceFactory) {
        this.conveyanceFactory = conveyanceFactory;
        return this;
    }

    @Override
    public SimulationEnvironmentProvider setAbstractLivingFactory(AbstractLivingFactory livingFactory) {
        this.livingFactory = livingFactory;
        return this;
    }
}
