package view;

import model.actors.Living;
import java.util.Collection;

public interface SurvivorReporter {

    /**
     * G
     * @param list of livings
     */
    void generateReport(Collection<Living> list);
}
