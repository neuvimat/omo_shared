package view;

import model.configuration.House;
import model.configuration.Room;
import model.device.ConsumptionType;
import model.device.Device;

import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.Duration;

/**
 * Implementation of ConsumptionReporter using PrintWriter.
 */
public class BasicConsumptionReporter extends BasicReporter implements ConsumptionReporter {

    private Duration from;
    private Duration to;
    private ConsumptionCosts costs;

    public BasicConsumptionReporter(FileWriter fw, Duration from, Duration to, ConsumptionCosts costs) {
        super(fw);
        this.from = from;
        this.to = to;
        this.costs = costs;
    }

    public BasicConsumptionReporter(OutputStream os, Duration from, Duration to, ConsumptionCosts costs) {
        super(os);
        this.from = from;
        this.to = to;
        this.costs = costs;
    }

    @Override
    public void generateReport(House house) {
        pw.println("Total consumption: ");
        for(ConsumptionType type: ConsumptionType.values()){
            float consumption = house.getTotalConsumption(type, from, to);
            printConsumption(pw, consumption, type);
        }
        pw.println();
        for(Room room: house){
            for (Device device: room.getDevices()){
                if(device.hasAnyConsumption()){
                    pw.println(device.getCategoryName()+ ": ");
                    for(ConsumptionType type: ConsumptionType.values()){
                        if(device.hasConsumption(type)){
                            printConcreteConsumption(pw, device, type);
                        }
                    }
                }
            }
        }
        pw.close();
    }

    private void printConcreteConsumption(PrintWriter pw, Device device, ConsumptionType type){
        float consumption = device.getTotalConsumption(type, from, to);
        printConsumption(pw, consumption, type);
    }

    private void printConsumption(PrintWriter pw, float consumption, ConsumptionType type){
        appendWhitespace(pw, 3);
        pw.print(getConsumptionLong(type) + ": ");
        pw.print(consumption + " u/h, "); // u as unit
        int cost = (int) (consumption * costs.getCost(type));
        pw.print(cost + " B"); // totally random currency choose, B as Bananas
        pw.println();
    }

    private String getConsumptionLong(ConsumptionType type){
        switch (type) {
            case ELECTRICITY:
                return "ELECTRICITY";
            case GAS:
                return "GAS";
            case WATER:
                return "WATER";
        }
        return "Error";
    }
}
