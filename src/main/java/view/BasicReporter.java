package view;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Implementation of reporters using PrintWriter.
 */
public abstract class BasicReporter {

    protected PrintWriter pw;

    protected BasicReporter(FileWriter fw) {
        pw = new PrintWriter(fw);
    }

    protected BasicReporter(OutputStream os){
        pw = new PrintWriter(os);
    }

    protected void appendWhitespace(PrintWriter pw, int count){
        for (int i = 0; i < count; i++){
            pw.print(' ');
        }
    }
}
