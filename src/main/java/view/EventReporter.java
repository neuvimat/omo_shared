package view;

import controller.event.EventHistoryRecord;

import java.time.Duration;
import java.util.List;

public interface EventReporter {

    /**
     * Generates report of events that occurred in simulation.
     * @param history History of events.
     */
    void generateReport(List<EventHistoryRecord> history);
}
