package view;

import controller.action.ActionHistoryRecord;
import controller.event.EventExecution;

/**
 * For disabling messages in simulation.
 */
public class BlankSimulationView implements SimulationView {
    @Override
    public void displayAction(ActionHistoryRecord record) {
    }

    @Override
    public void displayEvent(EventExecution eventExecution) {
    }
}
