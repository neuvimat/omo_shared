package view;

import controller.action.ActionHistoryRecord;
import model.actors.Actor;

import java.util.List;

public interface ActivityAndUsageReporter {

    /**
     * Generates report of actions from simulation.
     * @param history history of actions from simulation.
     */
    void generateReport(List<ActionHistoryRecord> history);

}
