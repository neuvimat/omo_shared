package view;

import controller.action.ActionHistoryRecord;

import java.io.FileWriter;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.reducing;

/**
 * Implementation of ActivityAndUsageReporter using PrintWriter.
 */
public class BasicActivityAndUsageReporter extends BasicReporter implements ActivityAndUsageReporter {

    public BasicActivityAndUsageReporter(FileWriter fw) {
        super(fw);
    }

    public BasicActivityAndUsageReporter(OutputStream os) {
        super(os);
    }

    @Override
    public void generateReport(List<ActionHistoryRecord> history) {
        // todo: I have to study this, this is some nice stream work! -- Mates
        Map<String, List<ActionHistoryRecord>> byActor =
                history.stream().collect(groupingBy((ActionHistoryRecord record) -> {
                    return record.getActor().getName();
                }));

        byActor.forEach((String actor, List<ActionHistoryRecord> records1) ->
        {
            pw.println(actor + ":");
            Map<String, List<ActionHistoryRecord>> byAction = records1.stream()
                    .sorted(Comparator.comparing(ActionHistoryRecord::getActionName))
                    .collect(groupingBy(ActionHistoryRecord::getActionName));

            byAction.forEach((String action, List<ActionHistoryRecord> records2) ->
            {
                appendWhitespace(pw, 3);
                pw.println(action + " " + records2.size() + "x");
                Map<String, List<ActionHistoryRecord>> byUsed =
                        records2.stream().collect(groupingBy((ActionHistoryRecord record) ->{
                            if(record.getUsed() == null){
                                return "";
                            }
                            return record.getUsed().getCategoryName();
                        }));
                byUsed.forEach((String used, List<ActionHistoryRecord> records3) ->
                {
                    if(!used.equals("")){
                        appendWhitespace(pw, 6);
                        pw.println("-> " + used + ", used " + records3.size() + "x");
                    }
                });
            });
            pw.println();
        });
        pw.close();
    }
}
