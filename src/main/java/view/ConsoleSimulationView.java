package view;

import controller.action.Action;
import controller.action.ActionHistoryRecord;
import controller.event.EventExecution;
import controller.event.EventHistoryRecord;

/**
 * Writes simulation messages to console.
 */
public class ConsoleSimulationView implements SimulationView {

    @Override
    public void displayAction(ActionHistoryRecord record) {
        System.out.println(record.toString());
    }

    @Override
    public void displayEvent(EventExecution eventExecution) {
        System.out.println(eventExecution.toString());
    }
}
