package view;

import model.configuration.House;

public interface ConsumptionReporter {

    /**
     * Generates report from some date in some period of some house.
     * @param house House from simulation.
     */
    void generateReport(House house);
}
