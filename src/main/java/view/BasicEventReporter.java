package view;

import controller.event.EventHistoryRecord;

import java.io.FileWriter;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.reducing;


/**
 * Implementation of EventReporter using PrintWriter.
 */
public class BasicEventReporter extends BasicReporter implements EventReporter {

    public BasicEventReporter(FileWriter fw) {
        super(fw);
    }

    public BasicEventReporter(OutputStream os) {
        super(os);
    }

    @Override
    public void generateReport(List<EventHistoryRecord> history) {
        Map<String, List<EventHistoryRecord>> byEvent = history.stream().collect(groupingBy(
            (EventHistoryRecord record) -> record.getExecution().getEvent().getCategoryName()));

        byEvent.forEach((String event, List<EventHistoryRecord> records1) -> {
            pw.println(event + " x" + records1.size() + ":");
            Map<String, List<EventHistoryRecord>> bySource = records1.stream().collect(groupingBy(
                (EventHistoryRecord record) -> record.getExecution().getEventSource().getCategoryName()));
            bySource.forEach((String source, List<EventHistoryRecord> records2) -> {
                appendWhitespace(pw, 3);
                pw.println(source + " x" + records2.size() + "->");
                Map<String, List<EventHistoryRecord>> byListener = records2.stream().collect(groupingBy(
                        (EventHistoryRecord record) -> record.getListener().getOwner().getName()));
                Set<String> listeners = byListener.keySet();
                for(String listener: listeners) {
                    appendWhitespace(pw, 6);
                    pw.println(listener);
                }
            });
        });
        pw.close();
    }
}
