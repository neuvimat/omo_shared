package view;

import model.device.ConsumptionType;

import java.time.Duration;

public interface ConsumptionReportable {

    /**
     * Enables to compute consumption.
     * @param type Type of consumption.
     * @param from From some point in time.
     * @param to To some point in time.
     * @return total consumption in specified interval.
     */
    float getTotalConsumption(ConsumptionType type, Duration from, Duration to);

}
