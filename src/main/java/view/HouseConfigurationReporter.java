package view;

import model.configuration.House;

public interface HouseConfigurationReporter {

    /**
     * Generates report of house structure.
     * @param house House from simulation
     */
    void generateReport(House house);
}
