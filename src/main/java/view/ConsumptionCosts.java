package view;

import model.device.ConsumptionType;

/**
 * Represents actual cost of consumption types.
 */
public class ConsumptionCosts {

    private int electricity;
    private int gas;
    private int watter;

    public ConsumptionCosts(int electricity, int gas, int watter) {
        this.electricity = electricity;
        this.gas = gas;
        this.watter = watter;
    }

    /**
     * Cost of specified consumption type for unit/h
     * @param type consumption type
     * @return cost
     */
    public int getCost(ConsumptionType type){
        switch (type) {
            case ELECTRICITY:
                return electricity;
            case GAS:
                return gas;
            case WATER:
                return watter;
        }
        return 0;
    }
}
