package view;

import model.actors.Living;
import model.configuration.Floor;
import model.configuration.House;
import model.configuration.Room;
import model.device.Device;

import java.io.*;

/**
 * Implementation of HouseConfigurationReporter using PrintWriter.
 */
public class BasicHouseConfigurationReporter extends BasicReporter implements HouseConfigurationReporter {

    public BasicHouseConfigurationReporter(FileWriter fw) {
        super(fw);
    }

    public BasicHouseConfigurationReporter(OutputStream os) {
        super(os);
    }

    @Override
    public void generateReport(House house) {
        pw.println("House:");
        for(Floor floor: house.getFloors()){
            appendWhitespace(pw, 3);
            pw.println("Floor " + floor.getFloorNumber() + ":");
            for (Room room: floor.getRooms()){
                appendWhitespace(pw, 6);
                pw.println(room.getName() + ":");
                for(Device device: room.getDevices()){
                    appendWhitespace(pw, 9);
                    pw.println(device.getCategoryName());
                }
            }
        }
        pw.println("Inhabitants:");
        for(Living living: house.getAllLivings()){
            appendWhitespace(pw, 3);
            pw.println(living.getName());
        }
        pw.close();
    }
}
