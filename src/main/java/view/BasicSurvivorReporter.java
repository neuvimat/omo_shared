package view;

import controller.event.EventHistoryRecord;
import model.actors.Living;
import model.actors.livings.Human;
import utils.DurationUtil;

import java.io.FileWriter;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.groupingBy;


/**
 * Implementation of SurvivorReporter using PrintWriter.
 */
public class BasicSurvivorReporter extends BasicReporter implements SurvivorReporter {

    public BasicSurvivorReporter(FileWriter fw) {
        super(fw);
    }

    public BasicSurvivorReporter(OutputStream os) {
        super(os);
    }

    @Override
    public void generateReport(Collection<Living> list) {
        list.forEach((living)-> {
            pw.println(living.getName());
            appendWhitespace(pw, 3);
            pw.print("-> ");
            if (living.isAlive()) {
                pw.println("Alive");
            }
            else {
                pw.println("Deceased");
                appendWhitespace(pw, 6);
                pw.println("-> Time of death:" + DurationUtil.format(living.getTimeOfDeath()));
            }
            appendWhitespace(pw, 3);
            pw.println("-> Fun facts:");
            appendWhitespace(pw, 6);
            pw.println("-> Eaten total " + living.getHunger().getTotalFoodEaten() + " units of food!");
            if (living.getCategoryName().equals("Human")) {
                float savedByCooking = ((Human)living).getSavedByCooking();
                if (savedByCooking > 0) {
                    appendWhitespace(pw, 6);
                    pw.println("-> Food saved by cooking " + savedByCooking + " units of food!");
                }
            }
        });

        pw.close();
    }
}
