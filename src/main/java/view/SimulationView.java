package view;

import controller.action.Action;
import controller.action.ActionHistoryRecord;
import controller.event.EventExecution;

/**
 * For handling messages from simulation.
 */
public interface SimulationView {
    /**
     * Displays concrete action.
     */
    void displayAction(ActionHistoryRecord record);

    /**
     * Displays concrete event execution.
     */
    void displayEvent(EventExecution eventExecution);
}
