package model.configuration;

import model.device.ConsumptionType;
import view.ConsumptionReportable;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Floor in house.
 */
public class Floor implements ConsumptionReportable{
    private int floorNumber;
    private List<Room> rooms;

    public Floor(int floorNumber) {
        this.floorNumber = floorNumber;
        rooms = new ArrayList<>();
    }

    public Floor(FloorBuilder builder) {
        this.floorNumber = builder.floorNumber;
        this.rooms = builder.rooms;
    }
    public int getFloorNumber() {
        return floorNumber;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void addRoom(Room r) {
        this.rooms.add(r);
    }

    private static class FloorBuilder {
        private int floorNumber;
        private List<Room> rooms;

        public Floor build() {
            return new Floor(this);
        }
    }

    @Override
    public float getTotalConsumption(ConsumptionType type, Duration from, Duration to) {
        float sum = 0;
        for(Room room: rooms){
            sum += room.getTotalConsumption(type, from, to);
        }
        return sum;
    }
}
