package model.configuration;

import model.actors.Living;
import model.device.ConsumptionType;
import model.device.Device;
import utils.BasicNamed;
import view.ConsumptionReportable;

import java.time.Duration;
import java.util.*;

/**
 * Room located on some floor. Contains devices and living actors.
 */
public class Room implements BasicNamed, ConsumptionReportable{

    private String name;
    private Map<String, List<Device>> deviceNameToDevicesMap;
    private Map<Integer, Living> livings;

    public Room(String name) {
        this.name = name;
        deviceNameToDevicesMap = new HashMap<>();
        livings = new HashMap<>();
    }

    public Room() {
        this.name = "undefined";
        deviceNameToDevicesMap = new HashMap<>();
        livings = new HashMap<>();
    }

    public Room(RoomBuilder builder) {
        this.name = builder.name;
        deviceNameToDevicesMap = builder.deviceNameToDevicesMap;
        livings = new HashMap<>();
    }

    /**
     * Adds device to room.
     * @param device device to be added.
     */
    public void addDevice(Device device){
        String name = device.getCategoryName();
        List<Device> devices;
        if(deviceNameToDevicesMap.containsKey(name)){
            devices = deviceNameToDevicesMap.get(name);
        }
        else {
            devices = new ArrayList<>();
            deviceNameToDevicesMap.put(name, devices);
        }
        devices.add(device);
    }

    /**
     * Moves living into this room.
     */
    public void addLiving(Living living){
        if(!livings.containsKey(living.getId())){
            livings.put(living.getId(), living);
        }
    }

    /**
     * Removes living from this room.
     */
    public void removeLiving(Living living){
        livings.remove(living.getId());
    }

    /**
     * @return True if living is in this room.
     */
    public boolean containsLiving(Living living){
        return livings.containsKey(living.getId());
    }

    /**
     * Determines if specified device is in this room.
     */
    public boolean containsDevice(Device device){
        if(containsDeviceCategory(device)){
            return deviceNameToDevicesMap.get(device.getCategoryName()).contains(device);
        }
        return false;
    }

    /**
     * Determines if specified device category is in this room.
     * @param device Device of category to find.
     */
    public boolean containsDeviceCategory(Device device){
        return deviceNameToDevicesMap.containsKey(device.getCategoryName());
    }

    /**
     * Determines if specified device category is in this room.
     * @param deviceCode Device name of category to find.
     */
    public boolean containsDeviceCategory(String deviceCode){
        return deviceNameToDevicesMap.containsKey(deviceCode);
    }

    public boolean containsDeviceByRef(Device device){
        List<Device> list = deviceNameToDevicesMap.get(device.getCategoryName());
        if (list != null) {
            return list.contains(device);
        }
        return false;
    }

    public <D extends Device> List<D> getDevicesOfType(D blankDevice){
        List<Device> devices = deviceNameToDevicesMap.get(blankDevice.getCategoryName());
        List<D> result = new ArrayList<>();
        if(devices != null){
            for(Device device: devices){
                result.add((D)device);
            }
        }
        return result;
    }

    public <D extends Device> List<D> getDevicesOfType(String deviceCode){
        List<Device> devices = deviceNameToDevicesMap.get(deviceCode);
        List<D> result = new ArrayList<>();
        if(devices != null){
            for(Device device: devices){
                result.add((D)device);
            }
        }
        return result;
    }

    @Override
    public float getTotalConsumption(ConsumptionType type, Duration from, Duration to) {
        float sum = 0;
        for (List<Device> devices: deviceNameToDevicesMap.values()){
            for(Device device: devices){
                sum += device.getTotalConsumption(type, from, to);
            }
        }
        return sum;
    }

    public List<Device> getDevices() {
        List<Device> devices = new ArrayList<>();
        for (List<Device> d: deviceNameToDevicesMap.values()){
            devices.addAll(d);
        }
        return devices;
    }

    public Collection<Living> getLivings() {
        return livings.values();
    }

    public String getName() {
        return name;
    }

    public static class RoomBuilder {
        private String name;
        private Map<String, List<Device>> deviceNameToDevicesMap;
    }
}
