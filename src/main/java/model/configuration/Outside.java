package model.configuration;

import model.actors.Living;
import model.conveyance.Conveyance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Space excluding house.
 */
public class Outside extends Room {

    private List<Conveyance> conveyances;

    public Outside() {
        super("Outside");
        conveyances = new ArrayList<>();
    }

    public void addConveyance(Conveyance... conveyance) {
        this.conveyances.addAll(Arrays.asList(conveyance));
    }

    public void addConveyance(Conveyance conveyance) {
        this.conveyances.add(conveyance);
    }

    public <D extends Conveyance> List<D> getConveyancesOfType(String conveyanceCode){
        List<D> result = new ArrayList<>();
        for (Conveyance conveyance : conveyances)
        if(conveyance.getCategoryName().equals(conveyanceCode)){
            result.add((D)conveyance);
        }
        return result;
    }

    public List<Conveyance> getConveyances() {
        return conveyances;
    }
}
