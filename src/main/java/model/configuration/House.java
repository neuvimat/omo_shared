package model.configuration;

import model.actors.Actor;
import model.actors.Living;
import model.device.ConsumptionType;
import model.device.Device;
import model.sensor.Environment;
import model.sensor.Sensor;
import view.ConsumptionReportable;

import java.time.Duration;
import java.util.*;
import java.util.function.Consumer;

/**
 * Just a normal house storing actors, devices etc.
 */
public class House implements Iterable<Room>, ConsumptionReportable {
    private List<Floor> floors;
    private Outside outside;
    private Map<Integer, Living> allLivings;

    private List<Actor> allActors;
    private Environment environment;
    private List<Sensor> sensors;

    public House(HouseBuilder builder) {
        this.floors = builder.floors;
        this.outside = builder.outside;

        this.allLivings = new HashMap<>();
        this.allActors = new ArrayList<>();
        this.sensors = new ArrayList<>();
    }

    /**
     * Finds room where is contained specified living.
     * @param living Living contained in room.
     * @return Room if found, else null.
     */
    public Room findRoomByLiving(Living living)
    {
        for(Room room: this){
            if(room.containsLiving(living)){
                return room;
            }
        }
        if (outside.containsLiving(living)) {
            return outside;
        }
        return null;
    }

    /**
     * Finds all other Livings (both humans and animals) in the room where the inputted living is
     * @param living Living who to search around
     * @return Collection of all livings
     */
    public Collection<Living> findAdjacentLivings(Living living) {
        Collection<Living> collection = findRoomByLiving(living).getLivings();
        collection.remove(living);
        return collection;
    }

    /**
     * Finds all other Livings with the code specified in the room where the inputted living is
     * @param living Living who to search around
     * @param livingCode Search for livings with this code
     * @return Collection of all livings
     */
    public Collection<Living> findAdjacentLivings(Living living, String livingCode) {
        // Todo: vyhodi error jako wtf, fix later, chce se mi spat -- Mates
        // Todo: debug result - for some reason, the actor is not assigned in any room, it vanished, probably alongside one another guy
        // fixme:
        // Sheter: XD
        // FIXED BY RESOLVING THE CAUSE
        Room room = findRoomByLiving(living);
        if (room == null) {
            // keep this in case it gets broken agains, we can consider this a test case xd
            System.out.println("Note - delete me from source code please");
            return new ArrayList<>();
        }
        Collection<Living> collection = findRoomByLiving(living).getLivings();

        collection.removeIf(l -> !l.getCategoryName().equals(livingCode));
        collection.remove(living);

        return collection;
    }

    /**
     * Finds rooms where is contained specified device type.
     * @param device Instance of device type searching for.
     * @return List of rooms containing device.
     */
    public List<Room> findRoomsByDeviceCategory(Device device){
        List<Room> rooms = new ArrayList<>();
        for(Room room: this){
            if(room.containsDeviceCategory(device)){
                rooms.add(room);
            }
        }
        return rooms;
    }

    /**
     * Finds room where is contained specified device.
     * @param device device to search for
     * @return the room if found, else null
     */
    public Room findRoomByDevice(Device device){
        for(Room room: this){
            if(room.containsDevice(device)){
                return room;
            }
        }
        return null;
    }

    /**
     * Finds rooms where is contained specified device type.
     * @param deviceCode Name of device type searching for.
     * @return List of rooms containing device.
     */
    public List<Room> findRoomsByDeviceCategory(String deviceCode){
        List<Room> rooms = new ArrayList<>();
        for(Room room : this){
            if(room.containsDeviceCategory(deviceCode)){
                rooms.add(room);
            }
        }
        return rooms;
    }

    /**
     * Gets all devices in house of specified type.
     * @param deviceCode Name of device type searching for.
     * @param <D> // fixme: hmm, looks like a bug
     * @return all devices in house of specified type
     */
    public <D extends Device> List<D> getDevicesOfType(String deviceCode){
        List<D> result = new ArrayList<>();
        for (Room r : this) {
            result.addAll(r.getDevicesOfType(deviceCode));
        }
        result.addAll(getOutside().getDevicesOfType(deviceCode));
        return result;
    }

    public Room findRoomByDeviceReference(Device device) {
        for (Room r : this) {
            if (r.containsDeviceByRef(device)) return r;
        }
        if (outside.containsDeviceByRef(device)) return outside;
        return null;
    }

    public Outside getOutside() {
        return outside;
    }

    /**
     * Moves living to specified room
     * @param living living to move
     * @param room room to move to
     */
    public void moveLivingToRoom(Living living, Room room) {
        Room r = findRoomByLiving(living);
        if (r != null) {
            r.removeLiving(living);
        }
        if(outside.containsLiving(living)){
            outside.removeLiving(living);
        }
        room.addLiving(living);
        Room test = findRoomByLiving(living);
    }

    /**
     * Moves specified living outside
     * @param living living to move
     */
    public void moveLivingOutside(Living living){
        Room r = findRoomByLiving(living);
        if(r != null){
            r.removeLiving(living);
        }
        outside.addLiving(living);
        Room test = findRoomByLiving(living);
    }

    @Override
    public Iterator<Room> iterator() {
        return new RoomsIterator(floors);
    }

    @Override
    public void forEach(Consumer<? super Room> action) {
        for (Room room : this) {
            action.accept(room);
        }
    }

    public List<Room> getAllRooms() {
        Iterator<Room> iterator = iterator();
        ArrayList<Room> rooms = new ArrayList<>();
        while (iterator.hasNext()) {
            rooms.add(iterator.next());
        }
        return rooms;
    }

    public void registerActor(Actor actor) {
        if (!allActors.contains(actor)) {
            allActors.add(actor);
        }
    }

    public void registerLiving(Living living) {
        if (!allLivings.containsKey(living.getId())) {
            allLivings.put(living.getId(), living);
        }
    }

    public void registerSensor(Sensor sensor) {
        if (!sensors.contains(sensor)) {
            sensors.add(sensor);
        }
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public List<Actor> getAllActors() {
        return allActors;
    }

    public Collection<Living> getAllLivings() {
        return allLivings.values();
    }

    @Override
    public float getTotalConsumption(ConsumptionType type, Duration from, Duration to) {
        float sum = 0;
        for(Floor floor: floors){
            sum += floor.getTotalConsumption(type, from, to);
        }
        return sum;
    }

    public static class HouseBuilder {
        private List<Floor> floors;
        private Outside outside;

        public HouseBuilder() {
            floors = new ArrayList<>();
        }

        public House build() {
            return new House(this);
        }

        public HouseBuilder addFloor(Room... rooms) {
            Floor f = new Floor(floors.size());
            for (Room r : rooms) {
                f.addRoom(r);
            }
            floors.add(f);
            return this;
        }

        public HouseBuilder addFloor(ArrayList<Room> rooms) {
            Floor f = new Floor(floors.size());
            for (Room r : rooms) {
                f.addRoom(r);
            }
            floors.add(f);
            return this;
        }

        public HouseBuilder setOutside(Outside outside) {
            this.outside = outside;
            return this;
        }
    }
}
