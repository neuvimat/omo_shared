package model.configuration;

import java.util.Iterator;
import java.util.List;

/**
 * For easy iteration of rooms in house.
 */
public class RoomsIterator implements Iterator<Room> {
    private List<Floor> floors;
    private Floor lastFloor;
    private int lastFloorIdx;
    private int lastRoomIdx;

    public RoomsIterator(List<Floor> floors) {
        this.floors = floors;
        lastFloorIdx = 0;
        lastRoomIdx = 0;
        if(!floors.isEmpty()){
            lastFloor = floors.get(0);
        }
    }

    @Override
    public boolean hasNext() {
        return !floors.isEmpty() && lastFloorIdx < floors.size();
    }

    @Override
    public Room next() {
        List<Room> rooms = lastFloor.getRooms();
        Room ret = rooms.get(lastRoomIdx);
        lastRoomIdx++;
        if(lastRoomIdx >= rooms.size()){
            lastRoomIdx = 0;
            lastFloorIdx++;
            if(lastFloorIdx < floors.size()){
                lastFloor = floors.get(lastFloorIdx);
            }
        }
        return ret;
    }
}
