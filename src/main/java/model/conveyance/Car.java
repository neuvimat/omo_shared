package model.conveyance;

import model.device.Consumption;
import model.device.ConsumptionType;
import model.device.Documentation;

public class Car extends Conveyance {
    public static final String categoryName = "Car";

    public Car(float deteriorationPercentage) {
        super(deteriorationPercentage);
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(350,20,0));
    }

    public float getSpeed() {
        return 80f;
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public boolean isOn() {
        return false;
    }

    @Override
    public void turnOn() {

    }

    @Override
    public void turnOff() {

    }

    @Override
    public void repair() {

    }
}