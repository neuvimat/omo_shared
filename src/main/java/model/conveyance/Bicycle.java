package model.conveyance;

import model.device.Documentation;

public class Bicycle extends Conveyance {
    public Bicycle() {
        super(0);
    }

    @Override
    public String getCategoryName() {
        return "Bicycle";
    }

    @Override
    public void accept(Documentation documentation) {

    }

    @Override
    public boolean isOn() {
        return false;
    }

    @Override
    public void turnOn() {

    }

    @Override
    public void turnOff() {

    }

    @Override
    public boolean isBroken() {
        return false;
    }

    @Override
    public void repair() {

    }
}
