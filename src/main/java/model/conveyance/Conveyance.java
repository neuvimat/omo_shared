package model.conveyance;

import model.device.Device;
import model.device.Functionality;
import model.device.Usable;

public abstract class Conveyance extends Device {

    private boolean isBeingUsed = false;

    protected Conveyance(float deteriorationPercentage) {
        super(new Functionality(deteriorationPercentage));
    }

    public void startUsing(){
        isBeingUsed = true;
    }

    public void stopUsing(){
        isBeingUsed = false;
    }

    public boolean isBeingUsed() {
        return isBeingUsed;
    }
}
