package model.device;

public class Functionality {
    private static final float MAX_PERCENT = 100f;

    private float actualPercent;
    private float decreasingPercent;

    public Functionality(float decreasingPercent) {
        actualPercent = MAX_PERCENT;
        this.decreasingPercent = decreasingPercent;
    }

    /**
     * Get current healthiness
     * @return percentage health
     */
    public float getActualPercent() {
        return actualPercent;
    }

    /**
     * Self explanatory
     */
    public boolean isBroken(){
        return actualPercent <= 0;
    }

    /**
     * @return True if broken.
     */
    public boolean use(){
        actualPercent -= decreasingPercent;
        return isBroken();
    }

    /**
     * Sets functionality to full
     */
    public void repair(){
        actualPercent = MAX_PERCENT;
    }
}
