package model.device.devices;

import model.device.DeviceState;

public interface CookerState extends DeviceState {
    void cook();
}
