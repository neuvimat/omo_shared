package model.device.devices;

import model.device.DeviceState;

public interface LightState extends DeviceState {
    default void changeColor(LightColor color) {}
}
