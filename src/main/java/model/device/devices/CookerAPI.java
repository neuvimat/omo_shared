package model.device.devices;

import model.device.*;
import utils.Constants;

import java.util.Objects;

public class CookerAPI extends Device implements CookerState {
    public static final String categoryName = "Cooker";

    private CookerModel model;

    public CookerAPI(float deteriorationRate) {
        super(new Functionality(deteriorationRate));
        addConsumption(ConsumptionType.GAS, new Consumption(13,2,0));
        model = new CookerModel();
        model.setState(new CookerOff());
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void turnOn() {
        getState().turnOn();
    }

    @Override
    public void turnOff() {
        getState().turnOff();
    }

    @Override
    public void repair() {
        getState().repair();
    }

    @Override
    public void cook() {
        getState().cook();
    }

    @Override
    public boolean isBroken() {
        return getState().isBroken();
    }


    public CookerState getState() {
        return model.getState();
    }

    /**
     * Checks the filth of the cooker if it is in manageable levels
     * @return true if the cooker is usable, false if it is too filthy
     */
    public boolean isTooFilthy() {
        return getFilth() >= Constants.COOKER_MAX_BEARABLE_FILTH;
    }

    /**
     * Returns how much filth is accumulated on the cooker
     * @return filth represented in numeric value
     */
    public float getFilth() {
        return model.getFilth();
    }

    /**
     * Makes the cooker blissfully clean
     */
    public void clean() {
        model.setFilth(0);
    }

    private void makeDirtier(float filth) {
        model.setFilth(model.getFilth() + filth);
    }

    //============
    // STATES
    // ===========
    private class CookerOn implements CookerState {
        public CookerOn() {
            setConsumptionsActive();
        }

        @Override
        public void cook() {
            if (functionality.use()) {
                model.setState(new CookerBroken());
            }
            makeDirtier(Constants.COOKER_FILTH_PER_SESSION);
        }

        @Override
        public boolean isOn() {
            return true;
        }

        @Override
        public void turnOff() {
            model.setState(new CookerOff());
        }
    }

    private class CookerOff implements CookerState {
        public CookerOff() {
            setConsumptionsOff();
        }

        @Override
        public void cook() {

        }

        @Override
        public void turnOn() {
            model.setState(new CookerOn());
        }
    }

    private class CookerBroken implements CookerState {
        public CookerBroken() {
            setConsumptionsOff();
            executeBrokenEvent();
        }

        @Override
        public void cook() {

        }

        @Override
        public void repair() {
            functionality.repair();
            model.setState(new CookerOff());
        }
    }
}
