package model.device.devices;

import utils.Constants;

public class CookerModel {
    private CookerState state;
    private float filth = 0;

    public CookerState getState() {
        return state;
    }

    public void setState(CookerState state) {
        this.state = state;
    }

    public float getFilth() {
        return filth;
    }

    public void setFilth(float filth) {
        this.filth = filth;
        normalizeUpperDirtinessBound();
        normalizeLowerDirtinessBound();
    }

    private void normalizeUpperDirtinessBound() {
        if (filth > Constants.COOKER_MAX_BEARABLE_FILTH) {
            filth = 100;
        }
    }

    private void normalizeLowerDirtinessBound() {
        if (filth < 0) {
            filth = 0;
        }
    }
}
