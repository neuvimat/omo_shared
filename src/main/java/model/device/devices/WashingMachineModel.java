package model.device.devices;

import utils.Constants;

import java.time.Duration;

public class WashingMachineModel {
    private float clothesStored;
    private Duration lastTurnOn;
    private WashingMachineState state;

    public WashingMachineModel() {
        clothesStored = 0;
        lastTurnOn = Duration.ZERO;
    }

    public WashingMachineState getState() {
        return state;
    }

    public void setState(WashingMachineState state) {
        this.state = state;
    }

    public float getClothesStored() {
        return clothesStored;
    }

    public void setClothesStored(float clothesStored) {
        this.clothesStored = clothesStored;
        if (this.clothesStored < 0) {
            this.clothesStored = 0;
        }
        if (this.clothesStored > Constants.WASHING_MACHINE_CLOTH_CAPACITY) {
            this.clothesStored = Constants.WASHING_MACHINE_CLOTH_CAPACITY;
        }
    }

    public Duration getLastTurnOn() {
        return lastTurnOn;
    }

    public void setLastTurnOn(Duration lastTurnOn) {
        this.lastTurnOn = lastTurnOn;
    }
}
