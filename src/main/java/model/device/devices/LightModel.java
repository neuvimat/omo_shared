package model.device.devices;

public class LightModel {
    private LightState state;
    private LightColor actualColor;

    public LightModel(LightColor actualColor) {
        this.actualColor = actualColor;
    }

    public LightState getState() {
        return state;
    }

    public LightColor getActualColor() {
        return actualColor;
    }

    public void setState(LightState state) {
        this.state = state;
    }

    public void setActualColor(LightColor actualColor) {
        this.actualColor = actualColor;
    }
}
