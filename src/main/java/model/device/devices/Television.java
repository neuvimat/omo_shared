package model.device.devices;

import model.device.Device;
import model.device.Documentation;
import model.device.Functionality;

public class Television extends Device implements TelevisionState {
    public static final String categoryName = "Television";

    private TelevisionState state;
    private int channel;

    public Television(Functionality functionality) {
        super(functionality);
        channel = 0;
        state = new TelevisionOff(this);
    }

    public Television() {
        super(new Functionality(1f));
        channel = 0;
        state = new TelevisionOff(this);
    }

    private class TelevisionOn implements TelevisionState {
        private Television television;

        public TelevisionOn(Television television) {
            this.television = television;
        }

        @Override
        public boolean isOn() {
            return true;
        }

        @Override
        public void changeChannel(int chanel) {
            television.channel = chanel;
            if (functionality.use()) {
                state = new TelevisionBroken(television);
            }
        }

        @Override
        public void turnOff() {
            state = new TelevisionOff(television);
        }
    }

    private class TelevisionOff implements TelevisionState {
        private Television television;

        public TelevisionOff(Television television) {
            this.television = television;
        }

        @Override
        public void turnOn() {
            state = new TelevisionOn(television);
            if (functionality.use()) {
                state = new TelevisionBroken(television);
            }
        }
    }

    private class TelevisionBroken implements TelevisionState {
        private Television television;

        public TelevisionBroken(Television television) {
            this.television = television;
            television.channel = 0;
            television.executeBrokenEvent();
        }

        @Override
        public boolean isBroken() {
            return true;
        }

        @Override
        public void repair() {
            functionality.repair();
            state = new TelevisionOff(television);
        }
    }

    public int getChannel() {
        return channel;
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void turnOn() {
        state.turnOn();
    }

    @Override
    public void turnOff() {
        state.turnOff();
    }

    @Override
    public void repair() {
        state.repair();
    }

    @Override
    public boolean isBroken() {
        return state.isBroken();
    }

    @Override
    public void changeChannel(int chanel) {
        state.changeChannel(chanel);
    }
}
