package model.device.devices;

public class ComputerModel {

    private ComputerState state;
    private boolean isLagged = false;

    public boolean isLagged() {
        return isLagged;
    }

    public void setLagged(boolean lagged) {
        isLagged = lagged;
    }

    public ComputerModel(ComputerState state) {
        this.state = state;
    }

    public ComputerModel() {
    }

    public ComputerState getState() {
        return state;
    }

    public void setState(ComputerState state) {
        this.state = state;
    }
}
