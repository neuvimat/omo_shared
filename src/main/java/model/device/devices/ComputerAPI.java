package model.device.devices;

import model.device.*;

public class ComputerAPI extends Device {
    public static final String categoryName = "ComputerAPI";

    private ComputerModel computerModel = new ComputerModel(new ComputerOn(this));

    public ComputerAPI(float deteriorationRate) {
        super(new Functionality(deteriorationRate));
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(15,3,0));
    }

    public ComputerAPI() {
        super(new Functionality(2.5f));
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void turnOn() {
        getState().turnOn();
    }

    @Override
    public void turnOff() {
        getState().turnOff();
    }

    @Override
    public void repair() {
        getState().repair();
    }

    /**
     * If the device's state allows to, browse the web. Reduces functionality
     */
    public void browseInternet() {
        getState().browseWeb();
    }

    @Override
    public boolean isBroken() {
        return getState().isBroken();
    }

    private ComputerState getState() {
        return computerModel.getState();
    }

    //==============
    // States
    //==============
    private class ComputerOn implements ComputerState {
        ComputerAPI computerAPI;

        public ComputerOn(ComputerAPI computerAPI) {
            this.computerAPI = computerAPI;
            computerAPI.setConsumptionsActive();
        }

        @Override
        public boolean isOn() {
            return true;
        }

        @Override
        public void turnOff() {
            computerModel.setState(new ComputerOff(computerAPI));
        }

        @Override
        public void browseWeb() {
            if (functionality.use()) {
                computerModel.setState(new ComputerOff(computerAPI));
            }
        }
    }

    private class ComputerOff implements ComputerState {
        ComputerAPI computerAPI;

        public ComputerOff(ComputerAPI computerAPI) {
            this.computerAPI = computerAPI;
            computerAPI.setConsumptionsOff();
        }

        @Override
        public boolean isOn() {
            return true;
        }

        @Override
        public void turnOff() {
            computerAPI.computerModel.setState(new ComputerOff(computerAPI));
        }
    }

    private class ComputerBroken implements ComputerState {
        ComputerAPI computerAPI;

        public ComputerBroken(ComputerAPI computerAPI) {
            this.computerAPI = computerAPI;
            computerAPI.setConsumptionsOff();
            computerAPI.executeBrokenEvent();
        }

        @Override
        public void repair() {
            functionality.repair();
            computerModel.setState(new ComputerOff(computerAPI));
        }

        @Override
        public boolean isBroken() {
            return true;
        }
    }
}
