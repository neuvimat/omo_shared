package model.device.devices;

import controller.TimeTracker;
import model.actors.Living;
import model.device.*;
import utils.Constants;

import java.time.Duration;

public class FridgeAPI extends Device implements FridgeState {
    public static final String categoryName = "FridgeAPI";
    // The fridgeModel model
    private FridgeModel fridgeModel;
    private Duration lastWithdrawal = Duration.ZERO;

    public FridgeAPI(float percentageDeterioration) {
        super(new Functionality(percentageDeterioration));
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(12,10,0));
        this.fridgeModel = new FridgeModel();
        fridgeModel.setState(new FridgeOn(this));
    }

    public FridgeAPI() {
        super(new Functionality(3f));
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(12,10,0));
        this.fridgeModel = new FridgeModel();
        fridgeModel.setState(new FridgeOn(this));
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void feedLiving(Living living) {
        float before = fridgeModel.getFoodAmount();
        fridgeModel.getState().feedLiving(living);
    }

    private void eatFromStatic(Living living) {
        float livingConsumption = living.getHunger().getFoodAmountForFullStomach();
        float availableFood = withdraw(livingConsumption);
        living.getHunger().refreshAbsolute(availableFood);
    }

    /**
     *
     * @param amount
     * @return
     */
    public float withdraw(float amount) {
        Duration now = TimeTracker.getInstance().getActualTime();

        if (!(fridgeModel.getState() instanceof FridgeOn)) {
            // Food is spoiling while the fridgeModel is turned off!
            Duration timeDelta = now.minus(lastWithdrawal);
//            float old = fridgeModel.getFoodAmount();
            float after = fridgeModel.reduceFoodAmount(timeDelta.getSeconds() / Constants.FRIDGE_SPOIL_RATE);
//            System.out.println(old - after + " food spoiled");
            // todo: uncomment here for some spoiling fun
        }

        float disposable = Math.min(amount, fridgeModel.getFoodAmount());
        fridgeModel.setFoodAmount(fridgeModel.getFoodAmount() - disposable);

        this.lastWithdrawal = now;

        if (functionality.use()) {
            fridgeModel.setState(new FridgeBroken(this));
        }

        return disposable;
    }

    public void refill() {
        this.fridgeModel.setFoodAmount(fridgeModel.getFoodAmount() + Constants.FRIDGE_REFILL_AMOUNT);
    }

    @Override
    public boolean isOn() {
        return fridgeModel.getState().isOn();
    }

    @Override
    public void turnOn() {
        fridgeModel.getState().turnOn();
    }

    @Override
    public void turnOff() {
        fridgeModel.getState().turnOff();
    }

    @Override
    public void repair() {
        fridgeModel.getState().repair();
    }

    /**
     * Checks if the fridge is empty
     * @return true if the food amount in the fridge is 0
     */
    public boolean isEmpty() {
        return fridgeModel.getFoodAmount() <= 0;
    }

    /**
     * Returns the amount of food in fridgeModel left
     */
    public float getFoodLeft() {
        return fridgeModel.getFoodAmount();
    }


    // ==============================
    // STATES BELOW
    // ==============================
    private class FridgeOff implements FridgeState {
        FridgeAPI fridgeAPI;

        private FridgeOff(FridgeAPI fridgeAPI) {
            this.fridgeAPI = fridgeAPI;
            setConsumptionsOff();
        }

        @Override
        public void feedLiving(Living living) {
            eatFromStatic(living);
        }

        @Override
        public boolean isOn() {
            return false;
        }

        @Override
        public void turnOn() {
            fridgeAPI.setConsumptionsActive();
            fridgeAPI.fridgeModel.setState(new FridgeOn(fridgeAPI));
        }
    }

    private class FridgeOn implements FridgeState {
        FridgeAPI fridgeAPI;

        private FridgeOn(FridgeAPI fridgeAPI) {
            this.fridgeAPI = fridgeAPI;
            this.fridgeAPI.setConsumptionsActive();
        }

        @Override
        public void feedLiving(Living living) {
            eatFromStatic(living);
        }

        @Override
        public void turnOff() {
            fridgeAPI.setConsumptionsOff();
            fridgeAPI.fridgeModel.setState(new FridgeOff(fridgeAPI));
        }

        @Override
        public boolean isOn() {
            return true;
        }
    }

    private class FridgeBroken implements FridgeState{
        FridgeAPI fridgeAPI;
        private FridgeBroken(FridgeAPI fridgeAPI) {
            this.fridgeAPI = fridgeAPI;
            fridgeAPI.executeBrokenEvent();
        }

        @Override
        public void feedLiving(Living living) {
            eatFromStatic(living);
        }

        @Override
        public void repair() {
            functionality.repair();
            fridgeAPI.fridgeModel.setState(new FridgeOff(fridgeAPI));
        }
    }
}
