package model.device.devices;

import model.device.*;

public class LightAPI extends Device implements LightState {
    public static final String categoryName = "LightAPI";

    private LightModel lightModel;

    public LightAPI() {
        super(new Functionality(1));
        lightModel = new LightModel(LightColor.WHITE);
        lightModel.setState(new LightOff(this));
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(1));
    }

    public LightAPI(float percentageDeterioration, float activeConsumption) {
        super(new Functionality(percentageDeterioration));
        lightModel = new LightModel(LightColor.WHITE);
        lightModel.setState(new LightOff(this));
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(activeConsumption));
    }

    private class LightOff implements LightState {
        LightAPI lightAPI;

        private LightOff(LightAPI lightAPI) {
            this.lightAPI = lightAPI;
        }

        @Override
        public boolean isOn() {
            return true;
        }

        @Override
        public void turnOn() {
            lightAPI.lightModel.setState(new LightOn(lightAPI));
            lightAPI.setConsumptionsActive();
        }
    }

    private class LightOn implements LightState {
        LightAPI lightAPI;

        private LightOn(LightAPI lightAPI) {
            this.lightAPI = lightAPI;
        }

        @Override
        public void changeColor(LightColor color) {
            lightAPI.lightModel.setActualColor(color);
            if(lightAPI.functionality.use()){
                lightAPI.lightModel.setState(new LightBroken(lightAPI));
            }
        }

        @Override
        public void turnOff() {
            lightAPI.lightModel.setState(new LightOff(lightAPI));
            lightAPI.setConsumptionsOff();
        }
    }

    private class LightBroken implements LightState{
        LightAPI lightAPI;
        private LightBroken(LightAPI lightAPI) {
            this.lightAPI = lightAPI;
            lightAPI.executeBrokenEvent();
        }

        @Override
        public boolean isBroken() {
            return true;
        }

        @Override
        public void repair() {
            functionality.repair();
            lightAPI.lightModel.setState(new LightOff(lightAPI));
            lightAPI.setConsumptionsOff();
        }
    }

    @Override
    public void turnOn() {
        lightModel.getState().turnOn();
    }

    @Override
    public void turnOff() {
        lightModel.getState().turnOff();
    }

    @Override
    public boolean isBroken() {
        return lightModel.getState().isBroken();
    }

    @Override
    public void changeColor(LightColor color) {
        lightModel.getState().changeColor(color);
    }

    @Override
    public void repair() {
        lightModel.getState().repair();
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    public LightColor getActualColor() {
        return lightModel.getActualColor();
    }

    @Override
    public String getCategoryName() {
        return  categoryName;
    }
}
