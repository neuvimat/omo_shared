package model.device.devices;

import java.util.logging.Logger;

public class FridgeModel {
    private FridgeState state;
    private float foodAmount = 1000;
    private int concurrentUsers = 0;

    public FridgeModel() {
    }

    // Delete if it ends up unused
    public void addConcurrentUser() {
        this.concurrentUsers++;
    }

    // Delete if it ends up unused
    public void removeConcurrentUser() {
        this.concurrentUsers--;
        if (concurrentUsers < 0) {
            Logger.getLogger("").warning("FridgeModel reached negative concurrent users!");
        }
    }

    public float reduceFoodAmount(float amount) {
        foodAmount -= amount;
        if (foodAmount < 0) foodAmount = 0;
        return foodAmount;
    }

    public FridgeState getState() {
        return state;
    }
    public void setState(FridgeState state) {
        this.state = state;
    }
    public float getFoodAmount() {
        return foodAmount;
    }
    public void setFoodAmount(float foodAmount) {
        this.foodAmount = foodAmount;
    }
    public int getConcurrentUsers() {
        return concurrentUsers;
    }
    public void setConcurrentUsers(int concurrentUsers) {
        this.concurrentUsers = concurrentUsers;
    }
}
