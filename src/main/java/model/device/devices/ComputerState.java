package model.device.devices;

import model.device.DeviceState;

public interface ComputerState extends DeviceState {
    default void browseWeb() {}
}
