package model.device.devices;

import model.actors.Living;
import model.actors.livings.Human;
import model.device.DeviceState;

public interface FridgeState extends DeviceState {
    default void feedLiving(Living living) {}
}
