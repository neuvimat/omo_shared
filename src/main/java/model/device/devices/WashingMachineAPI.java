package model.device.devices;

import OLD.utility.Random;
import controller.TimeTracker;
import model.device.*;
import utils.Constants;
import utils.RandomUtils;

import java.time.Duration;

public class WashingMachineAPI extends Device {
    public static final String categoryName = "Washing Machine";

    private WashingMachineModel model;

    public WashingMachineAPI() {
        super(new Functionality(5f));
        addConsumption(ConsumptionType.WATER, new Consumption(40, 0, 0));
        model = new WashingMachineModel();
        model.setState(new WashingMachineOff());
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public void turnOn() {
        getState().turnOn();
    }

    @Override
    public void turnOff() {
        getState().turnOff();
    }

    @Override
    public void repair() {
        getState().repair();
    }

    @Override
    public boolean isOn() {
        return getState().isOn();
    }

    public WashingMachineState getState() {
        return model.getState();
    }

    public Duration getLastTurnOn() {
        return model.getLastTurnOn();
    }

    public float getClotherRemaining() {
        return model.getClothesStored();
    }

    public void hangClothes() {
        model.setClothesStored(model.getClothesStored() - (1 + RandomUtils.getInstance().nextInt(3)));
    }

    //==================
    // STATES
    //==================

    private class WashingMachineOn implements WashingMachineState {

        public WashingMachineOn() {
            if (functionality.use()) {
                model.setState(new WashingMachineBroken());
                executeBrokenEvent();
            }
            else {
                setConsumptionsActive();
                model.setLastTurnOn(TimeTracker.getInstance().getActualTime());
                model.setClothesStored(Constants.WASHING_MACHINE_CLOTH_CAPACITY);
            }
        }

        @Override
        public boolean isOn() {
            return true;
        }

        @Override
        public void turnOff() {
            model.setState(new WashingMachineOff());
        }
    }

    private class WashingMachineOff implements WashingMachineState {

        public WashingMachineOff() {
            setConsumptionsOff();
        }

        @Override
        public void turnOn() {
            model.setState(new WashingMachineOn());
        }
    }

    private class WashingMachineBroken implements WashingMachineState {

        public WashingMachineBroken() {
            setConsumptionsOff();
        }

        @Override
        public void repair() {
            functionality.repair();
            model.setState(new WashingMachineOn());
        }

        @Override
        public boolean isBroken() {
            return true;
        }
    }
}
