package model.device.devices;

public enum LightColor {
    WHITE,
    YELLOW,
    RED,
    BLUE,
    GREEN
}
