package model.device.devices;

import controller.event.DisposableEvent;
import model.device.Device;

public class BrokenEvent implements DisposableEvent {
    private Device device;

    public BrokenEvent(Device device) {
        this.device = device;
    }

    public Device getDevice() {
        return device;
    }

    public static BrokenEvent getBlank(){
        return new BrokenEvent(null);
    }

    @Override
    public String getCategoryName() {
        return "BrokenEvent";
    }
}
