package model.device.devices;

import model.device.*;

public class Heating extends Device implements HeatingState {
    public static final String categoryName = "Heating";

    private HeatingState state;
    private HeatingRegulation regulation;

    public Heating() {
        super(new Functionality(0.1f));
        state = new HeatingOff(this);
        regulation = HeatingRegulation.NORMAL;
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(10));
    }

    public Heating(float percentageDeterioration, float activeConsunption) {
        super(new Functionality(percentageDeterioration));
        state = new HeatingOff(this);
        regulation = HeatingRegulation.NORMAL;
        addConsumption(ConsumptionType.ELECTRICITY, new Consumption(activeConsunption));
    }

    private class HeatingOff implements HeatingState {
        Heating heating;

        private HeatingOff(Heating heating) {
            this.heating = heating;
        }

        @Override
        public void regulate(HeatingRegulation regulation) {
            heating.regulation = regulation;
            if(heating.functionality.use()){
                heating.state = new HeatingBroken(heating);
            }
            else {
                switch (regulation){
                    case NORMAL:
                        heating.setConsumptionsIdle();
                        break;
                    case MAX:
                        heating.setConsumptionsActive();
                        break;
                }
            }
        }

        @Override
        public void turnOn() {
            heating.state = new HeatingOn(heating);
            heating.setConsumptionsActive();
        }
    }

    private class HeatingOn implements HeatingState {
        Heating heating;

        private HeatingOn(Heating heating) {
            this.heating = heating;
        }

        @Override
        public void turnOff() {
            heating.state = new HeatingOff(heating);
            heating.setConsumptionsOff();
        }
    }

    private class HeatingBroken implements HeatingState{
        Heating heating;

        private HeatingBroken(Heating heating) {
            this.heating = heating;
            heating.executeBrokenEvent();
        }

        @Override
        public boolean isBroken() {
            return true;
        }

        @Override
        public void repair() {
            functionality.repair();
            heating.state = new HeatingOff(heating);
            heating.setConsumptionsOff();
        }
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public void turnOn() {
        state.turnOn();
    }

    @Override
    public void turnOff() {
        state.turnOff();
    }

    @Override
    public void regulate(HeatingRegulation regulation) {
        state.regulate(regulation);
    }

    @Override
    public void repair() {
        state.repair();
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }
}
