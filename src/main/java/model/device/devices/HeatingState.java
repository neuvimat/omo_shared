package model.device.devices;

import model.device.DeviceState;

public interface HeatingState extends DeviceState {
    default void regulate(HeatingRegulation regulation) {}
}
