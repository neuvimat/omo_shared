package model.device.devices;

import model.device.*;

public class Window extends Device{
    public static final String categoryName = "Window";
    private DeviceState state;

    public Window() {
        super(new Functionality(0.1f));
        state = new WindowClosed(this);
    }

    private class WindowClosed implements DeviceState {
        Window window;

        private WindowClosed(Window window) {
            this.window = window;
        }

        @Override
        public void turnOn() {
            if(functionality.use()){
                window.state = new WindowBroken(window);
            }
            else{
                window.state = new WindowOpen(window);
            }
        }
    }

    private class WindowOpen implements DeviceState {
        Window window;

        private WindowOpen(Window window) {
            this.window = window;
        }

        @Override
        public void turnOff() {
            if(functionality.use()){
                window.state = new WindowBroken(window);
            }
            else{
                window.state = new WindowClosed(window);
            }
        }
    }

    private class WindowBroken implements DeviceState{
        Window window;

        private WindowBroken(Window window) {
            this.window = window;
            window.executeBrokenEvent();
        }

        @Override
        public boolean isBroken() {
            return true;
        }

        @Override
        public void repair() {
            functionality.repair();
            window.state = new WindowClosed(window);
        }
    }

    @Override
    public void accept(Documentation documentation) {
        documentation.visitDevice(this);
    }

    @Override
    public void turnOn() {
        state.turnOn();
    }

    @Override
    public void turnOff() {
        state.turnOff();
    }

    @Override
    public void repair() {
        state.repair();
    }

    @Override
    public String getCategoryName() {
        return categoryName;
    }
}
