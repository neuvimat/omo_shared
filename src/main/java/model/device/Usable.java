package model.device;

import utils.CategoryNamed;

public interface Usable extends CategoryNamed {
    void startUsing();
    void stopUsing();
    boolean isBeingUsed();
}
