package model.device;

public enum ConsumptionType {
    ELECTRICITY,
    GAS,
    WATER
}
