package model.device;

public interface DeviceSubject {
    void accept(Documentation documentation);
}
