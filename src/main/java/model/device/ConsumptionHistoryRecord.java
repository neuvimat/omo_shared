package model.device;

import java.time.Duration;

public class ConsumptionHistoryRecord {
    private Duration time;
    private float value;

    public ConsumptionHistoryRecord(Duration time, float value) {
        this.time = time;
        this.value = value;
    }

    public Duration getTime() {
        return time;
    }

    public float getValue() {
        return value;
    }
}
