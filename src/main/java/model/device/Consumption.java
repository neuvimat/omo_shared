package model.device;

import controller.TimeTracker;
import java.time.Duration;

import java.util.ArrayList;
import java.util.List;

public class Consumption{

    private float active;
    private float idle;
    private float off;
    private List<ConsumptionHistoryRecord> history;
    private TimeTracker timeTracker;

    public Consumption(float active) {
        this.active = active;
        this.idle = 0;
        this.off = 0;
        history = new ArrayList<>();
        timeTracker = TimeTracker.getInstance();
    }

    public Consumption(float active, float idle) {
        this.active = active;
        this.idle = idle;
        this.off = 0;
        history = new ArrayList<>();
        timeTracker = TimeTracker.getInstance();
    }

    public Consumption(float active, float idle, float off) {
        this.active = active;
        this.idle = idle;
        this.off = off;
        history = new ArrayList<>();
        timeTracker = TimeTracker.getInstance();
    }

    public void setOff(){
        history.add(new ConsumptionHistoryRecord(timeTracker.getActualTime(), off));
    }
    public void setActive(){
        history.add(new ConsumptionHistoryRecord(timeTracker.getActualTime(), active));
    }
    public void setIdle(){
        history.add(new ConsumptionHistoryRecord(timeTracker.getActualTime(), idle));
    }

    /**
     * Gets total consumption in specified interval.
     * @param from from consumptions
     * @param to to consumptions
     * @return total consumption in specified interval
     */
    public float getConsumption(Duration from, Duration to) {
        float sum = 0;
        for (int i = 1; i < history.size(); i++){
            ConsumptionHistoryRecord record1 = history.get(i - 1);
            ConsumptionHistoryRecord record2 = history.get(i);
            Duration time1 = record1.getTime();
            Duration time2 = record2.getTime();
            if(time1.compareTo(to) < 0 && time2.compareTo(from) > 0){ // eg it has intersection with interval (from, to)
                Duration interval;
                if(time1.compareTo(from) < 0){ // intersecting (from, to) from left
                    interval = time2.minus(from);
                }
                else if(time2.compareTo(to) > 0){ // intersecting (from, to) from right
                    interval = to.minus(time1);
                }
                else{ // included in (from, to)
                    interval = time2.minus(time1);
                }
                sum += record1.getValue() * getHours(interval);
            }
        }
        if(history.size() > 0){
            ConsumptionHistoryRecord lastRecord = history.get(history.size() - 1);
            Duration time = lastRecord.getTime();
            if(time.compareTo(to) < 0){
                Duration interval = to.minus(time);
                sum += lastRecord.getValue() * getHours(interval);
            }
        }
        return sum;
    }

    private float getHours(Duration duration){
        long sec = duration.getSeconds();
        float result = (float)sec / 3600f;
        return result;
    }
}
