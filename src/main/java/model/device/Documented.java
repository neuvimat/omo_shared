package model.device;

public interface Documented {
    Documentation getDocumentation();
}
