package model.device;

import controller.TimeTracker;
import controller.event.DisposableEventExecution;
import controller.event.EventSource;
import controller.event.GlobalEventSystem;
import model.device.devices.BrokenEvent;
import view.ConsumptionReportable;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public abstract class Device
        implements DeviceState, ConsumptionReportable, Usable,
        Documented, DeviceSubject, EventSource
{
    private boolean isBeingUsed = false;
    private DocumentationProxy documentationProxy;
    protected Map<ConsumptionType, Consumption> consumptions;
    protected Functionality functionality;

    protected Device(Functionality functionality) {
        this.consumptions = new HashMap<>();
        this.functionality = functionality;
        documentationProxy = new DocumentationProxy(this);
    }

    protected void addConsumption(ConsumptionType type, Consumption consumption){
        if(!consumptions.containsKey(type)){
            consumptions.put(type, consumption);
        }
    }

    protected Consumption getConsumtion(ConsumptionType type){
        return consumptions.get(type);
    }

    public boolean hasConsumption(ConsumptionType type) {
        return consumptions.containsKey(type);
    }

    public boolean hasAnyConsumption() { return consumptions.size() > 0; }

    protected void setConsumptionsActive(){
        for(Consumption consumption: consumptions.values()){
            consumption.setActive();
        }
    }

    protected void setConsumptionsOff(){
        for(Consumption consumption: consumptions.values()){
            consumption.setOff();
        }
    }

    protected void setConsumptionsIdle(){
        for(Consumption consumption: consumptions.values()){
            consumption.setIdle();
        }
    }
    
    public float getTotalConsumption(ConsumptionType type, Duration from, Duration to) {
        if(!consumptions.containsKey(type)){
            return 0;
        }
        Consumption consumption = consumptions.get(type);
        return consumption.getConsumption(from, to);
    }
    
    public float getFunctionalityPercent(){
        return functionality.getActualPercent();
    }

    public void damageFunctionality() {
        this.functionality.use();
    }
    
    public model.device.Documentation getDocumentation() {
        return documentationProxy.getDocumentation();
    }

    @Override
    public void startUsing() {
        isBeingUsed = true;
    }

    @Override
    public void stopUsing() {
        isBeingUsed = false;
    }

    @Override
    public boolean isBeingUsed() {
        return isBeingUsed;
    }

    protected void executeBrokenEvent(){
        GlobalEventSystem.getInstance().execute(new DisposableEventExecution(
            new BrokenEvent(this), this, TimeTracker.getInstance().getActualTime()
        ));
    }
}
