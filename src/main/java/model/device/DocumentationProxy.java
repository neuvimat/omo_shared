package model.device;

public class DocumentationProxy implements Documented {
    private Documentation documentation = null;
    private Device device;

    public DocumentationProxy(Device device) {
        this.device = device;
    }

    @Override
    public Documentation getDocumentation() {
        if(documentation == null){
            documentation = new Documentation(device);
        }
        return documentation;
    }
}
