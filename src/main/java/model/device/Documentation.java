package model.device;

import model.conveyance.Car;
import model.device.devices.*;

public class Documentation implements DeviceVisitor {
    private Device device;
    private String description = "";
    private RepairSolution repairSolution = RepairSolution.REPAIR_YOURSELF;

    public Documentation(Device device) {
        this.device = device;
        device.accept(this);
    }

    public String getDescription() {
        return description;
    }

    public RepairSolution getRepairSolution() {
        return repairSolution;
    }

    public void visitDevice(LightAPI device) {
        repairSolution = RepairSolution.REPAIR_YOURSELF;
    }

    public void visitDevice(Heating device) {
        repairSolution = RepairSolution.REPAIR_YOURSELF;
    }

    public void visitDevice(Window device) {
        repairSolution = RepairSolution.BUY_NEW;
    }

    public void visitDevice(WashingMachineAPI device) {
        repairSolution = RepairSolution.BUY_NEW;
    }

    public void visitDevice(Television device) {
        repairSolution = RepairSolution.CALL_REPAIRMAN;
    }

    public void visitDevice(FridgeModel device) {
        repairSolution = RepairSolution.REPAIR_YOURSELF;
    }

    public void visitDevice(CookerAPI device) {
        repairSolution = RepairSolution.BUY_NEW;
    }

    public void visitDevice(ComputerAPI device) {
        repairSolution = RepairSolution.CALL_REPAIRMAN;
    }

    @Override
    public void visitDevice(FridgeAPI fridgeAPI) {
        repairSolution = RepairSolution.REPAIR_YOURSELF;
    }

    @Override
    public void visitDevice(Car car) {
        repairSolution = RepairSolution.CALL_REPAIRMAN;
    }
}
