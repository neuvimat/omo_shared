package model.device;

import model.conveyance.Car;
import model.device.devices.*;

public interface DeviceVisitor {
    void visitDevice(LightAPI device);
    void visitDevice(Heating device);
    void visitDevice(Window device);
    void visitDevice(WashingMachineAPI device);
    void visitDevice(Television device);
    void visitDevice(FridgeModel device);
    void visitDevice(CookerAPI device);
    void visitDevice(ComputerAPI device);

    // Todo: transition to API sheet
    void visitDevice(FridgeAPI fridgeAPI);

    void visitDevice(Car car);
}
