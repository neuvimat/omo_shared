package model.device;

public interface DeviceState {
    /**
     * @return true if the device is on
     */
    default boolean isOn() { return false; }

    /**
     * Attempts to turn on the device
     */
    default void turnOn() {}

    /**
     * Attempts to turn off the device
     */
    default void turnOff() {}

    /**
     * @return true if the device is broken
     */
    default boolean isBroken() {return false;}

    /**
     * Attempt to repair the device
     */
    default void repair() {}
}
