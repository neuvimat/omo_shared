package model.device;

public enum RepairSolution {
    BUY_NEW,
    REPAIR_YOURSELF,
    CALL_REPAIRMAN
}
