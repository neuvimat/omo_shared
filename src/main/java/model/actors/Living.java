package model.actors;

import controller.TimeTracker;
import controller.action.Action;
import controller.action.ActionNoArgs;
import controller.action.ActionSingleArgs;

import java.sql.Time;
import java.time.Duration;
import java.util.logging.Logger;

import model.actors.livings.needs.Hunger;
import model.configuration.House;
import model.configuration.Room;
import utils.BasicNamed;
import utils.DurationUtil;

/**
 * Actor that can move in house and have some basic life constrains.
 */
public abstract class Living extends Actor implements BasicNamed {

    private static int idGen = 0;
    private int id;
    private Hunger hunger;
    private boolean alive = true;
    private Duration timeOfDeath;

    protected Living(House house, String name, float maxHunger) {
        super(house, name);
        id = idGen++;
        hunger = new Hunger(this, maxHunger);
        house.registerLiving(this);
    }

    protected Living(House house, String name) {
        super(house, name);
        id = idGen++;
        hunger = new Hunger(this);
        house.registerLiving(this);
    }

    protected Living(House house) {
        super(house, "Unnamed Living");
        id = idGen++;
        hunger = new Hunger(this);
        house.registerLiving(this);
    }


    /**
     * Takes into consideration the difference between the last time the entity was updated and the inputted duration
     * Does some basic things like lowering hunger for example
     * @param now
     */
    @Override
    public boolean update(Duration now) {
        Duration delta = now.minus(lastUpdated);
        if (hunger.drain(delta) == 0) {
            die();
            return false;
        }
        lastUpdated = now;
        return true;
    }

    /**
     * Clean up after yourself, each type of entity might have left a different kind of mess behind
     */
    abstract protected void dieImpl();

    /**
     * Do the specific dying based on the entity type and then the fixed operations such as removal from the house,
     * firing an die event, etc...
     */
    public void die() {
        System.out.println("R.I.P. " + getName() + ". Time of death: " + DurationUtil.format(TimeTracker.getInstance().getActualTime()) + ". Some fun facts: " + getName() + " has eaten total of " + this.getHunger().getTotalFoodEaten() + " units of food!");
        alive = false;
        timeOfDeath = timeTracker.getActualTime();
        dieImpl();
    }

    public Duration getTimeOfDeath() {
        return timeOfDeath;
    }

    public boolean isAlive() {
        return alive;
    }

    public int getId() {
        return id;
    }

    public boolean isInRoom(Room room){
        return room.equals(house.findRoomByLiving(this));
    }

    @Override
    public int hashCode() {
        return id;
    }

    public Action moveToRoomAction(Room room) {
        return new ActionSingleArgs<Room>("Going to " + room.getName(), this, Duration.ofMinutes(3),
                (Room r) -> house.moveLivingToRoom(this, r), room);
    }

    public Action moveOutsideAction() {
        return new ActionNoArgs("Going outside", this, Duration.ofMinutes(5),
                () -> house.moveLivingOutside(this));
    }

    public Action waitAction(Duration duration) {
        return new ActionNoArgs("Waiting", this, duration, () -> {});
    }

    public Hunger getHunger() {
        return this.hunger;
    }

    public void overrideBasicHunger(Hunger hunger) {
        this.hunger = hunger;
    }
}
