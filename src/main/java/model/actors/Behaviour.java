package model.actors;

import controller.action.Action;

/**
 * Represents brain of some actor, that is responsible for executed actions.
 */
public interface Behaviour {
    /**
     * @return Action which actor has come up with.
     */
    Action think();
}
