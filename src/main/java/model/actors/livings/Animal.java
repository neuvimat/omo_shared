package model.actors.livings;

import controller.TimeTracker;
import controller.action.Action;
import controller.action.ActionNoArgs;
import controller.action.ActionSingleArgs;
import controller.event.EventExecution;
import controller.event.GlobalEventSystem;
import model.actors.Living;
import model.actors.behaviours.AnimalBehaviour;
import model.actors.behaviours.strategies.HungryAnimalEvent;
import model.configuration.House;
import model.configuration.Room;
import model.device.Device;
import utils.RandomUtils;

import java.time.Duration;

/**
 * Non-human living being.
 */
public class Animal extends Living {

    private AnimalBehaviour behaviour;
    private String species;

    public Animal(House house, AnimalBehaviour behaviour, String name, String species) {
        super(house, name);
        this.behaviour = behaviour;
        this.behaviour.setActor(this);
        this.species = species;
    }

    @Override
    public Action getNextAction() {
        return behaviour.think();
    }

    @Override
    protected void dieImpl() {

    }

    @Override
    public String getCategoryName() {
        return species;
    }


    public Action wanderAround() {
        Room room = RandomUtils.getInstance().getRandomFromList(house.getAllRooms());
        return new ActionSingleArgs<Room>("Wandering around " + room.getName(), this, Duration.ofSeconds(60*20 + randomUtils.nextInt(60*20)),
                (Room r) -> house.moveLivingToRoom(this, r), room);
    }

    public Action scratch(Device target) {
        return new ActionNoArgs("Scratching", this, Duration.ofSeconds(randomUtils.nextInt(300) + 5), target, () -> {
            // todo: possibly add damage to the device?
        });
    }

    public Action announceHunger(String sound) {
        return new ActionNoArgs(sound,  this, Duration.ofMinutes(5), () -> {
            GlobalEventSystem.getInstance().execute(new EventExecution(new HungryAnimalEvent(this), this, TimeTracker.getInstance().getActualTime()));
        });
    }

    public Action chaseOwnTail() {
        Duration duration = Duration.ofMinutes(10 + randomUtils.nextInt(25));
        return new ActionNoArgs("Chasing its own tail",  this, Duration.ofMinutes(10 + randomUtils.nextInt(25)), () -> {
            this.getHunger().drain(duration.dividedBy(5));
        });
    }
}
