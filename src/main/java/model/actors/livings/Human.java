package model.actors.livings;

import controller.action.*;
import model.actors.Living;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.HumanBehaviour;
import model.configuration.House;
import model.conveyance.Conveyance;
import model.conveyance.Car;
import model.device.Device;
import model.device.DeviceState;
import model.device.Documentation;
import model.device.RepairSolution;
import model.device.devices.CookerAPI;
import model.device.devices.LightAPI;
import model.device.devices.LightColor;
import model.device.devices.Television;
import utils.RandomUtils;

import java.time.Duration;

/**
 * Human being.
 */
public class Human extends Living {
    private HumanBehaviour behaviour;
    private String surname;
    private boolean adult;
    private int repairRank;
    private float savedByCooking = 0;

    public Human(House house, HumanBehaviour behaviour, String name, String surname, boolean adult) {
        super(house, name);
        this.behaviour = behaviour;
        this.behaviour.setActor(this);
        this.surname = surname;
        this.adult = adult;
        repairRank = adult ? 5 : 0;
    }

    public Human(House house, HumanBehaviour behaviour, String name, String surname, boolean adult, float maxHunger) {
        super(house, name, maxHunger);
        this.behaviour = behaviour;
        this.behaviour.setActor(this);
        this.surname = surname;
        this.adult = adult;
        repairRank = adult ? 5 : 0;
    }

    public Human(House house, HumanBehaviour behaviour, String name, String surname, boolean adult, float maxHunger, int repairRank) {
        super(house, name, maxHunger);
        this.behaviour = behaviour;
        this.behaviour.setActor(this);
        this.surname = surname;
        this.adult = adult;
        this.repairRank = repairRank;
    }

    public void addToDo(HumanBehaviorStrategy todo) {
        behaviour.addTodo(todo);
    }

    @Override
    public String getName() {
        return name + " " + surname;
    }

    public String getFirstname() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getRepairRank() {
        return repairRank;
    }

    public Action getNextAction() {
        return behaviour.think();
    }

    public Action turnDeviceOnAction(Device device){
        return new ActionSingleArgs<Device>("Turning device on", this, Duration.ofSeconds(2), device,
                DeviceState::turnOn, device);
    }

    public Action turnDeviceOnAction(Device device, String customMessage){
        return new ActionSingleArgs<Device>(customMessage, this, Duration.ofSeconds(2), device,
                DeviceState::turnOn, device);
    }

    public Action turnDeviceOffAction(Device device){
        return new ActionSingleArgs<Device>("Turning device off", this, Duration.ofSeconds(2), device,
                DeviceState::turnOff, device);
    }

    public Action changeLightColorAction(LightAPI lightAPI, LightColor color){
        return new ActionTwoArgs<LightAPI, LightColor>("Changing lightAPI color to " + color, this, Duration.ofSeconds(2), lightAPI,
                LightAPI::changeColor, lightAPI, color);
    }

    public Action changeTVChanelAction(Television television, Integer chanel){
        return new ActionTwoArgs<Television, Integer>("Changing TV channel to " + chanel, this, Duration.ofSeconds(1), television,
                Television::changeChannel, television, chanel);
    }

    public Action watchTVAction(Television television, Duration duration) {
        return new ActionNoArgs("Watching TV", this, duration, television, () -> {});
    }

    public Action doSportAction(Conveyance conveyance){
        Duration duration = Duration.ofMinutes(20 + RandomUtils.getInstance().nextInt(55));
        return new ActionNoArgs("Doing sport", this, duration, conveyance, () -> {
            // burning some additional calories due to sporting, effectively 50% more
            this.getHunger().drain(duration.dividedBy(2));
        });
    }

    public Action driveCar(Duration duration, Car car) {
        car.startUsing();
        this.turnDeviceOnAction(car);
        return new ActionNoArgs("Driving car", this, duration, car, () -> {});
    }

    public Action feedAnimal(Animal animal) {
        return new ActionNoArgs("Feeding animal", this, Duration.ofMinutes(5), animal, () -> {
            animal.getHunger().refreshFully();
        });
    }

    /**
     * Parks the car, turns it off and stops using it.
     */
    public Action parkCarAtHome(Duration duration, Car car) {
        car.stopUsing();
        this.turnDeviceOffAction(car);
        return new ActionNoArgs("Parking car at home", this, duration, car, () -> {});
    }

    /**
     * Parks the car but disallows other to use it for now.
     */
    public Action parkCarButKeepUsing(Duration duration, Car car) {
        this.turnDeviceOffAction(car);
        return new ActionNoArgs("Parking car somewhere", this, duration, car, () -> {});
    }

    public Action useCooker(CookerAPI cooker) {
        return new ActionNoArgs("Cooking", this, Duration.ofMinutes(90 + randomUtils.nextInt(60)), cooker, ()->{
            cooker.cook();
        });
    }

    public Action repairDevice(Device device){
        Documentation documentation = device.getDocumentation();
        RepairSolution solution = documentation.getRepairSolution();
        switch (solution){
            case BUY_NEW:
                return repairDeviceSolution(device, "Buying new device");
            case REPAIR_YOURSELF:
                return repairDeviceSolution(device, "Repairing myself");
            case CALL_REPAIRMAN:
                return repairDeviceSolution(device, "Calling with repairman");
        }
        return repairDeviceSolution(device, "Repairing myself");
    }

    private Action repairDeviceSolution(Device device, String solution){
        return new ActionSingleArgs<Device>(solution,this, Duration.ofHours(1), device,
            (Device d) -> {
                d.repair();
            }, device);
    }

    @Override
    protected void dieImpl() {
        // Remove self from the action queue, stop listening for events, etc...
    }

    @Override
    public String getCategoryName() {
        return "Human";
    }

    /**
     * Add record of how much food from fridge this human have saved by cooking for himself! Just a flavor stat.
     * @param amount Amount of food saved
     * @return Total amount of food saved
     */
    public float savedByCooking(float amount) {
        return savedByCooking += amount;
    }

    public float getSavedByCooking() {
        return savedByCooking;
    }

    public boolean isAdult() {
        return adult;
    }

    public Action doSocialAction(Living target, float socialFactor) {
        Duration duration = Duration.ofSeconds(120 + RandomUtils.getInstance().nextInt((int)(120 * socialFactor)));
        // All that talk makes Livings hungry
        return new ActionNoArgs(RandomUtils.getInstance().getSocialActionDescription(), this, duration, target, () -> {
            target.getHunger().reducePercentage(socialFactor/4.5f);
        });
    }
}
