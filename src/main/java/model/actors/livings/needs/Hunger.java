package model.actors.livings.needs;

import model.actors.Living;

import java.time.Duration;

public class Hunger {
    private Living living;
    private float max;
    private float current;
    private float drain;
    private float totalFoodEaten = 0;

    public Hunger(Living living) {
        this.living = living;
        max = 100;
        current = 100;
        drain = 1.38f;
    }

    public Hunger(Living living, float max) {
        this.living = living;
        this.max = max;
        this.current = max;
        this.drain = max/80;
    }

    public Hunger(Living living, float max, float hoursUntilDeath) {
        this.living = living;
        this.max = max;
        this.current = max;
        this.drain = max/hoursUntilDeath;
    }

    /**
     * Sets current hunger to max, making the Living fully sated
     * @return amount of food actually needed to refresh to full
     */
    public float refreshFully() {
        float eaten = max-current;
        totalFoodEaten += eaten;
        current = max;
        return eaten;
    }

    /**
     * Refreshes x percent of maximum hunger
     * @param percentage
     * @return Percentage of current hunger status (1=full, 0=starving)
     */
    public float refreshPercentage(float percentage) {
        float old = current;
        current += this.max * percentage;
        if (current > max) current = max;
        totalFoodEaten += current - old;
        return current/max;
    }

    /**
     * Adds the inputted amount of hunger units to the hunger meter
     * @param amount hunger meter units
     * @return Absolute amount of current hunger status (0 = starving)
     */
    public float refreshAbsolute(float amount) {
        float old = current;
        current += amount;
        normalizeUpperBound();
        totalFoodEaten += current-old;
        return current;
    }

    /**
     * Reduces hunger based on time. Used the hunger drain property.
     * @param timeDelta
     * @return current hunger after drain
     */
    public float drain(Duration timeDelta) {
        current -= drain * (float)timeDelta.getSeconds() / 3600f;
        normalizeLowerBound();
        return current;
    }

    /**
     * Get the maximum hunger
     * @return
     */
    public float getMax() {
        return max;
    }

    /**
     * Return current numeric representation of hunger
     * @return
     */
    public float getCurrent() {
        return current;
    }

    /**
     * Get how much hunger drains per hour
     * @return
     */
    public float getDrain() {
        return drain;
    }

    /**
     * Get the current hunger as percentage in relation to maximum hunger
     * @return
     */
    public float getPercentageHunger() {
        return current/max;
    }

    /**
     * Self explanatory
     * @return
     */
    public float getFoodAmountForFullStomach() {
        return max - current;
    }

    /**
     * Self explanatory
     * @return
     */
    public float getTotalFoodEaten() {
        return this.totalFoodEaten;
    }

    /**
     * Reduces the hunger meter by percentage in relation to max hunger
     * @param percentage
     */
    public void reducePercentage(float percentage) {
        current -= max*percentage;
        normalizeLowerBound();
    }

    /**
     * Sets the current hunger to the inputted percentage
     * @param percentage percentage [0,1]
     */
    public void setCurrentPercentage(float percentage) {
        this.current = max*percentage;
        normalizeLowerBound();
        normalizeUpperBound();
    }

    private void normalizeLowerBound() {
        if (current < 0) {
            current = 0;
        }
    }

    private void normalizeUpperBound() {
        if (current > max) {
            current = max;
        }
    }
}
