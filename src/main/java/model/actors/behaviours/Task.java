package model.actors.behaviours;

import controller.action.Action;

/**
 * Represent some sequence of actions to be executed.
 */
public interface Task {
    /**
     * @return next action in sequence
     */
    Action getNextStep();

    /**
     * @return true if there is action to be executed
     */
    boolean hasNextStep();

    /**
     * Stops the sequence of actions
     */
    void cancelTask();
}
