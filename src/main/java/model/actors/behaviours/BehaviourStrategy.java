package model.actors.behaviours;

import controller.action.Action;
import utils.RandomUtils;

public abstract class BehaviourStrategy
{
    protected RandomUtils randomUtils = RandomUtils.getInstance();
    protected Task task;

    protected BehaviourStrategy(Task task)
    {
        this.task = task;
    }

    public Action getNextAction()
    {
        return task.getNextStep();
    }
    public boolean hasNextAction()
    {
        return task.hasNextStep();
    }
}
