package model.actors.behaviours;

import controller.TimeTracker;
import controller.event.DisposableEventListenerImpl;
import controller.event.EventListenerOwner;
import controller.event.GlobalEventSystem;
import model.actors.Living;
import model.actors.behaviours.strategies.human.*;
import model.actors.livings.Human;
import model.device.devices.BrokenEvent;
import model.device.devices.CookerAPI;
import model.device.devices.FridgeAPI;
import model.device.devices.WashingMachineAPI;
import utils.Constants;
import utils.RandomUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ParametrizedHumanBehavior extends HumanBehaviour {


    public ParametrizedHumanBehavior(Builder builder) {
        this.choirsChance = builder.choirsChance;
        this.hungryAtPercent = builder.hungryAtPercent;
        this.fridgeRecheckTime = builder.fridgeRecheck;
        this.refillFridge = builder.refillFridge;
        this.socializeFactor = builder.socializeFactor;
        this.talksToAnimals = builder.talksToAnimals;
        this.playsWithLights = builder.playsWithLights;
        this.canCook = builder.canCook;
        this.cookChance = builder.cookChance;

        this.todoList = new ArrayList<>();
    }

    protected Duration lastFridgeCheckTime = Duration.ZERO;
    protected Duration lastSocializeTime = Duration.ZERO;

    protected float fridgeRecheckTime;
    protected float choirsChance;
    protected float hungryAtPercent;
    protected float socializeFactor;
    protected float cookChance;

    protected boolean refillFridge;
    protected boolean talksToAnimals;
    protected boolean playsWithLights;
    protected boolean canCook;

    public void setActor(Human actor) {
        this.actor = actor;
        GlobalEventSystem.getInstance().attach(new DisposableEventListenerImpl<BrokenEvent>(
                BrokenEvent.getBlank(), this, this::onDeviceBrokenEvent, actor.getRepairRank()
        ));
    }

    private void onDeviceBrokenEvent(BrokenEvent event){
        addTodo(new RepairDeviceStrategy(actor, event.getDevice()));
    }

    @Override
    protected HumanBehaviorStrategy setStrategy() {
        if (!todoList.isEmpty()) {
            strategy = todoList.get(0);
            todoList.remove(0);
            return strategy;
        }
        // Eat if hungry, eat more likely the more hungry we are
        // Keep checking the fridge at max once every 4 hours
        if (actor.getHunger().getPercentageHunger() < hungryAtPercent && (randomUtils.nextFloat() < 0.25 + (1 - actor.getHunger().getPercentageHunger()))) {
            if (TimeTracker.getInstance().getActualTime().minus(lastFridgeCheckTime).getSeconds() > (60 * 60 * fridgeRecheckTime)) {
                lastFridgeCheckTime = TimeTracker.getInstance().getActualTime();
                CookerAPI cookerAPI = randomUtils.getRandomFromList(actor.getHouse().getDevicesOfType(CookerAPI.categoryName));
                if (cookerAPI != null && canCook && randomUtils.nextFloat() < cookChance) {
                    return new CookStrategy(actor, cookerAPI);
                }
                else {
                    return new GetNutritionStrategy(actor);
                }
            }
        }

        if (TimeTracker.getInstance().getActualTime().minus(lastSocializeTime).getSeconds() > (60 * 60 * 3 / socializeFactor) && randomUtils.nextFloat() < socializeFactor / 2) {
            Collection<Living> livings;
            if (talksToAnimals) {
                livings = actor.getHouse().findAdjacentLivings(actor);
            } else {
                livings = actor.getHouse().findAdjacentLivings(actor, "Human");
            }

            if (!livings.isEmpty()) {
                lastSocializeTime = TimeTracker.getInstance().getActualTime();
                return new SocializeStrategy(actor, livings, socializeFactor);
            }
        }

        // Do some nasty choirs
        if (randomUtils.nextFloat() < choirsChance) {
            if (refillFridge) {
                FridgeAPI fridgeAPI = RandomUtils.getInstance().getRandomFromList(actor.getHouse().getDevicesOfType(FridgeAPI.categoryName));
                if (fridgeAPI != null && fridgeAPI.getFoodLeft() < 125) {
                    return new RefillFridgeStrategy(actor, fridgeAPI);
                }
            }

            CookerAPI cookerAPI = RandomUtils.getInstance().getRandomFromList(actor.getHouse().getDevicesOfType(CookerAPI.categoryName));
            if (cookerAPI != null && randomUtils.nextFloat() < (cookerAPI.getFilth() / Constants.COOKER_MAX_BEARABLE_FILTH / 20)) {
                return new CleanCookerStrategy(actor, cookerAPI);
            }

            WashingMachineAPI washingMachineAPI = RandomUtils.getInstance().getRandomFromList(actor.getHouse().getDevicesOfType(WashingMachineAPI.categoryName));
            if (washingMachineAPI != null && randomUtils.nextFloat() < .15f) {
                return new InteractWithWashingMachine(actor, washingMachineAPI);
            }
        }
        // Just do something
        int random = RandomUtils.getInstance().nextInt(5);
        switch (random) {
            case 0:
                return new WatchTVStrategy(actor);
            case 1:
                return new DoSomeSportStrategy(actor);
            case 2:
                if (playsWithLights) return new PlayWithLight(actor);
                // No break means adults look out of window, and they kinda prefer it also
            case 3:
                return new LookOutOfWindow(actor);
            case 4:
                return new BrowseWeb(actor);
        }
        // Java, pls, we cannot get here! But just to make it work
        return new PlayWithLight(actor);
    }

    public static class Builder {
        private float choirsChance = .1f;
        private float hungryAtPercent = 0.89f;
        private float fridgeRecheck = 4;
        private float socializeFactor = 0.4f;
        private float cookChance = 0.25f;

        private boolean refillFridge = false;
        private boolean talksToAnimals = false;
        private boolean playsWithLights = false;
        protected boolean canCook = false;

        public Builder setChoirsChance(float choirsChance) {
            this.choirsChance = choirsChance;
            return this;
        }

        /**
         * The Human will start looking for food in a fridge after his hunger drops below the percentage. The more
         * hungry the Human gets, the more likely he is to trigger the fridge check
         * @param hungryAtPercent default 0.89f
         * @return the builder for easy chaining
         */
        public Builder setHungryAtPercent(float hungryAtPercent) {
            this.hungryAtPercent = hungryAtPercent;
            return this;
        }

        /**
         * In hours, if the Human looks for food in a fridge and it is empty, is allowed to go look for food in fridge
         * again?
         * @param fridgeRecheck default 4
         * @return the builder for easy chaining
         */
        public Builder setFridgeRecheck(float fridgeRecheck) {
            this.fridgeRecheck = fridgeRecheck;
            return this;
        }

        /**
         * Can this person drive the car to the shop and refill the fridge?
         * @param refillFridge  default false
         * @return the builder for easy chaining
         */
        public Builder setRefillFridge(boolean refillFridge) {
            this.refillFridge = refillFridge;
            return this;
        }

        /**
         * Determines how much often will the Human socialize and how long each socialization will take as well
         * as the hunger damage it will do
         * @param socializeFactor default 0.4
         * @return the builder for easy chaining
         */
        public Builder setSocializeFactor(float socializeFactor) {
            this.socializeFactor = socializeFactor;
            return this;
        }

        /**
         * Can the Human socialize with animals?
         * @param talksToAnimals default false
         * @return the builder for easy chaining
         */
        public Builder setTalksToAnimals(boolean talksToAnimals) {
            this.talksToAnimals = talksToAnimals;
            return this;
        }

        /**
         * Allows the PlayWithLights behavior strategy to take effect on this Human
         * @param playsWithLights default false
         * @return the builder for easy chaining
         */
        public Builder setPlaysWithLights(boolean playsWithLights) {
            this.playsWithLights = playsWithLights;
            return this;
        }

        /**
         * Persons that can cook can enjoy operating the cooker device. The person will only use the cooker as
         * substitution for looking into fridge for food. Cooking is magical process that does not require
         * any food stored in the house anywhere.
         * @param canCook default false;
         * @return the builder for easy chaining
         */
        public Builder setCanCook(boolean canCook) {
            this.canCook = canCook;
            return this;
        }

        /**
         * Persons that can cook can enjoy operating the cooker device. The person will only use the cooker as
         * substitution for looking into fridge for food. Cooking is magical process that does not require
         * any food stored in the house anywhere <br/><br/>
         * If the Human can cook, chance will determine if they will rather cook for themselves instead of eating from
         * fridge. Has no effect unless canCook is set to true
         * @param canCook default false;
         * @param chance default 0.2f
         * @return the builder for easy chaining
         */
        public Builder setCanCook(boolean canCook, float chance) {
            this.canCook = canCook;
            this.cookChance = chance;
            return this;
        }

        /**
         * Chance that this Human will rather cook for themselves instead of eating from fridge. Has no effect unless
         * canCook is set to true
         * @param chance default 0.2f
         * @return the builder for easy chaining
         */
        public Builder setCookChance(float chance) {
            this.cookChance = chance;
            return this;
        }

        /**
         * @return the complete behavior based on the settings of this builder
         */
        public ParametrizedHumanBehavior build() {
            return new ParametrizedHumanBehavior(this);
        }
    }
}
