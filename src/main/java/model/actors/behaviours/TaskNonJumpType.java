package model.actors.behaviours;

public enum TaskNonJumpType {
    INCREMENTAL,
    WAITING,
    STEP_OVER
}
