package model.actors.behaviours;

import controller.TimeTracker;
import controller.action.Action;
import controller.action.ActionNoArgs;
import controller.event.EventExecution;
import controller.event.GlobalEventSystem;
import model.actors.Behaviour;
import model.actors.behaviours.strategies.HungryAnimalEvent;
import model.actors.behaviours.strategies.animal.MeowStrategy;
import model.actors.behaviours.strategies.animal.ScratchStrategy;
import model.actors.behaviours.strategies.animal.WanderAroundStrategy;
import model.actors.livings.Animal;
import utils.RandomUtils;

import java.time.Duration;

public class CatBehaviour extends AnimalBehaviour {

    protected static RandomUtils randomUtils = RandomUtils.getInstance();
    protected static TimeTracker timeTracker = TimeTracker.getInstance();

    @Override
    protected AnimalBehaviorStrategy setStrategy() {
        if (actor.getHunger().getPercentageHunger() < 0.7f) {
            return new MeowStrategy(actor);
        }
        else {
            switch (randomUtils.nextInt(2)) {
                case 0:
                    return new WanderAroundStrategy(actor);
                case 1:
                    return new ScratchStrategy(actor);
            }
        }
        // We cant get here in reality but just to make sure and to make the IDE shut up
        return new WanderAroundStrategy(actor);
    }
}
