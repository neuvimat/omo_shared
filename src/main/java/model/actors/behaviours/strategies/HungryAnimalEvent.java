package model.actors.behaviours.strategies;

import controller.event.DisposableEvent;
import controller.event.Event;
import model.actors.livings.Animal;

public class HungryAnimalEvent implements Event {
    private Animal animal;

    public HungryAnimalEvent(Animal animal) {
        this.animal = animal;
    }

    public Animal getAnimal() {
        return animal;
    }

    @Override
    public String getCategoryName() {
        return "HungryAnimalEvent";
    }
}
