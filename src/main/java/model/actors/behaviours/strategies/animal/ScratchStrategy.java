package model.actors.behaviours.strategies.animal;

import model.actors.behaviours.AnimalBehaviorStrategy;
import model.actors.behaviours.Task;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Animal;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.Device;
import model.device.devices.Television;

import java.util.List;

public class ScratchStrategy extends AnimalBehaviorStrategy {

    private Room room;
    private Device target;

    public ScratchStrategy(Animal animal) {
        super(new TaskLinear(), animal);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            room = randomUtils.getRandomFromList(animal.getHouse().getAllRooms());
            return animal.wanderAround();
        });
        taskLinear.addTaskStepMethod(() -> {
            target = randomUtils.getRandomFromList(room.getDevices());
            return animal.scratch(target);
        });
    }
}
