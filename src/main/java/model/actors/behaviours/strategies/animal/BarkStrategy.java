package model.actors.behaviours.strategies.animal;

import model.actors.behaviours.AnimalBehaviorStrategy;
import model.actors.behaviours.Task;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Animal;
import model.actors.livings.Human;

public class BarkStrategy extends AnimalBehaviorStrategy {

    public BarkStrategy(Animal animal) {
        super(new TaskLinear(), animal);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            return animal.announceHunger("Barking");
        });
    }
}
