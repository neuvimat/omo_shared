package model.actors.behaviours.strategies.animal;

import model.actors.behaviours.AnimalBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Animal;
import model.configuration.Room;
import model.device.devices.Television;

import java.util.List;

public class WanderAroundStrategy extends AnimalBehaviorStrategy {

    Room room;

    public WanderAroundStrategy(Animal animal) {
        super(new TaskLinear(), animal);

        TaskLinear taskLinear = (TaskLinear)task;

        wanderAgain(animal, taskLinear);
        wanderAgain(animal, taskLinear);
        if (randomUtils.nextFloat() < .5) {
            wanderAgain(animal, taskLinear);
        }
        if (randomUtils.nextFloat() < .1) {
            wanderAgain(animal, taskLinear);
        }
    }

    private void wanderAgain(Animal animal, TaskLinear taskLinear) {
        taskLinear.addTaskStepMethod(() -> {
            List<Room> rooms = animal.getHouse().findRoomsByDeviceCategory(new Television());
            room = randomUtils.getRandomFromList(rooms);
            return animal.wanderAround();
        });
    }
}
