package model.actors.behaviours.strategies.animal;

import model.actors.behaviours.AnimalBehaviorStrategy;
import model.actors.behaviours.Task;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Animal;
import model.actors.livings.Human;

public class ChaseOwnTailStrategy extends AnimalBehaviorStrategy {

    public ChaseOwnTailStrategy(Animal animal) {
        super(new TaskLinear(), animal);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            return animal.chaseOwnTail();
        });

        for (int i = 0; i < 4; i++) {
            if (randomUtils.nextFloat() < 0.6f - i*0.10)
            taskLinear.addTaskStepMethod(() -> {
                return animal.chaseOwnTail();
            });
        }
    }
}
