package model.actors.behaviours.strategies.human;

import model.actors.behaviours.*;
import model.actors.livings.Human;
import model.conveyance.Conveyance;

import java.time.Duration;
import java.util.List;

public class DoSomeSportStrategy extends HumanBehaviorStrategy {
    private Conveyance usedConveyeance = null;

    public DoSomeSportStrategy(Human human) {
        super(new TaskNonLinear(), human);

        TaskNonLinear taskNonLinear = (TaskNonLinear)task;

        taskNonLinear.addTaskStepMethod(()->{
            return new TaskNonLinearStep(human.moveOutsideAction());
        });
        taskNonLinear.addTaskStepMethod(()->{
            List<Conveyance> conveyances = human.getHouse().getOutside().getConveyances();
            usedConveyeance = randomUtils.getRandomFromList(conveyances);
            if(usedConveyeance.isBeingUsed()){
                // fixme: Mates added random duration for sport action: from 20 to 75 minutes, the wait might be kinda
                // fixme: wasteful right now, needs a different approach, maybe look for other usable Conveyance?
                // fixme: also sporting includes driving a car around xD
                return new TaskNonLinearStep(human.waitAction(Duration.ofMinutes(20)), TaskNonJumpType.WAITING);
            }
            usedConveyeance.startUsing();
            return new TaskNonLinearStep(human.doSportAction(usedConveyeance));
        });

        taskNonLinear.addTaskStepMethod(()->{
            usedConveyeance.stopUsing();
            return new TaskNonLinearStep(human.moveToRoomAction(human.getHouse().getAllRooms().get(0)));
        });
    }
}
