package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.FridgeAPI;
import utils.RandomUtils;

import java.time.Duration;
import java.util.List;

public class SabotageFridge extends HumanBehaviorStrategy {

    FridgeAPI target;

    public SabotageFridge(Human human) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            List<Room> rooms = human.getHouse().findRoomsByDeviceCategory(FridgeAPI.categoryName);
            Room room = randomUtils.getRandomFromList(rooms);
            target = RandomUtils.getInstance().getRandomFromList(room.getDevicesOfType(FridgeAPI.categoryName));
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() -> {
            return new ActionNoArgs("Checking fridge for sabotage", human, Duration.ofSeconds(30), target, ()->{
                if (target.isOn()) {
                    taskLinear.addTaskStepMethod(()->{
                        return new ActionNoArgs("Sabotaging fridge", human, Duration.ofSeconds(60), target, ()->{
                            target.withdraw(12f);
                            target.turnOff();
                        });
                    });
                }
                if (!target.isOn()) {
                    taskLinear.addTaskStepMethod(()->{
                        return new ActionNoArgs("Fridge already sabotaged, waste food instead", human, Duration.ofSeconds(10), target, ()->{
                            target.withdraw(25f);
                        });
                    });
                }
            });
        });
    }
}
