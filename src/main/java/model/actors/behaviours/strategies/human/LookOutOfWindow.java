package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.Window;
import utils.RandomUtils;

import java.time.Duration;
import java.util.List;

public class LookOutOfWindow extends HumanBehaviorStrategy {

    private Window window = null;
    private Room room = null;

    public LookOutOfWindow(Human human) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            List<Room> rooms = human.getHouse().findRoomsByDeviceCategory(Window.categoryName);
            room = randomUtils.getRandomFromList(rooms);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(()-> {
            window = randomUtils.getRandomFromList(room.getDevicesOfType(Window.categoryName));
            return new ActionNoArgs("Looking out of window", human, Duration.ofMinutes(2+RandomUtils.getInstance().nextInt(10)), window, ()->{});
        });
    }
}