package model.actors.behaviours.strategies.human;

import OLD.devices.devices.WashingMachine;
import controller.TimeTracker;
import controller.action.ActionNoArgs;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.CookerAPI;
import model.device.devices.WashingMachineAPI;
import utils.Constants;

import java.time.Duration;
import java.util.List;

public class InteractWithWashingMachine extends HumanBehaviorStrategy {

    private Room room;
    private WashingMachineAPI washingMachine = null;

    public InteractWithWashingMachine(Human human, WashingMachineAPI washingMachine) {
        super(new TaskLinear(), human);
        this.washingMachine = washingMachine;

        TaskLinear taskLinear = (TaskLinear) task;

        taskLinear.addTaskStepMethod(() -> {
            room = human.getHouse().findRoomByDeviceReference(washingMachine);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() -> {
            if (!washingMachine.isBroken()) {
                if (!washingMachine.isOn() && washingMachine.getClotherRemaining() == 0) {
                    return human.turnDeviceOnAction(washingMachine);
                }
                else if (TimeTracker.getInstance().getActualTime().minus(washingMachine.getLastTurnOn()).getSeconds() > 60 * 60 * 2) {
                    if (washingMachine.isOn()) {
                        return new ActionNoArgs("Turning Device off", human, Duration.ofMinutes(1), washingMachine, () -> {
                            washingMachine.turnOff();
                            taskLinear.addTaskStepMethod(() -> {
                                return new ActionNoArgs("Is hanging clothes", human, Duration.ofMinutes(15 + randomUtils.nextInt(10)), () -> {
                                    washingMachine.hangClothes();
                                });
                            });
                        });
                    }
                    return new ActionNoArgs("Is hanging clothes", human, Duration.ofMinutes(15 + randomUtils.nextInt(10)), () -> {
                        washingMachine.hangClothes();
                    });
                }
                else {
                    return new ActionNoArgs("Can't hang clothes, Washing Machine is still operating...", human, Duration.ofMinutes(0), () -> {
                    });
                }
            }
            else {
                return new ActionNoArgs("Cant't use Washing Machine, it is broken! Aborting...", human, Duration.ZERO, washingMachine, () -> {
                    taskLinear.cancelTask();
                });
            }
        });
    }
}
