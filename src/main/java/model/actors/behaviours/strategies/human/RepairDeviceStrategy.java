package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.*;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.Device;
import model.device.Documentation;
import model.device.RepairSolution;

import java.time.Duration;
import java.util.List;

public class RepairDeviceStrategy extends HumanBehaviorStrategy {
    private Device toRepair;

    public RepairDeviceStrategy(Human human, Device device) {
        super(new TaskNonLinear(), human);
        toRepair = device;
        TaskNonLinear taskNonLinear = (TaskNonLinear)task;
        taskNonLinear.addTaskStepMethod(()->{
            Room room = human.getHouse().findRoomByDevice(device);
            if(room == null || device == null){
                taskNonLinear.cancelTask();
                return new TaskNonLinearStep(new ActionNoArgs("Aborting repair", human, Duration.ofSeconds(1), () ->{}));
            }
            if(!human.isInRoom(room)){
                return new TaskNonLinearStep(human.moveToRoomAction(room), TaskNonJumpType.WAITING);
            }
            return new TaskNonLinearStep(human.repairDevice(device));
        });
    }
}
