package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import controller.action.ActionSingleArgs;
import controller.action.ActionTwoArgs;
import model.actors.Living;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.CookerAPI;
import model.device.devices.FridgeAPI;

import java.time.Duration;
import java.util.List;

public class CookStrategy extends HumanBehaviorStrategy {

    private Room room;
    private CookerAPI cooker = null;

    public CookStrategy(Human human, CookerAPI cooker) {
        super(new TaskLinear(), human);
        this.cooker = cooker;

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            room = human.getHouse().findRoomByDeviceReference(cooker);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() ->{
            if (!cooker.isBroken()) {
                if (cooker.isTooFilthy()) {
                    return new ActionNoArgs("What the actual fuck! The cooker is too filthy! Aborting...", human, Duration.ZERO, cooker, ()->{
                        taskLinear.cancelTask();
                    });
                }
                else {
                    return human.turnDeviceOnAction(cooker);
                }
            }
            else {
                return new ActionNoArgs("Cant't cook, cooker is broken! Aborting...", human, Duration.ZERO, cooker, ()->{
                    taskLinear.cancelTask();
                });
            }
        });
        taskLinear.addTaskStepMethod(() ->{
            return human.useCooker(cooker);
        });
        taskLinear.addTaskStepMethod(() ->{
            return human.turnDeviceOffAction(cooker);
        });
        taskLinear.addTaskStepMethod(() ->{
            return new ActionNoArgs("Eating food they cooked themselves", human, Duration.ofMinutes(10), ()->{
                float saved = human.getHunger().refreshFully();
                human.savedByCooking(saved);
            });
        });
    }
}
