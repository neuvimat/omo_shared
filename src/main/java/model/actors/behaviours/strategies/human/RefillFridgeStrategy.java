package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.*;
import model.actors.livings.Human;
import model.configuration.Room;
import model.conveyance.Car;
import model.device.devices.FridgeAPI;
import utils.Constants;
import utils.RandomUtils;

import java.time.Duration;

public class RefillFridgeStrategy extends HumanBehaviorStrategy {
    public RefillFridgeStrategy(Human human, FridgeAPI fridge) {
        super(new TaskLinear(), human);
        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(()-> {
            return human.moveOutsideAction();
        });
        taskLinear.addTaskStepMethod(()->{
            return new ActionNoArgs("Looking for car to drive to shop", human, Duration.ofSeconds(30), ()-> {
                Car car = RandomUtils.getInstance().getRandomFromList(human.getHouse().getOutside().getConveyancesOfType("Car"));
                if (car != null && !car.isBeingUsed()) {
                    taskLinear.addTaskStepMethod(()->{
                        return human.driveCar(Duration.ofSeconds((int)(Constants.TIME_TO_DRIVE_TO_SHOP
                                * RandomUtils.getInstance().variance(Constants.TIME_TO_DRIVE_TO_SHOP_VARIANCE))), car);
                    });
                    taskLinear.addTaskStepMethod(()->{
                        return human.parkCarButKeepUsing(Duration.ofSeconds((int)(Constants.TIME_TO_PARK
                                * RandomUtils.getInstance().variance(Constants.TIME_TO_PARK_VARIANCE))), car);
                    });
                    taskLinear.addTaskStepMethod(()->{
                        return new ActionNoArgs("Shopping", human, Duration.ofSeconds((int)(Constants.TIME_TO_SHOP
                                * RandomUtils.getInstance().variance(Constants.TIME_TO_SHOP_VARIANCE))), ()->{});
                    });
                    taskLinear.addTaskStepMethod(()->{
                        return human.driveCar(Duration.ofSeconds((int)(Constants.TIME_TO_DRIVE_TO_SHOP
                                * RandomUtils.getInstance().variance(Constants.TIME_TO_DRIVE_TO_SHOP_VARIANCE))), car);
                    });
                    taskLinear.addTaskStepMethod(()->{
                        return human.parkCarAtHome(Duration.ofSeconds((int)(Constants.TIME_TO_PARK
                                * RandomUtils.getInstance().variance(Constants.TIME_TO_PARK_VARIANCE))), car);
                    });
                    taskLinear.addTaskStepMethod(()->{
                        Room r = human.getHouse().findRoomByDeviceReference(fridge);
                        return human.moveToRoomAction(r);
                    });
                    taskLinear.addTaskStepMethod(()->{
                        return new ActionNoArgs("Refilling frige", human, Duration.ofMinutes(3), fridge, ()->{
                            fridge.refill();
                        });
                    });
                }

            });
        });
    }
}
