package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import controller.action.ActionSingleArgs;
import controller.action.ActionTwoArgs;
import model.actors.Living;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.FridgeAPI;

import java.time.Duration;
import java.util.List;

public class GetNutritionStrategy extends HumanBehaviorStrategy {

    private Room room = null;
    private FridgeAPI fridge = null;

    public GetNutritionStrategy(Human human) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            List<Room> rooms = human.getHouse().findRoomsByDeviceCategory(FridgeAPI.categoryName);
            room = randomUtils.getRandomFromList(rooms);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() ->{
            fridge = randomUtils.getRandomFromList(room.getDevicesOfType(FridgeAPI.categoryName));
            return new ActionSingleArgs<FridgeAPI>("Checking fridge for food", human, Duration.ofSeconds(30), fridge, this::checkFridgeFood, fridge);
        });
    }

    private void checkFridgeFood(FridgeAPI f) {
        if (fridge.isEmpty()) {
            ((TaskLinear)task).addTaskStepMethod(() -> {
                return new ActionNoArgs("FridgeModel is empty", human, Duration.ofMinutes(0), fridge, ()->{});
            });
            return;
        }
        ((TaskLinear)task).addTaskStepMethod(() -> {
            return new ActionTwoArgs<FridgeAPI, Living>("Eating food from fridge", human, Duration.ofMinutes(5), fridge, FridgeAPI::feedLiving, fridge ,human);
        });
        if (!fridge.isOn() && !fridge.isBroken()) {
            ((TaskLinear)task).addTaskStepMethod(() -> {
                // Fixme: make saboteur NOT turn it on again,
                return human.turnDeviceOnAction(fridge, "Found out fridge is off. Turning it on");
            });
        }
    }
}
