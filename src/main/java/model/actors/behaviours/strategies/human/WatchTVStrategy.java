package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.Television;

import java.time.Duration;
import java.util.List;

public class WatchTVStrategy extends HumanBehaviorStrategy {

    private Television tv = null;
    private Room room = null;

    public WatchTVStrategy(Human human) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            // todo: does this consider the case when the television is already in the room with the actor? so it does not add an useless action of moving the actor
            // todo: maybe wrap this task.addTaskStepMethod inside an if, that checks whether the tv is already where the actor stands
            List<Room> rooms = human.getHouse().findRoomsByDeviceCategory(new Television());
            room = randomUtils.getRandomFromList(rooms);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() ->{
            tv = randomUtils.getRandomFromList(room.getDevicesOfType(new Television()));
            if (!tv.isBroken()) {
                return human.turnDeviceOnAction(tv);
            }
            else {
                return new ActionNoArgs("Cant't watch TV, it is broken! Aborting...", human, Duration.ZERO, tv, ()->{
                    taskLinear.cancelTask();
                });
            }
        });
        taskLinear.addTaskStepMethod(() ->{
            return human.changeTVChanelAction(tv, randomUtils.nextInt(5));
        });
        taskLinear.addTaskStepMethod(() ->{
            return human.watchTVAction(tv, Duration.ofMinutes(5 + randomUtils.nextInt(55)));
        });
        taskLinear.addTaskStepMethod(() ->{
            return human.turnDeviceOffAction(tv);
        });
    }
}