package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.Living;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.Television;
import utils.RandomUtils;

import java.time.Duration;
import java.util.Collection;
import java.util.List;

public class SocializeStrategy extends HumanBehaviorStrategy {

    public SocializeStrategy(Human human, Collection<Living> subjectsForSocialization, float socialFactor) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        Living target = RandomUtils.getInstance().getRandomFromCollection(subjectsForSocialization);

        taskLinear.addTaskStepMethod(() -> {
            return human.doSocialAction(target, socialFactor);
        });
    }
}
