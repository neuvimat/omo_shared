package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.LightAPI;
import model.device.devices.LightColor;
import utils.RandomUtils;

import java.time.Duration;
import java.util.List;

public class PlayWithLight extends HumanBehaviorStrategy {

    private LightAPI lightAPI = null;
    private Room room = null;

    public PlayWithLight(Human human) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            List<Room> rooms = human.getHouse().findRoomsByDeviceCategory(LightAPI.categoryName);
            room = randomUtils.getRandomFromList(rooms);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() -> {
            return new ActionNoArgs("Is looking for lightAPI to play with", human, Duration.ofMinutes(1), ()->{
                lightAPI = randomUtils.getRandomFromList(room.getDevicesOfType(LightAPI.categoryName));
            });
        });
        for (int i = 0; i < 1 + RandomUtils.getInstance().nextInt(6); i++) {
            taskLinear.addTaskStepMethod(() ->{
                if (lightAPI.isBroken()) {
                    return new ActionNoArgs("The lightAPI is broken! Aborting...", human, Duration.ZERO, lightAPI, ()->{
                        taskLinear.cancelTask();
                    });
                }
                else {
                    return human.turnDeviceOnAction(lightAPI);
                }
            });
            taskLinear.addTaskStepMethod(()-> {
                LightColor color = randomUtils.randomEnum(LightColor.class);
                return human.changeLightColorAction(lightAPI, color);
            });
            taskLinear.addTaskStepMethod(()-> {
                return new ActionNoArgs("Observing the lightAPI", human, Duration.ofSeconds(30 + RandomUtils.getInstance().nextInt(180)), ()->{});
            });
            taskLinear.addTaskStepMethod(()-> {
                return human.turnDeviceOffAction(lightAPI);
            });
        }
    }
}