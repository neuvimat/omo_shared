package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import controller.action.ActionSingleArgs;
import controller.action.ActionTwoArgs;
import model.actors.Living;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.CookerAPI;
import model.device.devices.FridgeAPI;

import java.time.Duration;
import java.util.List;

public class CleanCookerStrategy extends HumanBehaviorStrategy {

    private Room room;
    private CookerAPI cooker = null;

    public CleanCookerStrategy(Human human, CookerAPI cooker) {
        super(new TaskLinear(), human);
        this.cooker = cooker;

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            room = human.getHouse().findRoomByDeviceReference(cooker);
            return human.moveToRoomAction(room);
        });
        taskLinear.addTaskStepMethod(() ->{
            return new ActionNoArgs("Cleaning the cooker", human, Duration.ofMinutes(10), cooker, ()->{
                cooker.clean();
            });
        });
    }
}
