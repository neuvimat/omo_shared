package model.actors.behaviours.strategies.human;

import controller.TimeTracker;
import controller.action.ActionNoArgs;
import controller.event.EventExecution;
import controller.event.GlobalEventSystem;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.behaviours.strategies.AnimalFedEvent;
import model.actors.livings.Animal;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.ComputerAPI;
import utils.RandomUtils;

import java.time.Duration;
import java.util.List;

public class FeedAnimalStrategy extends HumanBehaviorStrategy {

    public FeedAnimalStrategy(Human human, Animal animal) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            GlobalEventSystem.getInstance().execute(new EventExecution(new AnimalFedEvent(animal), human, TimeTracker.getInstance().getActualTime()));
            return human.feedAnimal(animal);
        });
    }
}