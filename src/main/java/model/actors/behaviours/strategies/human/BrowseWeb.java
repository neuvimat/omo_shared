package model.actors.behaviours.strategies.human;

import controller.action.ActionNoArgs;
import model.actors.behaviours.HumanBehaviorStrategy;
import model.actors.behaviours.TaskLinear;
import model.actors.livings.Human;
import model.configuration.Room;
import model.device.devices.ComputerAPI;
import utils.RandomUtils;

import java.time.Duration;
import java.util.List;

public class BrowseWeb extends HumanBehaviorStrategy {

    private ComputerAPI computer = null;
    private Room room = null;

    public BrowseWeb(Human human) {
        super(new TaskLinear(), human);

        TaskLinear taskLinear = (TaskLinear)task;

        taskLinear.addTaskStepMethod(() -> {
            List<Room> rooms = human.getHouse().findRoomsByDeviceCategory(ComputerAPI.categoryName);
            room = randomUtils.getRandomFromList(rooms);
            if (room == null) {
                return new ActionNoArgs("Can't find computer! Aborting...", human, Duration.ofMinutes(0), ()->{
                    ((TaskLinear) task).cancelTask();
                });
            }
            else {
                return human.moveToRoomAction(room);
            }
        });
        taskLinear.addTaskStepMethod(()-> {
            computer = randomUtils.getRandomFromList(room.getDevicesOfType(ComputerAPI.categoryName));
            if (computer.isOn()) {
                return new ActionNoArgs("Browsing internet", human, Duration.ofMinutes(10+RandomUtils.getInstance().nextInt(110)), computer, ()->{
                    computer.browseInternet();
                });
            }
            else if (!computer.isBroken()) {
                return new ActionNoArgs("Turning device on", human, Duration.ofMinutes(1), computer,  ()->{
                    computer.turnOn();
                    taskLinear.addTaskStepMethod(()-> {
                        return new ActionNoArgs("Browsing internet", human, Duration.ofMinutes(10+RandomUtils.getInstance().nextInt(110)), computer, ()->{
                            computer.browseInternet();
                        });
                    });
                    taskLinear.addTaskStepMethod(()-> {
                        return human.turnDeviceOffAction(computer);
                    });
                });
            }
            else {
                return new ActionNoArgs("Computer is broken! Aborting...", human, Duration.ofMinutes(1), computer,  ()->{});
            }
        });
    }
}