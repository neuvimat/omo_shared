package model.actors.behaviours.strategies;

import controller.event.Event;
import model.actors.livings.Animal;

public class AnimalFedEvent implements Event {
    private Animal animal;

    public AnimalFedEvent(Animal animal) {
        this.animal = animal;
    }

    public Animal getAnimal() {
        return animal;
    }

    @Override
    public String getCategoryName() {
        return "AnimalFedEvent";
    }
}
