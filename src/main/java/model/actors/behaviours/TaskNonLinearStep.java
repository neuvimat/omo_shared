package model.actors.behaviours;

import controller.action.Action;

/**
 * Needed for non-linear tasks, determines jump to next task step.
 */
public class TaskNonLinearStep {
    private Action action;
    private int nextStep;
    TaskNonJumpType type;

    public TaskNonLinearStep(Action action) {
        this.action = action;
        this.nextStep = 0;
        this.type = TaskNonJumpType.INCREMENTAL;
    }

    public TaskNonLinearStep(Action action, TaskNonJumpType type) {
        this.action = action;
        this.nextStep = 0;
        this.type = type;
    }

    public TaskNonLinearStep(Action action, int nextStep) {
        this.action = action;
        this.nextStep = nextStep;
        type = null;
    }

    public boolean doStepOver(){
        return type == TaskNonJumpType.STEP_OVER;
    }

    public Action getAction() {
        return action;
    }

    public int getNextStep(int actualStep) {
        if(type != null){
            switch (type) {
                case INCREMENTAL:
                    return actualStep + 1;
                case WAITING:
                    return actualStep;
            }
        }
        return nextStep;
    }
}
