package model.actors.behaviours;

import model.actors.livings.Human;

public abstract class HumanBehaviorStrategy extends BehaviourStrategy
{
    protected Human human;

    protected HumanBehaviorStrategy(Task task, Human human) {
        super(task);
        this.human = human;
    }
}
