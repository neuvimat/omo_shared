package model.actors.behaviours;

import controller.action.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * Task which can have cycles and jumps.
 */
public class TaskNonLinear implements Task {

    private int actualStepIdx;
    private List<TaskNonLinearStepMethod> stepMethods;

    public TaskNonLinear() {
        actualStepIdx = 0;
        this.stepMethods = stepMethods = new ArrayList<>();
    }

    public TaskNonLinear(List<TaskNonLinearStepMethod> stepMethods) {
        actualStepIdx = 0;
        this.stepMethods = stepMethods;
    }

    public TaskNonLinear(int startingStep, List<TaskNonLinearStepMethod> stepMethods) {
        this.actualStepIdx = startingStep;
        this.stepMethods = stepMethods;
    }

    @Override
    public Action getNextStep()
    {
        if(hasNextStep()) {
            TaskNonLinearStepMethod actualMethod = stepMethods.get(actualStepIdx);
            TaskNonLinearStep actualStep = actualMethod.getStep();
            actualStepIdx = actualStep.getNextStep(actualStepIdx);
            return actualStep.getAction();
        }
        return null;
    }

    @Override
    public void cancelTask() {
        actualStepIdx = -1;
    }

    @Override
    public boolean hasNextStep(){
        return actualStepIdx >= 0 && actualStepIdx < stepMethods.size();
    }

    public void addTaskStepMethod(TaskNonLinearStepMethod method){
        stepMethods.add(method);
    }
}
