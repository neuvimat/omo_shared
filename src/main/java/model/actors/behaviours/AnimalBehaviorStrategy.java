package model.actors.behaviours;

import model.actors.livings.Animal;
import model.actors.livings.Human;

public abstract class AnimalBehaviorStrategy extends BehaviourStrategy
{
    protected Animal animal;

    protected AnimalBehaviorStrategy(Task task, Animal animal) {
        super(task);
        this.animal = animal;
    }
}
