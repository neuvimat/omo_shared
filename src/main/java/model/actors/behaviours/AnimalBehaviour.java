package model.actors.behaviours;

import controller.TimeTracker;
import controller.action.Action;
import model.actors.Behaviour;
import model.actors.livings.Animal;
import model.actors.livings.Human;
import utils.RandomUtils;

import java.util.ArrayList;

public abstract class AnimalBehaviour implements Behaviour {

    protected Animal actor;
    protected static RandomUtils randomUtils = RandomUtils.getInstance();
    protected static TimeTracker timeTracker = TimeTracker.getInstance();

    protected AnimalBehaviorStrategy strategy;

    public AnimalBehaviour() {
        strategy = null;
    }

    public void setActor(Animal actor) {
        this.actor = actor;
    }

    public Action think() {
        if (strategy == null || !strategy.hasNextAction()) {
            strategy = setStrategy();
        }
        return strategy.getNextAction();
    }

    abstract protected AnimalBehaviorStrategy setStrategy();
}
