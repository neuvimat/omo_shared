package model.actors.behaviours;

/**
 * Method for choosing next action step.
 */
public interface TaskNonLinearStepMethod {
    TaskNonLinearStep getStep();
}
