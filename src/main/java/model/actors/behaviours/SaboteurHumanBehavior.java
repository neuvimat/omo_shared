package model.actors.behaviours;

import controller.TimeTracker;
import model.actors.Living;
import model.actors.behaviours.strategies.human.*;
import model.actors.livings.Human;
import model.device.devices.FridgeAPI;
import utils.RandomUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;

public class SaboteurHumanBehavior extends ParametrizedHumanBehavior {

    private Duration lastSabotageTime = Duration.ZERO;
    float evilness = 1f;

    public SaboteurHumanBehavior(Builder builder, float evilness) {
        super(builder);
        this.evilness = evilness;
    }

    @Override
    protected HumanBehaviorStrategy setStrategy() {
        if (TimeTracker.getInstance().getActualTime().minus(lastSabotageTime).getSeconds() > (60 * 60 * 24 / evilness)) {
            lastSabotageTime = timeTracker.getActualTime();

            // Todo: add more possibilities for sabotage
            return new SabotageFridge(actor);
        }
        else return super.setStrategy();
    }
}
