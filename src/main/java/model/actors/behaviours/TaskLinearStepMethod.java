package model.actors.behaviours;

import controller.action.Action;

/**
 * Method for choosing next action.
 */
public interface TaskLinearStepMethod {
    Action getStep();
}
