package model.actors.behaviours;

import controller.action.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * Task which is executed incrementally.
 */
public class TaskLinear implements Task {
    private int actualStepIdx;
    private List<TaskLinearStepMethod> stepMethods;

    public TaskLinear() {
        actualStepIdx = 0;
        this.stepMethods = new ArrayList<TaskLinearStepMethod>();
    }

    public TaskLinear(List<TaskLinearStepMethod> stepMethods) {
        actualStepIdx = 0;
        this.stepMethods = stepMethods;
    }

    @Override
    public Action getNextStep()
    {
        if(hasNextStep()) {
            TaskLinearStepMethod actualMethod = stepMethods.get(actualStepIdx);
            actualStepIdx++;
            return actualMethod.getStep();
        }
        return null;
    }

    @Override
    public void cancelTask() {
        actualStepIdx = -1;
    }

    @Override
    public boolean hasNextStep(){
        return actualStepIdx >= 0 && actualStepIdx < stepMethods.size();
    }

    public void addTaskStepMethod(TaskLinearStepMethod method){
        stepMethods.add(method);
    }
}
