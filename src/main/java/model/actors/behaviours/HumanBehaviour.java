package model.actors.behaviours;

import controller.TimeTracker;
import controller.action.Action;
import controller.event.EventListenerOwner;
import model.actors.Behaviour;
import model.actors.behaviours.strategies.human.DoSomeSportStrategy;
import model.actors.behaviours.strategies.human.GetNutritionStrategy;
import model.actors.behaviours.strategies.human.RefillFridgeStrategy;
import model.actors.behaviours.strategies.human.WatchTVStrategy;
import model.actors.livings.Human;
import model.device.devices.FridgeAPI;
import utils.RandomUtils;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public abstract class HumanBehaviour implements Behaviour, EventListenerOwner {

    protected Human actor;
    protected static RandomUtils randomUtils = RandomUtils.getInstance();
    protected static TimeTracker timeTracker = TimeTracker.getInstance();
    protected List<HumanBehaviorStrategy> todoList;

    protected HumanBehaviorStrategy strategy;

    public HumanBehaviour() {
        strategy = null;
    }

    public void setActor(Human actor) {
        this.actor = actor;
    }

    public Action think() {
        if (strategy == null || !strategy.hasNextAction()) {
            strategy = setStrategy();
        }
        return strategy.getNextAction();
    }

    @Override
    public String getName() {
        return actor.getName();
    }

    abstract protected HumanBehaviorStrategy setStrategy();

    public void addTodo(HumanBehaviorStrategy strategy) {
        todoList.add(strategy);
    }
}
