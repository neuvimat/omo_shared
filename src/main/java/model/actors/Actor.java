package model.actors;

import controller.TimeTracker;
import controller.action.Action;
import controller.event.EventSource;
import model.configuration.House;
import utils.CategoryNamed;
import utils.RandomUtils;

import java.time.Duration;
import java.util.Random;

/**
 * Represent something that can execute actions and change its environment.
 */
public abstract class Actor implements CategoryNamed, EventSource {

    protected House house;
    protected Duration lastUpdated = Duration.ZERO;
    protected String name;

    protected RandomUtils randomUtils = RandomUtils.getInstance();
    protected TimeTracker timeTracker = TimeTracker.getInstance();

    protected Actor(House house, String name) {
        this.house = house;
        this.name = name;
        house.registerActor(this);
    }

    protected Actor(House house) {
        this.house = house;
        this.name = "Unnamed Actor";
        house.registerActor(this);
    }

    public abstract boolean update(Duration duration);

    /**
     * @return Next action that actor is going to execute.
     */
    public abstract Action getNextAction();

    /**
     * @return House that contains this actor.
     */
    public House getHouse() {
        return house;
    }

    /**
     * @return Some basic name of actor for reporting purposes.
     */
    public String getName() {
        return this.name;
    }
}