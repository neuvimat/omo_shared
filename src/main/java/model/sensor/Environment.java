package model.sensor;

import controller.TimeTracker;
import controller.action.Action;
import controller.action.ActionNoArgs;
import controller.event.EventExecution;
import controller.event.EventSource;
import controller.event.EventSystem;
import controller.event.GlobalEventSystem;
import java.time.Duration;
import model.actors.Actor;
import model.configuration.House;
import utils.RandomUtils;

public class Environment extends Actor implements EventSource {

    private Wind actualWind;
    private Temperature actualTemperature;
    private RandomUtils randomUtils;
    private EventSystem eventSystem;
    private TimeTracker timeTracker;

    public Environment(House house) {
        super(house, "The Weather");
        actualWind = Wind.NO_WIND;
        actualTemperature = Temperature.NORMAL;
        randomUtils = RandomUtils.getInstance();
        eventSystem = GlobalEventSystem.getInstance();
        timeTracker = TimeTracker.getInstance();
    }
    @Override
    public boolean update(Duration duration) {
        return true;
    }

    private Action getWindChangedAction(){
        return new ActionNoArgs("Wind changed", this, Duration.ofHours(3+randomUtils.nextInt(24)), this::executeWindChange);
    }

    private Action getTemperatureChangedAction(){
        return new ActionNoArgs("Temperature changed", this, Duration.ofHours(3+randomUtils.nextInt(24)), this::executeTemperatureChange);
    }

    private void executeWindChange(){
        Wind newWind = Wind.create(randomUtils.nextInt(3));
        if(newWind == Wind.HURRICANE){
            if(randomUtils.nextInt(5) > 0){
                newWind = Wind.WINDY;
            }
        }
        if(newWind != actualWind){
            actualWind = newWind;
            eventSystem.execute(new EventExecution(new WindEvent(newWind), this, timeTracker.getActualTime()));
        }
    }

    private void executeTemperatureChange(){
        Temperature newTemp = Temperature.create(randomUtils.nextInt(3));
        if(newTemp != actualTemperature){ // todo: equals instead, perhaps?
            actualTemperature = newTemp;
            eventSystem.execute(new EventExecution(new TemperatureEvent(newTemp), this, timeTracker.getActualTime()));
        }
    }

    @Override
    public Action getNextAction() {
        return randomUtils.nextInt(2) > 0 ? getWindChangedAction() : getTemperatureChangedAction();
    }

    @Override
    public String getCategoryName() {
        return "Environment";
    }
}
