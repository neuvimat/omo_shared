package model.sensor;

public enum Temperature {
    LOW(0),
    NORMAL(1),
    HIGH(2);

    private int v;
    private Temperature(int value){
        v = value;
    }

    public int getValue() {
        return v;
    }

    public static Temperature create(int value){
        switch (value){
            case 0:
                return LOW;
            case 1:
                return NORMAL;
            case 2:
                return HIGH;
            default:
                return NORMAL;
        }
    }
}
