package model.sensor;

import controller.event.EventListenerImpl;
import controller.event.EventListenerOwner;
import controller.event.EventSystem;
import controller.event.GlobalEventSystem;
import model.actors.Living;
import model.actors.behaviours.strategies.AnimalFedEvent;
import model.actors.behaviours.strategies.HungryAnimalEvent;
import model.actors.behaviours.strategies.human.FeedAnimalStrategy;
import model.actors.livings.Animal;
import model.actors.livings.Human;
import model.configuration.House;
import model.configuration.Room;
import model.device.devices.Window;
import utils.CategoryNamed;
import utils.Constants;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class HusbandrySensor extends Sensor implements EventListenerOwner {
    private boolean verbose = Constants.VERBOSE;
    private EventSystem eventSystem;
    private Set<Animal> hungryAnimals;

    public HusbandrySensor(House house) {
        super(house);
        eventSystem = GlobalEventSystem.getInstance();
        eventSystem.attach(new EventListenerImpl<HungryAnimalEvent>(new HungryAnimalEvent(null), this, this::onAnimalHungryEvent));
        eventSystem.attach(new EventListenerImpl<AnimalFedEvent>(new AnimalFedEvent(null), this, this::onAnimalFedEvent));
        hungryAnimals = new HashSet<>();
    }

    @Override
    public String getCategoryName() {
        return "Wind Sensor";
    }

    private void onAnimalHungryEvent(HungryAnimalEvent event){
        if (hungryAnimals.contains(event.getAnimal())) {
            // Yes we know! Ignore it for now, it should be worked on.
            return;
        }
        else {
            hungryAnimals.add(event.getAnimal());
            Collection<Living> livings = house.getAllLivings();
            livings.removeIf((l)-> !l.getCategoryName().equals("Human"));
            List<Living> list = livings.stream().sorted((o1, o2) -> {
                if (o1.getHunger().getPercentageHunger() > o2.getHunger().getPercentageHunger()) {
                    return -1;
                } else if (o1.getHunger().getPercentageHunger() < o2.getHunger().getPercentageHunger()) {
                    return +1;
                }
                return 0; // Very unlikely, but possible
            }).collect(Collectors.toList());
            if (list.isEmpty()) {
                // All people are dead?
            }
            else {
                Human human = (Human)(list.get(0));
                human.addToDo(new FeedAnimalStrategy(human, event.getAnimal()));
            }
        }
    }

    private void onAnimalFedEvent(AnimalFedEvent event) {
        if (!this.hungryAnimals.remove(event.getAnimal())) {
            Logger.getLogger("").warning("Mismatched animal in AnimalFedEvent!");
        }
    }

    @Override
    public String getName() {
        return getCategoryName();
    }
}
