package model.sensor;

import controller.event.Event;

public class TemperatureEvent implements Event {
    private Temperature temperature;

    public TemperatureEvent(Temperature temperature) {
        this.temperature = temperature;
    }

    public static TemperatureEvent getBlank(){
        return new TemperatureEvent(Temperature.NORMAL);
    }

    public Temperature getTemperature() {
        return temperature;
    }

    @Override
    public String getCategoryName() {
        return "TemperatureEvent";
    }
}
