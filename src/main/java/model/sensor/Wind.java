package model.sensor;


public enum Wind {
    NO_WIND(0),
    WINDY(1),
    HURRICANE(2);

    private int v;
    private Wind(int value){
        v = value;
    }

    public int getValue() {
        return v;
    }

    public static Wind create(int value){
        switch (value){
            case 0:
                return NO_WIND;
            case 1:
                return WINDY;
            case 2:
                return HURRICANE;
            default:
                return NO_WIND;
        }
    }
}
