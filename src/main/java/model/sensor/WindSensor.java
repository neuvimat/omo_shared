package model.sensor;

import controller.event.EventListenerImpl;
import controller.event.EventListenerOwner;
import controller.event.EventSystem;
import controller.event.GlobalEventSystem;
import model.configuration.House;
import model.configuration.Room;
import model.device.devices.Window;

import java.util.List;

public class WindSensor extends Sensor implements EventListenerOwner {
    private EventSystem eventSystem;

    public WindSensor(House house) {
        super(house);
        eventSystem = GlobalEventSystem.getInstance();
        eventSystem.attach(new EventListenerImpl<WindEvent>(WindEvent.getBlank(), this, this::onWindEvent));
    }

    @Override
    public String getCategoryName() {
        return  "Wind Sensor";
    }

    @Override
    public String getName() {
        return "Wind Sensor";
    }

    private void onWindEvent(WindEvent event){
        house.forEach((Room room) -> {
            List<Window> windows = room.getDevicesOfType(new Window());
            windows.forEach((Window window) -> {
                switch (event.getWind()){
                    case NO_WIND:
                        window.turnOn();
                        break;
                    case WINDY:
                        window.turnOff();
                        break;
                    case HURRICANE:
                        window.turnOn();
                        window.turnOff();
                        window.turnOn();
                        window.turnOff(); // todo: what is this?? is this supposed to simulate window rambling?
                        break;
                }
            });
        });
    }
}
