package model.sensor;

import controller.event.EventListenerImpl;
import controller.event.EventListenerOwner;
import controller.event.EventSystem;
import controller.event.GlobalEventSystem;
import model.configuration.House;
import model.configuration.Room;
import model.device.devices.Heating;
import model.device.devices.HeatingRegulation;

import java.util.List;

public class TemperatureSensor extends Sensor implements EventListenerOwner {
    private EventSystem eventSystem;

    public TemperatureSensor(House house) {
        super(house);
        eventSystem = GlobalEventSystem.getInstance();
        eventSystem.attach(new EventListenerImpl<TemperatureEvent>(TemperatureEvent.getBlank(), this, this::onTemperatureEvent));
    }

    public String getCategoryName() {
        return "TemperatureSensor";
    }

    @Override
    public String getName() {
        return "TemperatureSensor";
    }

    private void onTemperatureEvent(TemperatureEvent event){
        house.forEach((Room room) -> {
            List<Heating> heatings = room.getDevicesOfType(new Heating());
            heatings.forEach((Heating heating) -> {
                switch (event.getTemperature()){
                    case LOW:
                        heating.turnOn();
                        heating.regulate(HeatingRegulation.MAX);
                        break;
                    case NORMAL:
                        heating.turnOn();
                        heating.regulate(HeatingRegulation.NORMAL);
                        break;
                    case HIGH:
                        heating.turnOff();
                        break;
                }
            });
        });
    }

}
