package model.sensor;

import controller.action.Action;
import model.actors.Actor;
import model.configuration.House;
import utils.CategoryNamed;

public abstract class Sensor implements CategoryNamed {

    protected House house;

    protected Sensor(House house){
        this.house = house;
        house.registerSensor(this);
    }

}
