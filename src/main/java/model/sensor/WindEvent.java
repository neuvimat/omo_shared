package model.sensor;

import controller.event.Event;

public class WindEvent implements Event {
    private Wind wind;

    public WindEvent(Wind wind) {
        this.wind = wind;
    }

    public static WindEvent getBlank(){
        return new WindEvent(Wind.NO_WIND);
    }

    public Wind getWind() {
        return wind;
    }

    @Override
    public String getCategoryName() {
        return "WindEvent";
    }
}
