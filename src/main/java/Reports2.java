import controller.ActionDrivenSimulation;
import controller.Simulation;
import dao.JsonSimulationEnvironmentProvider;
import utils.RandomUtils;
import view.*;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;

public class Reports2 {
    public static void main(String[] args) {
        RandomUtils.create(5);
        final String filepath = "src/main/resources/houses/disfunctionalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofHours(24 * 50));

        ConsumptionCosts costs = new ConsumptionCosts(8, 6, 2);

        try {
            sim.generateReport(new BasicActivityAndUsageReporter(new FileWriter("reports/activities2")));
            sim.generateReport(new BasicConsumptionReporter(new FileWriter("reports/consumption2"),
                    Duration.ZERO, Duration.ofDays(50), costs));
            sim.generateReport(new BasicEventReporter(new FileWriter("reports/events2")));
            sim.generateReport(new BasicHouseConfigurationReporter(new FileWriter("reports/configuration2")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
