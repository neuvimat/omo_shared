import controller.ActionDrivenSimulation;
import controller.Simulation;
import dao.JsonSimulationEnvironmentProvider;
import view.*;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;

public class Reports1 {

    public static void main(String[] args) {
        final String filepath = "src/main/resources/houses/normalFamily.json";
        Simulation sim = new ActionDrivenSimulation(new JsonSimulationEnvironmentProvider(filepath));

        sim.doSteps(Duration.ofHours(24 * 30));

        ConsumptionCosts costs = new ConsumptionCosts(8, 6, 2);

        try {
            sim.generateReport(new BasicActivityAndUsageReporter(new FileWriter("reports/activities1")));
            sim.generateReport(new BasicConsumptionReporter(new FileWriter("reports/consumption1"),
                    Duration.ZERO, Duration.ofDays(30), costs));
            sim.generateReport(new BasicEventReporter(new FileWriter("reports/events1")));
            sim.generateReport(new BasicHouseConfigurationReporter(new FileWriter("reports/configuration1")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
